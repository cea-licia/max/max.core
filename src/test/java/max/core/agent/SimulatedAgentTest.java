/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.agent;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.Assertions.assertIsNotPlayingRole;
import static max.core.test.Assertions.assertIsPlayingRole;
import static max.core.test.Assertions.assertIsPlayingRoles;
import static max.core.test.Assertions.assertNotIn;
import static max.core.test.TestMain.launchTester;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;
import max.core.Community;
import max.core.ExperimenterAction;
import max.core.action.ACLeaveEnvironment;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.role.RActor;
import max.core.role.RGroupManager;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationSchedule;
import max.core.scheduling.ActivationScheduleFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * 
 * @author Önder Gürcan
 *
 */
public class SimulatedAgentTest {

	/**
	 * Define some testing roles.
	 */
	private interface RoleA extends RActor {}
	private interface RoleB extends RoleA {}
	private interface RoleC extends RoleA {}
	private interface RoleD extends RoleB, RoleC {}

	/**
	 * Setup simulation parameters before each test.
	 *
	 * @param tempDir Temporary folder created by JUnit
	 */
	@BeforeEach
	public void before(@TempDir Path tempDir) {
		// clear all parameter values
		clearParameters();
		// set simulation step to 1
		setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
		// set UI mode to Silent
		setParameter("MAX_CORE_UI_MODE", "Silent");
		setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
	}

	@Test
	public void activateWithWrongRole(TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			private SimulatedAgent simulatedAgent;
			private SimulatedEnvironment sEnv;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<>();

				// Launch the simulatedEnvironment
				sEnv = new SimulatedEnvironment();
				agents.add(sEnv);

				// Create an agent and launch it
				final Plan<SimulatedAgent> plan = new Plan<>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Collections.singletonList(
								new ActionActivator<>(
										ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(
												sEnv.getName(),
												RGroupManager.class,
												null)));
					}
				};
				simulatedAgent = new SimulatedAgent(plan);
				agents.add(simulatedAgent);

				return agents;
			}

			@Override
			protected void setupExperimenter() {
				schedule(new ActionActivator<>(
						ActivationScheduleFactory.createOneTime(BigDecimal.TEN),
						new ExperimenterAction<>(this) {
							@Override
							public void execute() {
								assertIsNotPlayingRole(
										simulatedAgent,
										sEnv,
										RGroupManager.class
								);
							}
						}
				));
			}
		};

		launchTester(tester, 15, testInfo);
	}

	@Test
	public void activateWithoutAnEnvironment(TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			private SimulatedAgent simulatedAgent;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<>();
				
				// Create an agent and launch it
				final Plan<SimulatedAgent> plan = new Plan<>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Collections.emptyList();
					}
				};
				simulatedAgent = new SimulatedAgent(plan);
				agents.add(simulatedAgent);
				
				return agents;
			}
			
			@Override
			protected void setupExperimenter() {
				// Launch the agent at tick 10
				ActivationSchedule schedule = ActivationScheduleFactory.ONE_ONE_INDEF;
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// agent should be alive
						assertTrue(simulatedAgent.isAlive());

						TreeSet<String> myRoles = simulatedAgent.getMyRoles(Community.COMMUNITY,
								Community.SIMULATION_ENVIRONMENT);
						// agent should have 2 roles (Role and RUserRole)
						assertEquals(2, myRoles.size());
						// which is AGENT
						assertEquals(RActor.class.getName(), myRoles.first());

						// agent should not have any contexts for any simulatedEnvironment
						assertEquals(0, simulatedAgent.getContexts().size());
					}
				}));
			}
		};

		launchTester(tester, 10, testInfo);
	}

	@Test
	public void activateWithAnEnvironment(TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			private SimulatedEnvironment simulatedEnvironment;
			private SimulatedAgent simulatedAgent;
			
			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<>();
				
				// Launch the simulatedEnvironment
				simulatedEnvironment = new SimulatedEnvironment();
				agents.add(simulatedEnvironment);

				// Create plan
				final Plan<SimulatedAgent> plan = new Plan<>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Collections.singletonList(new ActionActivator<>(
								ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
								new ACTakeRole<>(
										simulatedEnvironment.getName(),
										RActor.class,
										null)));
					}
				};

				simulatedAgent = new SimulatedAgent(plan);
				agents.add(simulatedAgent);

				return agents;
			}
			
			@Override
			protected void setupExperimenter() {
				// Launch the agent at tick 10
				ActivationSchedule schedule = ActivationScheduleFactory.ONE_ONE_INDEF;
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertIsPlayingRole(simulatedAgent, simulatedEnvironment, RActor.class);
					}
				}));
			}
		};

		launchTester(tester, 10, testInfo);
	}

	/**
	 * Test role inheritance : when an agent take a role in an environment, all intermediary roles should also be taken.
	 *
	 * @param testInfo injected by JUnit5
	 */
	@Test
	public void testRoleInheritance(final TestInfo testInfo) throws Throwable {
		final ExperimenterAgent tester = new ExperimenterAgent() {

			// Keep reference to simulated agent and environment
			private SimulatedAgent agent;
			private SimulatedEnvironment environment;

			@Override
			protected List<MAXAgent> setupScenario() {
				environment = new SimulatedEnvironment() {
					@Override
					protected void activate() {
						addAllowedRole(RoleA.class);
						addAllowedRole(RoleB.class);
						addAllowedRole(RoleC.class);
						addAllowedRole(RoleD.class);
						super.activate();
					}
				};

				final Plan<SimulatedAgent> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Collections.singletonList(
								new ActionActivator<>(
										ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(
												environment.getName(),
												RoleD.class,
												null
										)
								)
						);
					}
				};
				agent = new SimulatedAgent(rawPlan) {
					@Override
					protected void activate() {
						addPlayableRole(RoleA.class);
						addPlayableRole(RoleB.class);
						addPlayableRole(RoleC.class);
						addPlayableRole(RoleD.class);
						super.activate();
					}
				};

				return Arrays.asList(environment, agent);
			}

			@Override
			protected void setupExperimenter() {
				// Check that all intermediary roles are played
				schedule(new ActionActivator<>(
						ActivationScheduleFactory.createOneTime(BigDecimal.TEN),
						new ExperimenterAction<>(this) {
							@Override
							public void execute() {
								assertIsPlayingRoles(
										agent,
										environment,
										RActor.class,
										RoleA.class,
										RoleB.class,
										RoleC.class,
										RoleD.class
								);
							}
						}
				));
			}
		};

		launchTester(tester, 15, testInfo);
	}

	/**
	 * This test verifies if the SimulatedAgent leaves the environment. Tests
	 * {@link ACLeaveEnvironment}.
	 */
	@Test
	public void leave(final TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			private SimulatedEnvironment environment;
			private SimulatedAgent agent;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<>();

				// Launch the environment
				environment = new SimulatedEnvironment();
				agents.add(environment);
				final String envName = environment.getName();

				// Create agents
				final Plan<SimulatedAgent> rawPlan = new Plan<>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Arrays.asList(
								new ActionActivator<>(
										ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(
												envName, RActor.class, null
										)
								),
								new ActionActivator<>(
										ActivationScheduleFactory.createOneTime(BigDecimal.TEN),
										new ACLeaveEnvironment<>(envName, null)
								)
						);
					}
				};

				agent = new SimulatedAgent(rawPlan);
				agents.add(agent);

				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// schedule a one time action to be executed at tick 10.000 for testing if the
				// two agents are connected.
				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.TEN,
						BigDecimal.ONE);
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertNotIn(agent, environment);
					}
				}));
			}
		};

		launchTester(tester, 500, testInfo);
	}

}
