/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.agent;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import max.core.ExperimenterAction;
import max.core.MAXParameters;
import max.core.action.ACTakeRole;
import max.core.action.Action;
import max.core.action.Plan;
import max.core.inspection.DataInspector;
import max.core.inspection.collector.AgentNumberCollector;
import max.core.inspection.exporter.ListExporter;
import max.core.role.RActor;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * A collection of tests related to {@link ExperimenterAgent}.
 *
 */
public class ExperimenterAgentTest {

  /**
   * Setup simulation parameters before each test.
   *
   * @param tempDir Temporary folder created by JUnit
   */
  @BeforeEach
  public void before(@TempDir Path tempDir) {
    // clear all parameter values
    clearParameters();
    // set simulation step to 1
    setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
    // set UI mode to Silent
    setParameter("MAX_CORE_UI_MODE", "Silent");
    setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
  }

  /**
   * Tests a repetitive experiment and makes sure
   *
   * <ul>
   *   <li>We have 2 rounds as expected.
   *   <li>The two rounds are independent.
   * </ul>
   *
   * @param testInfo injected by JUnit
   * @throws Throwable any error that may occur during the simulation
   */
  @Test
  public void repetitiveExperiment(TestInfo testInfo) throws Throwable {
    MAXParameters.setParameter("MAX_CORE_EXPERIMENT_REPETITION", "2");
    final List<List<?>> storage = new ArrayList<>();

    final var tester =
        new ExperimenterAgent() {

          private WithCountAgent agt;
          private SimulatedEnvironment env;

          @Override
          protected List<MAXAgent> setupScenario() {
            env = new SimulatedEnvironment();
            agt =
                new WithCountAgent(
                    new Plan<WithCountAgent>() {
                      @Override
                      public List<ActionActivator<WithCountAgent>> getInitialPlan() {
                        return List.of(
                            new ActionActivator<>(
                                ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
                                new ACTakeRole<>(env.getName(), RActor.class, null)),
                            new ACIncreaseCount(env.getName(), null).oneTime(0L));
                      }
                    });
            return List.of(env, agt);
          }

          @Override
          protected void setupExperimenter() {
            schedule(
                new ActionActivator<>(
                    ActivationScheduleFactory.ONE_SHOT_AT_ONE,
                    new ExperimenterAction<>(this) {
                      @Override
                      public void execute() {
                        assertEquals(1, agt.count);
                      }
                    }));
          }

          @Override
          protected List<DataInspector> setupDataInspectors() {
            final var inspector = new DataInspector();
            final var collector = new AgentNumberCollector(inspector, env.getName());
            collector.addExporter(new ListExporter(storage));

            return List.of(inspector);
          }
        };
    launchTester(tester, 3, testInfo);

    // Round testing part
    final var groupedByTick = storage.stream().collect(Collectors.groupingBy(e -> e.get(0)));

    // Two values for each tick (<=> 2 rounds)
    groupedByTick.forEach((key, value) -> assertEquals(2, value.size()));

    // Duration of each experiment is 3
    assertEquals(4, groupedByTick.keySet().size());
  }

  /**
   * A simple agent with an internal count.
   *
   * @author Pierre Dubaillay
   */
  private static final class WithCountAgent extends SimulatedAgent {

    // Count things
    private int count;

    /**
     * Create a new {@link WithCountAgent}.
     *
     * <p>You need to provide a {@link Plan} that will be executed by the agent. The agent will make
     * a copy of it and set actions owner to itself. You can safely share plans with actions owner
     * set to {@code null}. All actions must support copy operation otherwise an exception is
     * thrown.
     *
     * @param plan a non-null plan
     */
    public WithCountAgent(Plan<? extends WithCountAgent> plan) {
      super(plan);
    }
  }

  /**
   * A simple action that increases the agent's internal count by one.
   *
   * @author Pierre Dubaillay
   */
  private static final class ACIncreaseCount extends Action<WithCountAgent> {

    /**
     * Default constructor.
     *
     * @param environment the environment that this action will be executed in
     * @param owner the owner of this action
     */
    private ACIncreaseCount(String environment, WithCountAgent owner) {
      super(environment, RActor.class, owner, new Object[0]);
    }

    @Override
    public void execute() {
      getOwner().count++;
    }

    @Override
    public <T extends Action<WithCountAgent>> T copy() {
      return (T) new ACIncreaseCount(getEnvironment(), getOwner());
    }
  }
}
