/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.agent;

import static madkit.kernel.AbstractAgent.ReturnCode.SUCCESS;
import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.Assertions.assertIsPlayingRole;
import static max.core.test.Assertions.assertNotIn;
import static max.core.test.TestMain.launchTester;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import max.core.ExperimenterAction;
import max.core.action.ACLeaveEnvironment;
import max.core.action.ACLeaveRole;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.role.RActor;
import max.core.role.RDataInspector;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Tests the basic functionalities of {@link SimulatedEnvironment}}.
 * 
 * @author Önder Gürcan <onder.gurcan@cea.fr>
 * 
 * @version $Revision$ $Date$
 *
 */
public class EnvironmentTest {

	/**
	 * Setup simulation parameters before each test.
	 *
	 * @param tempDir Temporary folder created by JUnit
	 */
	@BeforeEach
	public void before(@TempDir Path tempDir) {
		// clear all parameter values
		clearParameters();
		setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
		setParameter("MAX_CORE_UI_MODE", "Silent");
		setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
	}

	@DisplayName("All environments allow the Agent role")
	@Test
	public void allowsAgentRole() {
		SimulatedEnvironment simulatedEnvironment = new SimulatedEnvironment();
		assertTrue(simulatedEnvironment.allowsRole(RActor.class));
	}

	/**
	 * Tests if agents are added to this simulatedEnvironment when they enter to the
	 * group.
	 */
	@Test
	@DisplayName("An agent enters an simulatedEnvironment at the very beginning")
	public void enterAtTimeZero(TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			private SimulatedEnvironment simulatedEnvironment;
			private SimulatedAgent simulatedAgent;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<>();

				// Set up the simulatedEnvironment
				simulatedEnvironment = new SimulatedEnvironment();
				agents.add(simulatedEnvironment);

				// Set up an agent
				final Plan<SimulatedAgent> plan = new Plan<>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Collections
								.singletonList(new ActionActivator<>(ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ACTakeRole<>(simulatedEnvironment.getName(), RActor.class, null)));
					}
				};
				simulatedAgent = new SimulatedAgent(plan);
				agents.add(simulatedAgent);

				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Check if he entered the simulatedEnvironment and registered himself
				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertIsPlayingRole(simulatedAgent, simulatedEnvironment, RActor.class);
					}
				}.repeatInfinitely(1, 1));
			}
		};

		launchTester(tester, 100, testInfo);
	}

	/**
	 * Tests if agents are added to this simulatedEnvironment when they enter the
	 * simulatedEnvironment after some time.
	 */
	@Test
	@DisplayName("An agent enters an simulatedEnvironment after some time")
	public void enterAfterSomeTime(TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			private SimulatedEnvironment simulatedEnvironment;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<>();

				// Set up the simulatedEnvironment
				simulatedEnvironment = new SimulatedEnvironment();
				agents.add(simulatedEnvironment);

				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Create an agent
				final Plan<SimulatedAgent> plan = new Plan<>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Collections.singletonList(
								new ACTakeRole<SimulatedAgent>(simulatedEnvironment.getName(), RActor.class, null)
										.oneTime(11));
					}
				};
				final SimulatedAgent simulatedAgent = new SimulatedAgent(plan);

				// Launch the agent at tick 10
				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						simulatedAgent.setScheduler(getScheduler());
						assertEquals(SUCCESS, launchAgent(simulatedAgent, false));
					}
				}.oneTime(10));

				// Check if he entered the simulatedEnvironment and registered himself
				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertIsPlayingRole(simulatedAgent, simulatedEnvironment, RActor.class);
					}
				}.repeatInfinitely(12, 1));
			}
		};

		launchTester(tester, 100, testInfo);
	}

	/**
	 * Tests if agents are remove from this simulatedEnvironment when they leave the
	 * simulatedEnvironment.
	 */
	@DisplayName("An agent exits an simulatedEnvironment")
	@ParameterizedTest
	@ValueSource(ints = { 30, 50, 100 })
	public void exit(int exitTime, TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			private SimulatedEnvironment simulatedEnvironment;
			private SimulatedAgent simulatedAgent;

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<>();

				// Set up the simulatedEnvironment
				simulatedEnvironment = new SimulatedEnvironment();
				agents.add(simulatedEnvironment);

				// Create an agent
				final Plan<SimulatedAgent> plan = new Plan<>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Arrays.asList(
								new ACTakeRole<SimulatedAgent>(simulatedEnvironment.getName(), RActor.class, null)
										.oneTime(0),
								new ACLeaveEnvironment<SimulatedAgent>(simulatedEnvironment.getName(), null)
										.oneTime(exitTime));
					}
				};
				simulatedAgent = new SimulatedAgent(plan);
				agents.add(simulatedAgent);

				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Check if the agent is in the simulatedEnvironment till the moment it exits
				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertIsPlayingRole(simulatedAgent, simulatedEnvironment, RActor.class);
					}
				}.repeatFinitely(10, exitTime - 1, 1));

				// Check if the agent is not in the simulatedEnvironment anymore and was
				// unregistered from it after he left.
				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertNotIn(simulatedAgent, simulatedEnvironment);
					}
				}.repeatInfinitely(exitTime + 1, 1));
			}
		};

		launchTester(tester, 200, testInfo);
	}

	/**
	 * Tests if agents are remove from this simulatedEnvironment when they leave the
	 * simulatedEnvironment.
	 */
	@ParameterizedTest
	@DisplayName("Agent exits and then re-enters the same simulatedEnvironment")
	@ValueSource(ints = { 30, 50, 100 })
	public void reEnter(int exitTime, TestInfo testInfo) throws Throwable {
		ExperimenterAgent tester = new ExperimenterAgent() {
			private SimulatedEnvironment simulatedEnvironment;
			private SimulatedAgent simulatedAgent;
			private final BigDecimal RE_ENTRY_TICK = BigDecimal.valueOf(200);

			@Override
			protected List<MAXAgent> setupScenario() {
				List<MAXAgent> agents = new ArrayList<>();

				// Set up the simulatedEnvironment
				simulatedEnvironment = new SimulatedEnvironment();
				agents.add(simulatedEnvironment);

				// Create an agent
				final Plan<SimulatedAgent> plan = new Plan<SimulatedAgent>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Arrays.asList(
								new ACTakeRole<SimulatedAgent>(simulatedEnvironment.getName(), RActor.class, null)
										.oneTime(0),
								new ACLeaveEnvironment<SimulatedAgent>(simulatedEnvironment.getName(), null)
										.oneTime(exitTime),
								new ACTakeRole<SimulatedAgent>(simulatedEnvironment.getName(), RActor.class, null)
										.oneTime(RE_ENTRY_TICK));
					}
				};
				simulatedAgent = new SimulatedAgent(plan);
				agents.add(simulatedAgent);

				return agents;
			}

			@Override
			protected void setupExperimenter() {
				// Check if the agent is in the simulatedEnvironment till the moment he leaves
				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertIsPlayingRole(simulatedAgent, simulatedEnvironment, RActor.class);
					}
				}.repeatFinitely(10, exitTime - 1, 1));

				// Check if the agent is not in the simulatedEnvironment anymore and was
				// unregistered from it after he left.
				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertNotIn(simulatedAgent, simulatedEnvironment);
					}
				}.repeatFinitely(exitTime + 1, RE_ENTRY_TICK.longValue() - 1, 1));

				// Checks if the agent is in the simulatedEnvironment and registered after he
				// came back
				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						assertIsPlayingRole(simulatedAgent, simulatedEnvironment, RActor.class);
					}
				}.repeatInfinitely(RE_ENTRY_TICK.longValue() + 1, 1));
			}
		};

		launchTester(tester, 300, testInfo);
	}

	/**
	 * Non regression test: if an agent leaves a role in the environment but is
	 * still playing at least one role in it, then the context shouldn't be removed.
	 *
	 * The experiment will start an agent with two roles, leave one and check if the
	 * agent has still a context. Then the agent will lose the second role, and it
	 * checks if the context has been removed.
	 *
	 */
	@Test
	public void contextNotRemovedTwoRolesLeaveOne(TestInfo testInfo) throws Throwable {
		final var experimenter = new ExperimenterAgent() {

			private SimulatedAgent agent;
			private SimulatedEnvironment env;

			@Override
			protected List<MAXAgent> setupScenario() {
				env = new SimulatedEnvironment();
				env.addAllowedRole(RDataInspector.class);

				final var plan = new Plan<>() {
					@Override
					public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
						return Arrays.asList(
								new ACTakeRole<SimulatedAgent>(env.getName(), RDataInspector.class, null).oneTime(1),
								new ACTakeRole<SimulatedAgent>(env.getName(), RActor.class, null).oneTime(1),
								new ACLeaveRole<SimulatedAgent>(env.getName(), RDataInspector.class, null).oneTime(2),
								new ACLeaveRole<SimulatedAgent>(env.getName(), RActor.class, null).oneTime(4));
					}
				};
				agent = new SimulatedAgent(plan);
				agent.addPlayableRole(RDataInspector.class);

				return Arrays.asList(env, agent);
			}

			@Override
			protected void setupExperimenter() {
				super.setupExperimenter();

				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						Assertions.assertTrue(agent.hasContext(env.getName()));
					}
				}.oneTime(3));

				schedule(new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						Assertions.assertFalse(agent.hasContext(env.getName()));
					}
				}.oneTime(5));
			}
		};

		launchTester(experimenter, 20, testInfo);
	}
}
