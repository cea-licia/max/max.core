/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * A customized {@link Handler} (for logging).
 *
 * It stores each log message (non formatted) and let you access it. Mainly used for testing
 * purpose.
 *
 * @since 1.2.0
 */
public class InMemoryHandler extends Handler {

    /** Store each call to publish. */
    private final List<String> logs;

    /**
     * Create a new instance. No stored logs for now.
     */
    public InMemoryHandler() {
        logs = new ArrayList<>();
    }

    @Override
    public void publish(LogRecord record) {
        logs.add(record.getMessage());
    }

    @Override
    public void flush() {
        // Nothing to do
    }

    @Override
    public void close() throws SecurityException {
        // Nothing to do
    }

    /**
     * Get all logs stored by this handler.
     *
     * @return a list (never {@code null}) of logs.
     */
    public List<String> getRecordedLogs() {
        return logs;
    }
}
