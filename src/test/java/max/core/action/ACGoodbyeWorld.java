/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.action;

import java.util.logging.Level;

import max.core.agent.MAXAgent;
import max.core.role.Role;

/**
 * Dummy action used in tests. Will log 'GoodBy World !' using agent's logger.
 *
 * @param <A> Action's owner type
 */
public class ACGoodbyeWorld<A extends MAXAgent> extends Action<A> {

    /**
     * Create a new {@link ACGoodbyeWorld} object.
     *
     * @param environment defines the environment to join
     * @param role and the role to play
     * @param owner the owner of this action
     */
    public ACGoodbyeWorld(String environment, Class<? extends Role> role, A owner) {
        super(environment, role, owner);
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new ACGoodbyeWorld<>(getEnvironment(), getRole(), getOwner());
    }
    
    @Override
    public String toString() {
    	return "GoodByAction";
    }

    @Override
    public void execute() {
    	this.getOwner().getLogger().log(Level.INFO, "GoodBy World !");
    }
}
