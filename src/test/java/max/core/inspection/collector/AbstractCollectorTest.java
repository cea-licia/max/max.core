/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.collector;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.atomic.AtomicBoolean;
import max.core.inspection.DataInspector;
import max.core.inspection.Exporter;
import org.junit.jupiter.api.Test;

/**
 * Collection of tests regarding {@link AbstractCollector}.
 *
 */
public class AbstractCollectorTest {

    /**
     * A dummy collector that does nothing.
     */
    private static final class DummyCollector extends AbstractCollector<String> {

        /**
         * Create a new instance without any exporters. Register itself to the owner.
         *
         * @param owner    inspector owning the collector
         */
        protected DummyCollector(DataInspector owner) {
            super(owner);
        }
    }

    /**
     * Test if metadata is not null, even if we don't supply it. Also check if it is mutable.
     */
    @Test
    public void makeSureMetadataIsNeverNullAndMutable() {
        final var inspector = new DataInspector();
        final var collector = new DummyCollector(inspector);

        assertNotNull(collector.getMetadata());
        assertDoesNotThrow(() -> collector.getMetadata().put("Toto", "Titi"));
    }

    /**
     * Check if {@link Exporter#activate()}, {@link Exporter#export(Object)} and {@link Exporter#end()} are called
     * at the right time.
     */
    @Test
    public void exporterLifeCycleIsGood() {
        final var activated = new AtomicBoolean(false);
        final var ended = new AtomicBoolean(false);
        final var exported = new AtomicBoolean(false);

        final var inspector = new DataInspector();
        final var collector = new DummyCollector(inspector);

        collector.addExporter(new Exporter<>() {
            @Override
            public void export(String data) {
                exported.set(true);
            }

            @Override
            public void activate() {
                activated.set(true);
            }

            @Override
            public void end() {
                ended.set(true);
            }
        });

        collector.activate();
        assertTrue(activated.compareAndExchange(true, false));
        assertFalse(exported.get());
        assertFalse(ended.get());

        collector.export("");
        assertFalse(activated.get());
        assertTrue(exported.compareAndExchange(true, false));
        assertFalse(ended.get());

        collector.end();
        assertFalse(activated.get());
        assertFalse(exported.get());
        assertTrue(ended.compareAndExchange(true, false));
    }

}
