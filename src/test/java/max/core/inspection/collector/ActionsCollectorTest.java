/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.collector;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import max.core.ExperimenterAction;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.inspection.DataInspector;
import max.core.inspection.exporter.StringExporter;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * Test class to make sure {@link ActionsCollector} is behaving well.
 *
 */
public class ActionsCollectorTest {

    @BeforeEach
    public void beforeEach(@TempDir Path tempDir) {
        clearParameters();
        setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
        setParameter("MAX_CORE_UI_MODE", "Silent");
        setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
    }

    /**
     *  Create a simulation with an inspector running the {@link ActionsCollector}.
     *
     *  It should collect its own action each tick, and that is what we are checking.
     */
    @Test
    public void ensureActionsAreCollected(TestInfo testInfo) throws Throwable {
        // Storage for the exporter
        final var storage = new StringBuilder();

        // Simulation duration
        final var experimentDuration = 20;

        final var tester = new ExperimenterAgent() {
            @Override
            protected List<MAXAgent> setupScenario() {
                return Collections.emptyList();  // No agents this time
            }

            @Override
            protected List<DataInspector> setupDataInspectors() {
                // Create inspector
                final var inspector = new DataInspector();

                // Create collector
                final var collector = new ActionsCollector(inspector);
                collector.addExporter(new StringExporter(storage));

                return Collections.singletonList(inspector);
            }

            @Override
            protected void setupExperimenter() {
                schedule(new ActionActivator<>(
                        ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(experimentDuration)),
                        new ExperimenterAction<>(this) {
                            @Override
                            public void execute() {
                                final var collectedData = storage.toString().split(System.lineSeparator());
                                final var actionsByTick = new HashMap<Long, List<String>>();

                                for (final var data : collectedData) {
                                    final var tick = Long.parseLong(data.split(";")[0]);
                                    final var action = data.split(";")[1];

                                    // First column is a tick
                                    assertTrue(0 <= tick && tick <= experimentDuration);
                                    actionsByTick.computeIfAbsent(tick, k -> new ArrayList<>()).add(action);
                                }

                                // At least one action per tick during the entire simulation
                                assertEquals(experimentDuration, actionsByTick.keySet().size());

                                for (final var actions : actionsByTick.values()) {
                                    // At least one action is our ActionsCollector
                                    assertTrue(actions.stream().anyMatch(s -> s.startsWith("ActionsCollector")));
                                }
                            }
                        }
                ));
                super.setupExperimenter();  // The last action is to quit the simulation
            }
        };

        launchTester(tester, experimentDuration, testInfo);
    }

}
