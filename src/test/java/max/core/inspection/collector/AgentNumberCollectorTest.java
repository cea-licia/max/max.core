/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.collector;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import max.core.ExperimenterAction;
import max.core.action.ACLeaveEnvironment;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;
import max.core.inspection.DataInspector;
import max.core.inspection.exporter.StringExporter;
import max.core.role.RActor;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * Test class to make sure {@link AgentNumberCollector} is behaving well.
 *
 */
public class AgentNumberCollectorTest {

    @BeforeEach
    public void beforeEach(@TempDir Path tempDir) {
        clearParameters();
        setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
        setParameter("MAX_CORE_UI_MODE", "Silent");
        setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
    }

    /**
     *  Create a simulation with an inspector running the {@link AgentNumberCollector}.
     *
     *  An agent will join and then leave the environment during the simulation.
     */
    @Test
    public void ensureAgentNumberIsCorrect(TestInfo testInfo) throws Throwable {
        // Storage for the exporter
        final var storage = new StringBuilder();

        // Simulation duration
        final var experimentDuration = 20;
        final var agentJoinsAt = 5L;
        final var agentLeavesAt = 15L;

        final var tester = new ExperimenterAgent() {
            private SimulatedEnvironment env;

            @Override
            protected List<MAXAgent> setupScenario() {
                env = new SimulatedEnvironment();
                final var plan = new Plan<>() {
                    @Override
                    public List<ActionActivator<SimulatedAgent>> getInitialPlan() {
                        return Arrays.asList(
                                new ActionActivator<>(
                                        ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(agentJoinsAt)),
                                        new ACTakeRole<>(env.getName(), RActor.class, null)
                                ),
                                new ActionActivator<>(
                                        ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(agentLeavesAt)),
                                        new ACLeaveEnvironment<>(env.getName(), null)
                                )
                        );
                    }
                };

                return Arrays.asList(env, new SimulatedAgent(plan));
            }

            @Override
            protected List<DataInspector> setupDataInspectors() {
                // Create inspector
                final var inspector = new DataInspector();

                // Create collector
                final var collector = new AgentNumberCollector(inspector, env.getName());
                collector.addExporter(new StringExporter(storage));

                return Collections.singletonList(inspector);
            }

            @Override
            protected void setupExperimenter() {
                schedule(new ActionActivator<>(
                        ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(experimentDuration)),
                        new ExperimenterAction<>(this) {
                            @Override
                            public void execute() {
                                final var collectedData = storage.toString().split(System.lineSeparator());

                                for (final var data : collectedData) {
                                    final var tick = Long.parseLong(data.split(";")[0]);
                                    final var action = Long.parseLong(data.split(";")[1]);

                                    // First column is a tick
                                    assertTrue(0 <= tick && tick <= experimentDuration);

                                    // Second column contains 1 only if tick is in [agentJoinsAt; agentLeavesAt[
                                    assertEquals((agentJoinsAt <= tick && tick < agentLeavesAt) ? 1 : 0, action);
                                }
                            }
                        }
                ));
                super.setupExperimenter();  // The last action is to quit the simulation
            }
        };

        launchTester(tester, experimentDuration, testInfo);
    }

}
