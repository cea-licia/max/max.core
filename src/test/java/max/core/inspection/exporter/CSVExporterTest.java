/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.exporter;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import max.core.inspection.DataInspector;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

/**
 * Collection of tests related to {@link CSVFileExporter}.
 *
 */
public class CSVExporterTest {

    /**
     * Make sure metadata is exported once (the first time data is exported).
     *
     * @param tempDir Temporary folder created by JUnit
     * @throws IOException If we can't read the produced file (test failure)
     */
    @Test
    public void metadataAreExportedTheFirstTime(@TempDir Path tempDir) throws IOException {
        final var inspector = new DataInspector();
        final var file = tempDir.resolve("export.csv");
        final var metadata = Map.of(
                "title", "Some random data",
                "header", Arrays.asList("word1", "word2"),
                "chart-type", "LINE_CHART"
        );
        final var exporter = new CSVFileExporter(file, metadata, ";");


        exporter.activate();
        exporter.export(Arrays.asList("Hello", "World"));
        exporter.export(Arrays.asList("Other", "Data"));
        exporter.end();

        // Verify metadata
        final var lines = Files.readAllLines(file);

        assertTrue(lines.containsAll(metadata.entrySet().stream()
                .filter(e -> !CSVFileExporter.HEADER_ENTRY.equals(e.getKey()))
                .map(e -> "# " + e.getKey() + ": " + e.getValue())
                .collect(Collectors.toList())
        ));
        assertTrue(lines.contains(String.join(";", List.of("word1", "word2"))));

        assertFalse(lines.contains("# ignored: true"));
    }

    /**
     * Make sure all exported data is written in the file.
     *
     * @param tempDir Temporary folder created by JUnit
     * @throws IOException If we can't read the produced file (test failure)
     */
    @Test
    public void dataIsWritten(@TempDir Path tempDir) throws IOException {
        final var inspector = new DataInspector();
        final var file = tempDir.resolve("export.csv");
        final Map<String, Object> metadata = Collections.emptyMap();
        final var exporter = new CSVFileExporter(file, metadata, ";");

        final var data = Arrays.asList(
                Arrays.asList("Hello", "World"),
                Arrays.asList("Other", "Data"),
                Arrays.asList("One", "More")
        );

        exporter.activate();
        data.forEach(exporter::export);
        exporter.end();

        // Verify content
        final var lines = Files.readAllLines(file);
        assertTrue(lines.containsAll(data.stream()
                .map(d -> String.join(";", d))
                .collect(Collectors.toList()))
        );
    }
}
