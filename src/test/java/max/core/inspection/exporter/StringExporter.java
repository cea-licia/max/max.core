/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.exporter;

import java.util.List;
import java.util.stream.Collectors;
import max.core.inspection.Exporter;

/**
 * A custom {@link Exporter} storing data in a {@link StringBuilder}.
 *
 * @since 1.2.0
 */
public class StringExporter implements Exporter<List<?>> {

    /** Storage. */
    private final StringBuilder storage;

    /**
     * Create a new {@link StringExporter} instance.
     *
     * @param storage storage object.
     */
    public StringExporter(StringBuilder storage) {
        this.storage = storage;
    }

    @Override
    public void export(List<?> data) {
        storage.append(data.stream()
                .map(Object::toString)
                .collect(Collectors.joining(";"))
        ).append(System.lineSeparator());
    }

    @Override
    public void activate() {}

    @Override
    public void end() {}
}