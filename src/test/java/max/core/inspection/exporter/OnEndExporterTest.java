/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.exporter;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.atomic.AtomicBoolean;
import max.core.inspection.Exporter;
import org.junit.jupiter.api.Test;

/**
 * Collection of tests related to {@link OnEndExporter}.
 *
 */
public class OnEndExporterTest {

    /**
     * A simple exporter that let you verify if each three methods have been called.
     *
     * @author Pierre Dubaillay
     */
    private static final class VerifiableExporter implements Exporter<String> {

        private final AtomicBoolean exported;
        private final AtomicBoolean activated;
        private final AtomicBoolean ended;

        /**
         * Create a new instance.
         */
        private VerifiableExporter() {
            exported = new AtomicBoolean();
            activated = new AtomicBoolean();
            ended = new AtomicBoolean();
        }

        @Override
        public void export(String data) {
            exported.set(true);
        }

        @Override
        public void activate() {
            activated.set(true);
        }

        @Override
        public void end() {
            ended.set(true);
        }
    }

    /**
     * Make sure the wrapped exporter is working only at the end of the simulation, ie when the
     * {@link OnEndExporter#end()} method is called.
     */
    @Test
    public void dataIsExporterAtTheEnd() {
        final var wrapped = new VerifiableExporter();
        final var exporter = new OnEndExporter<>(wrapped);

        // Activate exporter: wrapped shouldn't be activated
        exporter.activate();
        assertFalse(wrapped.activated.get());
        assertFalse(wrapped.exported.get());
        assertFalse(wrapped.ended.get());

        // Export data: wrapped shouldn't be activated
        exporter.export("Hello world");
        assertFalse(wrapped.activated.get());
        assertFalse(wrapped.exported.get());
        assertFalse(wrapped.ended.get());

        // End exporter: it should activate, export and end the wrapped exporter
        exporter.end();
        assertTrue(wrapped.activated.get());
        assertTrue(wrapped.exported.get());
        assertTrue(wrapped.ended.get());
    }

}