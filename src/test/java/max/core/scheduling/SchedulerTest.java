/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling;

import static max.core.MAXParameters.clearParameters;
import static max.core.MAXParameters.setParameter;
import static max.core.test.TestMain.launchTester;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.math.BigDecimal;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import max.core.Community;
import max.core.ExperimenterAction;
import max.core.MAXParameters;
import max.core.action.Action;
import max.core.agent.ExperimenterAgent;
import max.core.agent.MAXAgent;
import max.core.role.RExperimenter;
import max.core.role.RGroupManager;
import max.core.role.RScheduler;
import max.core.role.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.io.TempDir;

/**
 * Tests the SchedulerAgent
 * 
 * @author Önder Gürcan
 * @author Mohamed Aimen Djari
 */
public class SchedulerTest {

	/**
	 * Setup simulation parameters before each test.
	 *
	 * @param tempDir Temporary folder created by JUnit
	 */
	@BeforeEach
	public void before(@TempDir Path tempDir) {
		// clear all parameter values
		clearParameters();
		// set simulation step to 1
		setParameter("MAX_CORE_SIMULATION_STEP", BigDecimal.ONE.toString());
		// set UI mode to Silent
		setParameter("MAX_CORE_UI_MODE", "Silent");
		setParameter("MAX_CORE_RESULTS_FOLDER_NAME", tempDir.toString());
	}

	@Test
	public void activate(TestInfo testInfo) throws Throwable {
		ExperimenterAgent experimenterAgent = new ExperimenterAgent() {
			@Override
			protected List<MAXAgent> setupScenario() {
				return new ArrayList<>();
			}

			@Override
			protected void setupExperimenter() {
				// Schedule verification of the initialization of the new community
				ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.TEN,
						BigDecimal.ONE);
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						// Community is COMMUNITY
						TreeSet<String> existingCommunities = getExistingCommunities();
						// COMMUNITY from MAX, local from MaDKit
						assertEquals(2, existingCommunities.size());
						// the first one is COMMUNITY
						Assertions.assertEquals(Community.COMMUNITY, existingCommunities.first());

						// environment
						TreeSet<String> existingEnvironments = getExistingGroups(Community.COMMUNITY);
						assertEquals(1, existingEnvironments.size());
						assertEquals(Community.SIMULATION_ENVIRONMENT, existingEnvironments.first());

						// Roles are EXPERIMENTER and SCHEDULER
						Set<Class<? extends Role>> existingRoles = getExistingRoles(
								Community.SIMULATION_ENVIRONMENT);
						// EXPERIMENTER and SCHEDULER from MAX, manager from MaDKit
						assertEquals(3, existingRoles.size());
						assertTrue(existingRoles.contains(RExperimenter.class));
						assertTrue(existingRoles.contains(RScheduler.class));
						// assertTrue(existingRoles.contains("manager"));
					}
				}));
			}
		};

		launchTester(experimenterAgent, 100,testInfo);
	}

	@Test
	public void schedule(TestInfo testInfo) throws Throwable {
		ExperimenterAgent experimenterAgent = new ExperimenterAgent() {
			@Override
			protected List<MAXAgent> setupScenario() {
				return new ArrayList<>();
			}

			@Override
			protected void setupExperimenter() {
				List<String> str = new ArrayList<>();

				// Create first scheduling containing "A0" at 10
				ActivationSchedule schedule = ActivationScheduleFactory.createOneTime(BigDecimal.TEN);
				schedule(new ActionActivator<>(schedule, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						str.add("A0");
					}
				}));

				// Create second scheduling containing "A1" at 10
				ActivationSchedule schedule1 = ActivationScheduleFactory.createOneTime(BigDecimal.TEN);
				schedule(new ActionActivator<>(schedule1, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						str.add("A1");
					}
				}));

				// Create third scheduling containing "A2" at 11
				ActivationSchedule schedule2 = ActivationScheduleFactory.createOneTime(BigDecimal.valueOf(11));
				schedule(new ActionActivator<>(schedule2, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						str.add("A2");
					}
				}));

				// Create fourth scheduling making sure the first three have been executed
				// before
				ActivationSchedule schedule3 = ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.valueOf(11),
						BigDecimal.ONE);
				schedule(new ActionActivator<>(schedule3, new ExperimenterAction<>(this) {
					@Override
					public void execute() {
						for (int i = 0; i < str.size(); i++) {
							assertEquals("A" + i, str.get(i));
						}
					}
				}));
			}
		};

		launchTester(experimenterAgent, 300, testInfo);
	}

  /**
   * Ensures that if an exception is thrown by an action, the test fails.
   *
   * <p>The simulation is simple: we create a tester and run two actions:
   *
   * <ul>
   *   <li>one throwing a {@link NullPointerException}</li>
	 *   <li>on throwing (later) a {@link IndexOutOfBoundsException}</li>
   * </ul>
   *
   * The test ensures the NPE is detected and thrown.
   */
  @Test
  public void exceptionsInActionsAreDetected(TestInfo testInfo) throws Throwable {
    final var tester =
        new ExperimenterAgent() {

          @Override
          protected List<MAXAgent> setupScenario() {
            return Collections.emptyList();
          }

          @Override
          protected void setupExperimenter() {
            schedule(
                new ActionActivator<>(
                    ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
                    new ExperimenterAction<>(this) {

                      @Override
                      public void execute() {
                        throw new NullPointerException();
                      }
                    }));
            super.setupExperimenter();
						schedule(
								new ActionActivator<>(
										ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
										new ExperimenterAction<>(this) {

											@Override
											public void execute() {
												throw new IndexOutOfBoundsException();
											}
										}));
						super.setupExperimenter();
          }
        };

    assertThrows(
				NullPointerException.class,
				() -> launchTester(tester, 5, testInfo)
		);
  }

  /**
   * Checks if an action can be executed by an agent even if it doesn't play the requested role,
   * meaning role check is disabled.
   *
   * @param testInfo injected by JUnit
   * @throws Throwable Any error that may occur during the simulation
   */
  @Test
  public void roleCheckFlagIsFalse(TestInfo testInfo) throws Throwable {
		MAXParameters.setParameter("MAX_CORE_ALWAYS_VERIFY_ROLE_PLAYING", "false");
		final var executed = new AtomicBoolean();
		final var tester = new ExperimenterAgent() {

			@Override
			protected List<MAXAgent> setupScenario() {
				return List.of();
			}

			@Override
			protected void setupExperimenter() {
				schedule(new ActionActivator<>(
						ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
						new Action<>(Community.SIMULATION_ENVIRONMENT, RGroupManager.class, this) {
							@Override
							public void execute() {
								executed.set(true);
							}
						}
				));
				schedule(new ActionActivator<>(
						ActivationScheduleFactory.ONE_SHOT_AT_STARTUP,
						new ExperimenterAction<>(this) {
							@Override
							public void execute() {
								assertTrue(executed.get());
							}
						}
				));
			}
		};

		launchTester(tester, 5, testInfo);
	}

	/**
	 * Checks if default check role flag value is {@code true}, meaning role check is enabled.
	 *
	 * @param testInfo injected by JUnit
	 * @throws Throwable Any error that may occur during the simulation
	 */
	@Test
	public void roleCheckFlagIsDefault(TestInfo testInfo) throws Throwable {
		final var tester = new ExperimenterAgent() {

			@Override
			protected List<MAXAgent> setupScenario() {
				return List.of();
			}

			@Override
			protected void setupExperimenter() {
				schedule(new ActionActivator<>(
						ActivationScheduleFactory.ONE_SHOT_AT_ONE,
						new Action<>(Community.SIMULATION_ENVIRONMENT, RGroupManager.class, this) {
							@Override
							public void execute() {
								fail("Action is executed.");
							}
						}
				));
			}
		};

		launchTester(tester, 5, testInfo);
	}
}
