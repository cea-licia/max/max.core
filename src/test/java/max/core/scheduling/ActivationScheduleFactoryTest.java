/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;

import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

public class ActivationScheduleFactoryTest {

	/**
	 * Tests {@link ActivationScheduleFactory#createOneTime(BigDecimal)}
	 *
	 * Make sure:
	 * <ul>
	 *     <li>Start time has same value as provided parameter</li>
	 *     <li>Delta time is 0</li>
	 *     <li>Max repetition count is 0</li>
	 * </ul>
	 */
	@ParameterizedTest()
	@ValueSource(ints = {0, 5, 10, 50, 500, 1000, 10000})
	public void createOneTime(int startTime) {
		final BigDecimal expected = BigDecimal.valueOf(startTime);
		final ActivationSchedule schedule = ActivationScheduleFactory.createOneTime(expected);

		assertEquals(expected, schedule.getStartTime());
		assertEquals(BigDecimal.ZERO, schedule.getDeltaTime());
		assertEquals(1, schedule.getMaximumRepetitionCount());
	}

	@ParameterizedTest
	@CsvSource(value = {"0,1", "0, 20", "12, 12", "200, 5"})
	public void createRepeatingInfinitely(final String start, final String delta) {
		final BigDecimal startT = new BigDecimal(start);
		final BigDecimal deltaT = new BigDecimal(delta);
		final ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingInfinitely(startT, deltaT);

		assertEquals(startT, schedule.getStartTime());
		assertEquals(deltaT, schedule.getDeltaTime());
		assertEquals(Long.MAX_VALUE, schedule.getMaximumRepetitionCount());
	}

	@ParameterizedTest
	@CsvSource(value = {"0,10,1, 11", "3,12,2,5", "2,15,8,2"})
	public void createRepeatingFinitely(final String start, final String end, final String delta,
			final String expected) {
		final BigDecimal startT = new BigDecimal(start);
		final BigDecimal endT = new BigDecimal(end);
		final BigDecimal deltaT = new BigDecimal(delta);
		final long expectedL = Long.parseLong(expected);
		final ActivationSchedule schedule = ActivationScheduleFactory.createRepeatingFinitely(startT, endT, deltaT);

		assertEquals(startT, schedule.getStartTime());
		assertEquals(deltaT, schedule.getDeltaTime());
		assertEquals(expectedL, schedule.getMaximumRepetitionCount());
	}

	@Test
	public void checkPoolMechanism() {
		final BigDecimal startT = BigDecimal.ONE;
		final BigDecimal deltaT = BigDecimal.ONE;
		final ActivationSchedule origin = ActivationScheduleFactory.ONE_ONE_INDEF;

		assertSame(origin, ActivationScheduleFactory.createRepeatingInfinitely(startT, deltaT));
		assertSame(origin, ActivationScheduleFactory.createRepeatingInfinitely(startT, deltaT));
		assertSame(origin, ActivationScheduleFactory.createRepeatingInfinitely(startT, deltaT));
		assertSame(origin, ActivationScheduleFactory.createRepeatingInfinitely(startT, deltaT));

		assertNotSame(origin, ActivationScheduleFactory.createRepeatingInfinitely(BigDecimal.ZERO, deltaT));
	}
}