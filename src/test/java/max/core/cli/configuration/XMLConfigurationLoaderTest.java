/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli.configuration;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.net.URISyntaxException;
import max.core.MAXParameters;
import org.junit.jupiter.api.Test;

/**
 * Tests the XML loader.
 *
 * @author Pierre Dubaillay
 */
public class XMLConfigurationLoaderTest {

  /** Path to resources. */
  private static final String VALID_XML_FILE = "max/core/cli/configuration/validXML.xml";

  private static final String INVALID_XML_FILE = "max/core/cli/configuration/invalidXML.xml";

  /**
   * Loads a valid XML and makes sure all information is loaded in {@link MAXParameters}.
   *
   * @throws ConfigurationLoadingException shouldn't happen unless the test is a failure
   * @throws URISyntaxException can't load the XML file
   */
  @Test
  public void loadValidXML() throws ConfigurationLoadingException, URISyntaxException {
    // Get the valid file
    final var validURL = Thread.currentThread().getContextClassLoader().getResource(VALID_XML_FILE);

    // Create the loader and load content of the XML file
    final var loader = new XMLConfigurationLoader(new File(validURL.toURI()).getPath());
    loader.load();

    // Assertions
    assertEquals(
        "some.kind.of.class.name", MAXParameters.getParameter("MAX_CORE_EXPERIMENTER_NAME"));
    assertEquals("1", MAXParameters.getParameter("MAX_CORE_SIMULATION_STEP"));
    assertEquals("100", MAXParameters.getParameter("MAX_CORE_TIMEOUT_TICK"));
    assertEquals("GUI", MAXParameters.getParameter("MAX_CORE_UI_MODE"));
    assertEquals("custom", MAXParameters.getParameter("CUSTOM"));
  }

  /**
   * Tries to load an invalid XML configuration file and makes sure a {@link
   * ConfigurationLoadingException} is thrown.
   *
   * @throws URISyntaxException can't load the XML file
   */
  @Test
  public void loadInvalidXML() throws URISyntaxException {
    // Get the invalid file
    final var invalidURL =
        Thread.currentThread().getContextClassLoader().getResource(INVALID_XML_FILE);

    // Create the loader and load content of the XML file
    final var loader = new XMLConfigurationLoader(new File(invalidURL.toURI()).getPath());
    assertThrows(ConfigurationLoadingException.class, loader::load);
  }
}
