@startuml
'Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
'
'This program and the accompanying materials are made
'available under the terms of the Eclipse Public License 2.0
'which is available at https://www.eclipse.org/legal/epl-2.0/
'
'SPDX-License-Identifier: EPL-2.0
title CLI class diagram

package cli {

  abstract class AbstractCommand {
    + void execute()
    # Logger getLogger()
  }
  hide AbstractCommand attributes

  class RunCommand extends AbstractCommand {
    - {static} Logger LOGGER
    - {static} String EXPERIMENTER_CLASS_KEY
    - Path configFile
    - void runExperiment(ExperimenterAgent)
    - boolean loadConfiguration()
    - void clean(Path)
  }

  class PlotCommand extends AbstractCommand {
    - {static} Logger LOGGER
    - {static} String DEFAULT_OUT_FOLDER
    - {static} String DEFAULT_LOG_FOLDER
    - {static} Path DEFAULT_R_PATH
    + {static} String PLOT_SCRIPT_LOCATION
    - Path inputPath
    - Path outputPath
    - Path rPath
    - Path logsFolderPath
    - void plot(Path fileIn, Path fileOut)
    - Path buildOutputFromInput(Path)
    - Path buildLogFromInput(Path)
    - Path extractInternalPlotRScript()
    - void setupDefaults()
    - void createOutputFolder()
    - void createLogsFolder()
    - void createFolder(Path)
  }

  class CLIMain {
    + {static} void main(String[] args)
    - {static} void executeCommand(AbstractCommand)
  }
  CLIMain -> RunCommand
  CLIMain -> PlotCommand
  hide CLIMain attributes

  class MainCommand {
    - boolean helpFlag
  }
  MainCommand --+ CLIMain
  hide MainCommand methods

  class CommandException
  AbstractCommand ..> CommandException
  hide CommandException members

}
@enduml

@startuml
hide footbox
title Simulation lifecycle using CLI

participant CLIMain
participant PlotCommand
participant RunCommand
participant ExperimenterLauncher
participant ExperimenterAgent

?-> CLIMain ++: main(str: cla)
CLIMain -> RunCommand **: new()
CLIMain -> PlotCommand **: new()
CLIMain -> CLIMain: parse(cla)

alt Command is run
  CLIMain -> RunCommand ++: execute()
  RunCommand -> RunCommand : loadConfiguration()
  RunCommand -> ExperimenterLauncher ++: fromClassName(str: config.expAgtClassName)
  ExperimenterLauncher -> ExperimenterAgent **: new()
  ExperimenterLauncher --> RunCommand --: experimenter

  alt clean flag is set
    RunCommand -> RunCommand : clean(previous)
  end

  RunCommand -> ExperimenterLauncher ++: launchExperimenter(experimenter)
  ExperimenterLauncher --> RunCommand --: void
  RunCommand --> CLIMain --: void

else Command is plot
  CLIMain -> PlotCommand ++: execute()
  PlotCommand -> PlotCommand : setup defaults and create folders
  PlotCommand -> PlotCommand : extract plot script
  loop for file in inputPath
    PlotCommand -> PlotCommand : plot(file, outputPath / filename, logsFolderPath / filename)
  end
  PlotCommand --> CLIMain --: void
end
?<-- CLIMain --: void

@enduml

@startuml

hide footbox
title Simulation lifecycle using Test

participant TestCase
participant ExperimenterLauncher

?-> TestCase ++: launchTester(expAgent, ...)
TestCase -> ExperimenterLauncher ++: launchExperimenter(expAgent)
ExperimenterLauncher --> TestCase --: void
?<- TestCase --: void

@enduml

@startuml

hide footbox
title Simulation lifecycle using OpenMOLE

participant OpenMOLEMain
participant ExperimenterLauncher
participant ExperimenterAgent

?-> OpenMOLEMain++: launchSimulation(?)
OpenMOLEMain-> ExperimenterLauncher ++: fromClassName(str: expAgtClassName)
ExperimenterLauncher -> ExperimenterAgent **: new()
ExperimenterLauncher --> OpenMOLEMain--: expAgent
OpenMOLEMain -> ExperimenterLauncher ++: launchExperimenter(expAgent)
ExperimenterLauncher --> OpenMOLEMain--: void
?<- OpenMOLEMain--: void

@enduml