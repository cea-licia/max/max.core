/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import max.core.agent.MAXAgent;
import max.core.agent.SimulatedEnvironment;
import max.core.role.Role;

/**
 * A collection of assertion methods to test MAX models.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 * @author Önder Gürcan
 */
public final class Assertions {

  // Hide default public implicit constructor by a private one.
  private Assertions() {
    // Empty
  }

  /**
   * Asserts the agent is playing the given role in the given environment.
   *
   * <p>Check two things:
   *
   * <ul>
   *   <li>The agent has an address linked to the provided environment/role couple
   *   <li>The environment is aware of that address
   * </ul>
   *
   * @param agent the agent supposed to play the role
   * @param environment where the agent is supposed to play the role
   * @param role the role the agent is supposed to play
   */
  public static void assertIsPlayingRole(
      MAXAgent agent, SimulatedEnvironment environment, Class<? extends Role> role) {
    final var address = agent.getAgentAddressIn(environment.getName(), role);
    assertNotNull(address);
    assertTrue(environment.hasAgent(address));
  }

  /**
   * Asserts the agent is playing the given roles in the given environment.
   *
   * <p>Check two things:
   *
   * <ul>
   *   <li>The agent has an address linked to the provided environment/role couples
   *   <li>The environment is aware of each address
   * </ul>
   *
   * @param agent the agent supposed to play the role
   * @param environment where the agent is supposed to play the role
   * @param roles the roles the agent is supposed to play
   */
  public static void assertIsPlayingRoles(
      MAXAgent agent, SimulatedEnvironment environment, Class<? extends Role>... roles) {
    for (final var role : roles) {
      assertIsPlayingRole(agent, environment, role);
    }
  }

  /**
   * Asserts the agent is not playing the given role, in the given environment.
   *
   * @param agent the agent to test
   * @param envName the environment name
   * @param role the role
   */
  public static void assertIsNotPlayingRole(
      MAXAgent agent, String envName, Class<? extends Role> role) {
    assertNull(agent.getAgentAddressIn(envName, role));
  }

  /**
   * Asserts the agent is not playing the given roles, in the given environment.
   *
   * @param agent the agent to test
   * @param envName the environment name
   * @param roles the roles
   */
  public static void assertIsNotPlayingRoles(
      MAXAgent agent, String envName, Class<? extends Role>... roles) {
    for (final var role : roles) {
      assertNull(agent.getAgentAddressIn(envName, role));
    }
  }

  /**
   * Asserts the agent is not playing the given role, in the given environment.
   *
   * @param agent the agent to test
   * @param environment the environment name
   * @param role the role
   * @see #assertIsNotPlayingRole(MAXAgent, String, Class)
   */
  public static void assertIsNotPlayingRole(
      MAXAgent agent, SimulatedEnvironment environment, Class<? extends Role> role) {
    assertIsNotPlayingRole(agent, environment.getName(), role);
  }

  /**
   * Asserts the given agent is inside the provided environment, i.e. it plays at least one role in
   * that environment.
   *
   * <p>Checks two things:
   *
   * <ul>
   *   <li>The agent is playing at least one role in the given environment
   *   <li>The environment is aware of the agent
   * </ul>
   *
   * @param agent agent to test
   * @param environment the environment
   */
  public static void assertIn(MAXAgent agent, SimulatedEnvironment environment) {
    final var roles = agent.getMyRoles(environment.getName());
    assertNotEquals(0, roles.size());
    roles.stream()
        .filter(environment::allowsRole)
        .forEach(
            role -> environment.hasAgent(agent.getAgentAddressIn(environment.getName(), role)));
  }

  /**
   * Asserts the agent is not in the given environment, i.e. it doesn't play any role in that
   * environment.
   *
   * @param agent the agent
   * @param environmentName the environment name
   */
  public static void assertNotIn(MAXAgent agent, String environmentName) {
    assertEquals(0, agent.getMyRoles(environmentName).size());
  }

  /**
   * Asserts the agent is not in the given environment, i.e. it doesn't play any role in that
   * environment.
   *
   * @param agent the agent
   * @param environment the environment
   * @see #assertNotIn(MAXAgent, String)
   */
  public static void assertNotIn(MAXAgent agent, SimulatedEnvironment environment) {
    assertEquals(0, agent.getMyRoles(environment.getName()).size());
  }

  /**
   * Asserts the agent is able to play the given role (agent configuration test).
   *
   * @param agent the agent
   * @param role the role
   */
  public static void assertCanPlay(MAXAgent agent, Class<? extends Role> role) {
    assertTrue(agent.canPlayRole(role));
  }

  /**
   * Asserts the given agent can't play the given role (configuration test).
   *
   * @param agent the agent
   * @param role the role
   */
  public static void assertCannotPlay(MAXAgent agent, Class<? extends Role> role) {
    assertFalse(agent.canPlayRole(role));
  }

  /**
   * Asserts the given environment allows agents to play the given role.
   *
   * @param environment the environment
   * @param role the role that should be allowed
   */
  public static void assertAllows(SimulatedEnvironment environment, Class<? extends Role> role) {
    assertTrue(environment.allowsRole(role));
  }

  /**
   * Asserts the given environment disallows to play the given role.
   *
   * @param environment the environment
   * @param role the role that shouldn't be allowed
   */
  public static void assertDisallows(SimulatedEnvironment environment, Class<? extends Role> role) {
    assertFalse(environment.allowsRole(role));
  }
}
