/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.test;

import java.util.Comparator;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.logging.Level;
import java.util.logging.Logger;
import max.core.agent.ExperimenterAgent;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ErrorHandler;
import max.core.scheduling.SchedulerAgent;
import org.junit.jupiter.api.TestInfo;

/**
 * A utility class for launching testing simulations.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public final class TestMain {

  // Private empty constructor to hide the implicit one.
  private TestMain() {}

  /**
   * Launches the simulation. If one or several exceptions occur during the simulation, this method
   * will throw the first one at the end of the simulation.
   *
   * @param tester the experimenter agent to launch
   * @param simulationDuration how many ticks the simulation will last. Overrides
   *     MAX_CORE_TIMEOUT_TICK.
   * @param info Test information
   * @throws Throwable any exception or error that may occur during the simulation
   */
  public static void launchTester(
      ExperimenterAgent tester, int simulationDuration, TestInfo info) throws Throwable {
    displayTestName(tester, info);

    final var scheduler = tester.getScheduler();
    final var errorOccurred = new AtomicReference<Throwable>();

    scheduler.setErrorHandler(new TestingErrorHandler(errorOccurred));
    tester.setDuration(simulationDuration);

    ExperimenterAgent.launchExperimenter(tester);
    waitUntilSimulationEnds(tester);

    final var error = errorOccurred.get();
    if (error != null) {
      throw error;
    }
  }

  /**
   * Displays, using the tester logger, the current test name.
   *
   * @param tester tester
   * @param info information provided by JUnit about the current test
   */
  private static void displayTestName(ExperimenterAgent tester, TestInfo info) {
    info.getTestMethod()
        .ifPresent(
            method ->
                tester
                    .getLogger()
                    .info(
                        "Test: " + method.getDeclaringClass().getName() + "#" + method.getName()));
  }

  /**
   * Given the provided experimenter, waits until the simulation ends. This is a blocking operation.
   *
   * @param tester experimenter to wait
   */
  private static void waitUntilSimulationEnds(ExperimenterAgent tester) {
    final var barrier = tester.isSimulationEnded();
    synchronized (barrier) {
      while (!barrier.get()) {
        try {
          barrier.wait();
        } catch (InterruptedException e) {
          Logger.getAnonymousLogger().severe("Test interrupted.");
          Thread.currentThread().interrupt();
        }
      }
    }
  }

  /**
   * A custom error handler used by the testing framework.
   *
   * <p>This handler will:
   *
   * <ul>
   *   <li>Terminates failed activators
   *   <li>Logs errors
   *   <li>Set the provided error flag to the first occurred error
   * </ul>
   */
  private static final class TestingErrorHandler implements ErrorHandler {

    /** User error flag. */
    private final AtomicReference<Throwable> firstException;

    /**
     * Creates a new error handler, which will set the provided exception flog to the first error
     * that occurs during the simulation (if any).
     *
     * @param exceptionFlag the user flag
     */
    private TestingErrorHandler(AtomicReference<Throwable> exceptionFlag) {
      this.firstException = exceptionFlag;
    }

    @Override
    public void handleExceptions(
        SchedulerAgent scheduler, Map<ActionActivator<?>, Throwable> exceptions) {
      exceptions.forEach(
          (act, error) -> {
            scheduler.getLogger().log(Level.SEVERE, error, () -> "Activator=" + act);
            act.finish();
          });
      if (firstException.get() == null) {
        firstException.set(
            exceptions.get(
                exceptions.keySet().stream()
                    .min(Comparator.comparingInt(ActionActivator::getId))
                    .get()));
      }
    }
  }
}
