/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.agent;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import madkit.kernel.AgentAddress;
import madkit.kernel.Scheduler;
import madkit.kernel.Watcher;
import max.core.Community;
import max.core.role.RActor;
import max.core.role.Role;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.SchedulerAgent;

/**
 * Base agent for all MAX agents, except the Scheduler (which is a special one).
 *
 * <p>Define base functionalities like scheduling and utility methods to retrieve roles, get agents
 * playing some role or retrieve agent addresses.
 *
 * @author Pierre Dubaillay
 * @author Önder Gürcan
 * @since 1.2
 */
public abstract class MAXAgent extends Watcher {

  /**
   * The boolean flag for the visibility of the user interface of this agent during the GUI mode. By
   * default, it is false.
   */
  private boolean isVisible;

  /** Reference to the scheduler agent assigned to this agent. */
  private SchedulerAgent scheduler;

  /**
   * Retrieve the associated scheduler.
   *
   * @return the scheduler. Can be {@code null} is not set.
   */
  public SchedulerAgent getScheduler() {
    return scheduler;
  }

  /**
   * Set the scheduler associated to this agent.
   *
   * @param scheduler the scheduler
   * @throws IllegalArgumentException <code>scheduler</code> is {@code null}
   */
  public void setScheduler(SchedulerAgent scheduler) {
    if (Objects.isNull(scheduler)) throw new IllegalArgumentException("scheduler is null");

    this.scheduler = scheduler;
  }

  /**
   * Given an activator, send the activator to this agent's scheduler.
   *
   * <p>The scheduler will later schedule the activator and execute the action at the right tick.
   *
   * @param activator activator to schedule
   * @throws IllegalArgumentException <code>activator</code> is {@code null}
   */
  public void schedule(ActionActivator<? extends MAXAgent> activator) {
    if (Objects.isNull(activator)) throw new IllegalArgumentException("activator is null");
    scheduler.schedule(activator);
  }

  /**
   * Can this agent play the given role ?
   *
   * Default implementation always return <code>true</code>.
   *
   * @param role role to test
   * @return <code>true</code> if the agent can play the given role, else <code>false</code>
   * @throws IllegalArgumentException <code>role</code> is {@code null}
   */
  public boolean canPlayRole(Class<? extends Role> role) {
    if (Objects.isNull(role)) throw new IllegalArgumentException("role is null");
    return true;
  }

  /** {@inheritDoc} Take the default role {@link Role} in the technical environment. */
  @Override
  protected void activate() {
    final var code =
        requestRole(Community.COMMUNITY, Community.SIMULATION_ENVIRONMENT, Role.class.getName());
    if (!ReturnCode.SUCCESS.equals(code)) {
      getLogger().severe("Unable to take default role max.core.role.Role, exiting.");
      killAgent(this);
    }
  }

  /**
   * Returns if the user interface of this agent during the GUI mode is visible or not.
   *
   * @return true if the user interface of this agent during the GUI mode is visible, false
   *     otherwise.
   */
  public final boolean isVisible() {
    return isVisible;
  }

  /**
   * Sets if the user interface of this agent during the GUI mode should be visible or not.
   *
   * @param isVisible the boolean flag for the visibility of the user interface of this agent during
   *     the GUI mode
   */
  public final void setVisible(boolean isVisible) {
    this.isVisible = isVisible;
  }

  // MAX version of Madkit queries

  /**
   * Get all roles played by this agent in the provided environment.
   *
   * @param environment lookup roles in this environment
   * @return A set of roles played by the agent. Can be empty but never {@code null}
   * @throws IllegalArgumentException <code>environment</code> is {@code null}
   */
  public Set<Class<? extends Role>> getMyRoles(String environment) {
    if (Objects.isNull(environment)) throw new IllegalArgumentException("environment is null");

    final var rolesAsStrings = getMyRoles(Community.COMMUNITY, environment);
    return classesFromStrings(rolesAsStrings);
  }

  /**
   * Given an environment and a role, retrieve all agents addresses playing role in environment.
   *
   * <p>Caller is excluded.
   *
   * @param environment environment to lookup
   * @param role role the agent must play
   * @return a list of matching agent addresses. Never {@code null} but can be empty.
   * @throws IllegalArgumentException either <code>environment</code> or <code>role</code> is <code>
   *     null</code>
   */
  public List<AgentAddress> getAgentsWithRole(String environment, Class<? extends Role> role) {
    if (Objects.isNull(environment)) throw new IllegalArgumentException("environment is null");
    if (Objects.isNull(role)) throw new IllegalArgumentException("role is null");

    return Optional.ofNullable(getAgentsWithRole(Community.COMMUNITY, environment, role.getName()))
        .orElseGet(ArrayList::new);
  }

  /**
   * Given an environment and a role, get this agent's address at the provided location.
   *
   * @param environment environment
   * @param role role
   * @return Either the agent address or {@code null} if the agent is not playing the role in
   *     the environment.
   */
  public AgentAddress getAgentAddressIn(String environment, Class<? extends Role> role) {
    if (Objects.isNull(environment)) throw new IllegalArgumentException("environment is null");
    if (Objects.isNull(role)) throw new IllegalArgumentException("role is null");

    return getAgentAddressIn(Community.COMMUNITY, environment, role.getName());
  }

  /**
   * Returns the address of the agents that are playing role1 and role2 at the intersection of the
   * environments environment1 and environment2 respectively. The address returned are for
   * environment1.
   *
   * @param environment1 First environment
   * @param role1 Role in the first environment
   * @param environment2 Second environment
   * @param role2 Role in the second environment
   * @return a list (never {@code null}) of addresses in (env1, role1)
   */
  public List<AgentAddress> getAgentAddressesAtIntersection(
      String environment1,
      Class<? extends RActor> role1,
      String environment2,
      Class<? extends RActor> role2) {

    final var addresses = getAgentsWithRole(environment1, role1);
    final var candidates =
        getAgentsWithRole(environment2, role2).stream()
            .map(AgentAddress::getAgent)
            .collect(Collectors.toList());

    return addresses.stream()
        .filter(a -> candidates.contains(a.getAgent()))
        .collect(Collectors.toList());
  }

  /**
   * {@inheritDoc}
   *
   * Delegates to this agent's scheduler.
   *
   * @throws IllegalStateException {@link #getScheduler()} returns {@code null}
   */
  @Override
  public final Scheduler.SimulationTime getSimulationTime() {
    if (Objects.isNull(scheduler)) throw new IllegalStateException("Scheduler not set");
    return scheduler.getSimulationTime();
  }

  /**
   * Shortcut method to retrieve the current tick from the {@link
   * madkit.kernel.Scheduler.SimulationTime}.
   *
   * @throws IllegalArgumentException {@link #getScheduler()} returns {@code null}
   * @return the current tick
   */
  public final BigDecimal getCurrentTick() {
    return getSimulationTime().getCurrentTick();
  }

  /**
   * Given an environment, get all roles existing in that environment.
   *
   * @param environment environment to lookup
   * @return a set of roles, never {@code null}
   */
  public Set<Class<? extends Role>> getExistingRoles(String environment) {
    if (Objects.isNull(environment)) throw new IllegalArgumentException("environment is null");

    final var rolesAsStrings = getExistingRoles(Community.COMMUNITY, environment);
    return classesFromStrings(rolesAsStrings);
  }

  // Tools

  /**
   * Given a collection of roles as {@link String}s, return a set of roles as classes.
   *
   * <p>Issue warnings if a role is not an interface name
   *
   * @param rolesAsStrings collection of roles as strings, or {@code null}>
   * @return set of roles as classes, never {@code null}
   */
  @SuppressWarnings(value = {"uncheked"})
  private Set<Class<? extends Role>> classesFromStrings(Collection<String> rolesAsStrings) {
    if (Objects.isNull(rolesAsStrings)) return Collections.emptySet();

    return rolesAsStrings.stream()
        .map(
            s -> {
              try {
                return (Class<? extends Role>) Class.forName(s);
              } catch (ClassNotFoundException e) {
                if (!"manager".equals(s)) { // Role given by madkit, ignore
                  getLogger()
                      .warning(
                          "Unknown role '"
                              + s
                              + "'. Did you used a MAX primitive to take the role ?");
                }
              } catch (ClassCastException e) {
                getLogger()
                    .warning(
                        "Incorrect role '"
                            + s
                            + "' (not descendant of max.core.role.Role). Did you used a MAX primitive to take the role ?");
              }
              return null;
            })
        .filter(Objects::nonNull)
        .collect(Collectors.toSet());
  }

  @Override
  public final boolean isAlive() {
    return super.isAlive();
  }

  @Override
  public final String getName() {
    return super.getName();
  }

  @Override
  public final void setName(String name) {
    super.setName(name);
  }

  @Override
  public final State getState() {
    return super.getState();
  }
}
