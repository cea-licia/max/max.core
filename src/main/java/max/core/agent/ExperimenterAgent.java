/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.agent;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import madkit.action.KernelAction;
import madkit.kernel.Madkit;
import madkit.kernel.Madkit.BooleanOption;
import madkit.kernel.Madkit.Option;
import madkit.kernel.Probe;
import max.core.Community;
import max.core.MAXParameters;
import max.core.SimulationProbe;
import max.core.action.ACTakeRole;
import max.core.action.Action;
import max.core.agent.gui.MAXAgentFrame;
import max.core.inspection.DataInspector;
import max.core.role.RExperimenter;
import max.core.role.Role;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;
import max.core.scheduling.SchedulerAgent;

/**
 * The ExperimenterAgent agent is responsible for designing and conducting
 * dynamic simulation experiments.
 *
 * It has complete knowledge and control over which nodes join and leave at what
 * times, may rewire the topology, may reset parameters and so on.
 *
 * @author Önder Gürcan
 * @author Pierre Dubaillay
 * @author Aimen Djari
 * @author François Le Fevre
 * @since 1.0
 */
public abstract class ExperimenterAgent extends MAXAgent {

	/** Static constants representing configuration keys. */
	private static final String SIMULATION_STEP_KEY = "MAX_CORE_SIMULATION_STEP";
	private static final String SIMULATION_DURATION_KEY = "MAX_CORE_TIMEOUT_TICK";
	private static final String GUI_MODE_KEY = "MAX_CORE_UI_MODE";
	private static final String DESKTOP_FLAG_KEY = "MAX_CORE_DESKTOP_FLAG";
	private static final String RESULTS_FOLDER_KEY = "MAX_CORE_RESULTS_FOLDER_NAME";

	// Default values for non-existing parameters
	private static final String RESULTS_FOLDER_DEFAULT = "./results";

  /**
   * A special action dedicated to cleaning up the simulation at the end.
   *
   * <p>The action starts a new thread, which will wait until the scheduler dies. Then, each agent
   * is killed finishing by the experimenter itself.
   *
   * <p>In the case of a repeated experiment, the experimenter is not killed, and it sets up a new
   * round.
   *
   * <p>The action runs inside the core environment.
   *
   * @max.role {@link RExperimenter}
   */
  private static class ACCleanupSimulation extends Action<ExperimenterAgent> {

		/** How to access the experimentation repetition count and what is the default value. */
		private static final String EXPERIMENT_REPETITION_KEY = "MAX_CORE_EXPERIMENT_REPETITION";
		private static final Integer EXPERIMENT_REPETITION_DEFAULT = 1;

    /**
     * Default constructor.
     *
     * @param owner experimenter agent owning the action
     */
    private ACCleanupSimulation(ExperimenterAgent owner) {
      super(Community.SIMULATION_ENVIRONMENT, RExperimenter.class, owner);
    }

    @Override
    public void execute() {
      final var experimenter = getOwner();
      new Thread(() -> {
                // Wait until scheduler dies
                final var barrier = experimenter.getScheduler().isSchedulerDead();
                synchronized (barrier) {
                  while (!barrier.get()) {
										try {
											barrier.wait();
										} catch (InterruptedException e) {
											// Current thread is interrupted, something bad happened
											// Kill the entire simulation (including scheduler)
											Thread.currentThread().interrupt();
											experimenter.getLogger().severeLog("Simulation end thread interrupted.", e);
											experimenter.killAgent(experimenter.getScheduler());
											break;  // Break while loop and terminate other agents
										}
									}
                }

                // Scheduler is dead, kill other agents
                for (final var agent : experimenter.agentProbe.getCurrentAgentsList()) {
                  if (!agent.equals(experimenter)) {
                    experimenter.killAgent(agent);
                  }
                }

                // Kill ourselves or go next round
								if (experimenter.currentRound + 1 < MAXParameters.getIntegerParameter(EXPERIMENT_REPETITION_KEY, EXPERIMENT_REPETITION_DEFAULT)) {
									experimenter.currentRound += 1;
									experimenter.setupScheduler();
									experimenter.setupRound();
								} else {
  	              experimenter.killAgent(experimenter);
								}
              }).start();
    }
  }

  /**
   * Create an {@link ExperimenterAgent} instance using the provided class name.
   *
   * @param className class name of the Experimenter
	 * @param <T> The experimenter's type
   * @return an instance
   * @throws IllegalArgumentException the provided class doesn't exist or the no-args public
   *     constructor doesn't exist.
   */
  public static <T extends ExperimenterAgent> T fromClassName(String className) {
    try {
      final var experimenterClass = Class.forName(className);
      return (T) experimenterClass.getDeclaredConstructor().newInstance();
    } catch (Exception e) {
      throw new IllegalArgumentException("Experimenter can't be created.", e);
    }
  }

  /**
   * Launch the provided experimenter.
   *
   * <p>The method will also make sure parameters (aka configuration) are correct by calling {@link
   * #verifyParameterValues()} before launching the experimenter.
   *
   * @param agent the experimenter to launch
   */
  public static void launchExperimenter(ExperimenterAgent agent) {
    // Verify parameters
    agent.verifyParameterValues();

    // Launch the simulation
    final var kernel =
        new Madkit(
            BooleanOption.desktop.toString(),
                String.valueOf(
                    MAXParameters.getBooleanParameter(DESKTOP_FLAG_KEY, false)
                        && agent.isGUIMode()),
            Option.agentFrameClass.toString(), MAXAgentFrame.class.getName());
    kernel.doAction(KernelAction.LAUNCH_AGENT, agent, agent.isVisible() && agent.isGUIMode());
  }

	/** Where data will be outputted. */
	private Path outputFolder;

	/** Simulation duration. */
	private double duration;

	/** A probe to query the environment and retrieve all agents in the simulation. */
	private final Probe<MAXAgent> agentProbe;

	/** A flag telling if the agent is about to die. If set to true, then there is no remaining operation. */
	private final AtomicBoolean simulationEnded;

	/** Number of the current round. */
	private int currentRound;

  /**
   * Creates a new instance.
   *
   * <p>It initializes:
   *
   * <ul>
   *   <li>a probe to keep track of all agents in the simulation
   *   <li>the simulation end flag (to false)
   *   <li>a default scheduler
   * </ul>
   *
   * It also loads two values from the configuration file : the output folder and the experiment
   * duration.
   */
  protected ExperimenterAgent() {
		// Load parameters
		setOutputFolder(Path.of(MAXParameters.getParameter(RESULTS_FOLDER_KEY, RESULTS_FOLDER_DEFAULT)));
		setDuration(MAXParameters.getDoubleParameter(SIMULATION_DURATION_KEY));

		// Initialize attributes
		agentProbe = new SimulationProbe<>(Community.SIMULATION_ENVIRONMENT, Role.class);
		simulationEnded = new AtomicBoolean();
		setupScheduler();
	}

	@Override
	protected void activate() {
		// Create the simulation group (Core environment)
		createGroup(Community.COMMUNITY, Community.SIMULATION_ENVIRONMENT);

		// MAXAgent stuff like taking default role
		super.activate();

		// Setup current round
		setupRound();

	}

	/**
	 * Set up the round.
	 *
	 * A round is one run of the experiment. The experimenter agent can run several times the same
	 * experiment, but it has to set up some things each time a run is about to begin. This is what
	 * this method does.
	 *
	 * First, it retrieves the scheduler, sets the end tick and launch the scheduler. Then it requests
	 * the {@link RExperimenter} role. Then the method creates the output folder, sets up agents and
	 * launch them. Finally, the simulation end action is scheduled and the scheduler resumed.
	 */
	private void setupRound() {
		// Launch the scheduler
		SchedulerAgent scheduler = getScheduler();
		scheduler.getSimulationTime().setEndTick(BigDecimal.valueOf(this.duration));
		launchAgent(scheduler, isGUIMode());

		// Request my role
		new ACTakeRole<>(Community.SIMULATION_ENVIRONMENT, RExperimenter.class, this).execute();
		addProbe(agentProbe);

		// Create output folder
		createOutputFolder();

		// Set up and launch agents
		launchAgents(setupScenario());
		launchAgents(setupDataInspectors());
		setupExperimenter();

		// Schedule simulation end action
		schedule(new ActionActivator<>(
				ActivationScheduleFactory.createOneTime(getSimulationTime().getEndTick()),
				new ACCleanupSimulation(this)
		));

		// Resume the simulation after set up
		getScheduler().resumeSimulation();
	}

  /**
   * Gets the output folder (where produced data should be written).
   *
   * <p>If the experimenter is activated, then the returned path exists and is a writable directory.
   * Otherwise, no assumptions on the path can be made.
   *
   * @return output folder, never {@code null}.
   */
  public Path getOutputFolder() {
		return outputFolder;
	}

  /**
   * Sets the output folder (where produced data should be written).
   *
   * <p>Only relevant if the experimenter is not launched yet. The final output folder will the
   * current date resolved against the experimenter name resolved against the provided path.
   *
   * <p>For example, if the provided path is "./results", the experimenter name is "bob" and current
   * date is 2021_09_01_09_33_00, then the final output folder is
   * "./results/bob/2021_09_01_09_33_00".
	 *
   *
   * @param outputFolder base folder for outputted data.
	 * @throws NullPointerException the provided path is {@code null}
	 * @throws IllegalStateException the method is called after the agent's launch
   */
  public void setOutputFolder(Path outputFolder) {
    if (!State.NOT_LAUNCHED.equals(getState())) {
			throw new IllegalStateException("Agent already launched");
    }
		final DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		this.outputFolder = outputFolder.resolve(getName()).resolve(dateFormat.format(new Date()));
  }

	/**
	 * Gets the duration of the experiment.
	 *
	 * @return the experiment duration, in ticks
	 */
	public double getDuration() {
		return duration;
	}

	/**
	 * Sets the experiment duration.
	 *
	 * Only relevant if the experimenter is not launched yet.
	 *
	 * @param duration how many ticks the simulation will last
	 * @throws IllegalStateException the method is called after the agent's launch
	 * @throws IllegalArgumentException the provided duration is less than zero
	 */
	public void setDuration(double duration) {
		if (!State.NOT_LAUNCHED.equals(getState())) {
			throw new IllegalStateException("Agent already launched");
		}
		if (duration < 0) {
			throw new IllegalArgumentException("duration must be greater or equal to zero");
		}
		this.duration = duration;
	}

	/**
	 * Creates and configures the scheduler agent.
	 */
	private void setupScheduler() {
		final var scheduler = new SchedulerAgent();
		BigDecimal simulationStep = MAXParameters.getBigDecimalParameter(SIMULATION_STEP_KEY);
		scheduler.setSimulationStep(simulationStep);
		scheduler.getLogger().setLevel(Level.WARNING);
		setScheduler(scheduler);
	}

  /**
   * Creates the output folder.
   *
   * <p>If the creation fails, the error is logged and the agent killed (preventing the simulation
   * to start).
   */
  private void createOutputFolder() {
    try {
      Files.createDirectories(outputFolder);
    } catch (IOException e) {
      getLogger().log(Level.SEVERE, e, () -> "Unable to create output folder, end simulation.");
      killAgent(this);
    }
	}

	/**
	 * Tells if the experimenter is in GUI mode (displays a GUI) or not.
	 *
	 * @return <code>true</code> if uiMode is "GUI"
	 */
	public boolean isGUIMode() {
		return "GUI".equals(MAXParameters.getParameter(GUI_MODE_KEY));
	}

	/**
	 * Verifies the parameter values one by one and throws an exception if a
	 * parameter is not correct.
	 *
	 * @throws IllegalArgumentException a parameter has an invalid value
	 */
	public void verifyParameterValues() {
		// Simulation step is grater than zero
		final var simulationStep = MAXParameters.getBigDecimalParameter(SIMULATION_STEP_KEY);
		if (simulationStep.compareTo(BigDecimal.ZERO) <= 0) {
			throw new IllegalArgumentException(SIMULATION_STEP_KEY + " must be greater than zero");
		}
	}

	/**
	 * Synchronization primitive telling if the experimenter ended all agents in the simulation.
	 * Other threads should wait on this object and check if the value is <code>true</code>.
	 *
	 * @return the synchronization primitive.
	 */
	public AtomicBoolean isSimulationEnded() {
		return simulationEnded;
	}

	/**
	 * Last behavior called by Madkit. Notifies all threads waiting on this agent to end. If
	 * overridden make sure it is called as the last operation in your end() method.
	 */
	@Override
	public final void end() {
		synchronized (simulationEnded) {
			simulationEnded.set(true);
			simulationEnded.notifyAll();
		}
	}

	/**
	 * Sets up the experiments to be conducted.
	 *
	 * @return the list of agents to launch
	 */
	protected abstract List<MAXAgent> setupScenario();

	/**
	 * Sets up the viewers that will observe agents and collect data, and returns
	 * them as a list.
	 *
	 * @return the list of data inspectors to launch
	 */
	protected List<DataInspector> setupDataInspectors() {
		return new ArrayList<>();
	}

	/**
	 * Sets up the experimenter level actions for doing something when a specific
	 * condition meets. For example, stopping the simulation when a specific thing
	 * happens, adding/removing agents when a specific thing happens and so on.
	 */
	protected void setupExperimenter() {}

  /**
   * Given a list od agents, launches them.
   *
   * <p>Whether a frame is displayed will depend on {@link #isGUIMode()} and {@link
   * MAXAgent#isVisible()}. This method also sets the scheduler for each agent.
   *
   * @param agents the list of agents to launch
   */
  protected final void launchAgents(List<? extends MAXAgent> agents) {
    agents.forEach(
        agent -> {
          agent.setScheduler(getScheduler());
          launchAgent(agent, isGUIMode() && agent.isVisible());
        });
  }
}
