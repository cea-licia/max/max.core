/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.agent;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import madkit.kernel.AbstractAgent;
import madkit.kernel.Probe;
import max.core.Community;
import max.core.Context;
import max.core.EnvironmentEvent;
import max.core.IEventListener;
import max.core.action.ACTakeRole;
import max.core.action.Plan;
import max.core.role.RActor;
import max.core.role.Role;

/**
 * An agent can make an initial plan where it schedules some actions as well as
 * it can schedule or reschedule actions during runtime. To do so, an agent
 * should be able to access to the scheduler and the global virtual time.
 *
 * @author Pierre Dubaillay
 * @since 1.0
 */
public class SimulatedAgent extends MAXAgent implements IEventListener {

	/**
	 * The time of creation of this agent in terms of simulation time (ticks).
	 */
	private BigDecimal activationTime;

	/**
	 * List of role in which this agent can play.
	 */
	private final Set<Class<? extends Role>> playableRoles;

	/**
	 * A map environment -> associated context.
	 */
	private final Map<String, Context> contexts;

	/** Agent's plan. */
	private final Plan<? extends SimulatedAgent> plan;

	/**
	 * Create a new {@link SimulatedAgent}.
	 *
	 * You need to provide a {@link Plan} that will be executed by the agent. The
	 * agent will make a copy of it and set actions owner to itself. You can safely
	 * share plans with actions owner set to {@code null}. All actions must
	 * support copy operation otherwise an exception is thrown.
	 *
	 * @param plan a non-null plan
	 * @throws IllegalArgumentException {@code plan} is {@code null}
	 */
	public SimulatedAgent(final Plan<? extends SimulatedAgent> plan) {
		if (plan == null) {
			throw new IllegalArgumentException("Argument 'plan' shouldn't be null");
		}
		// Initialize attributes.
		playableRoles = new HashSet<>();
		contexts = new HashMap<>();

		this.plan = plan.copyFor(this);

		// Add default playable roles
		addPlayableRole(Role.class);
		addPlayableRole(RActor.class);
	}

	/**
	 * Initializes this agent.
	 * First it will request the default role ({@link RActor}) in the default environment.
	 * Then the agent will schedule all actions describe in the plan.
	 * Finally, the activation time is stored for later use.
	 */
	@Override
	protected void activate() {
		super.activate();
		new ACTakeRole<>(Community.SIMULATION_ENVIRONMENT, RActor.class, this).execute();

		// Schedule all actions from the initial plan
		try (var stream = plan.getInitialPlan().stream()) {
			stream.forEach(this::schedule);
		}

		// Store activation time as current time
		this.activationTime = getSimulationTime().getCurrentTick();
		getLogger().log(Level.INFO, "Activated at t = " + activationTime);
	}

	/**
	 * Add a new {@link Context} to the agent.
	 * The new context will be associated to an environment (the one in {@link Context#getEnvironmentName()}). Replace
	 * old context association if it exists.
	 */
	final void addContext(Context context) {
		this.contexts.put(context.getEnvironmentName(), context);
	}

	/**
	 * Get all contexts belonging to the agent.
	 *
	 * @return an unmodifiable view of the agent's contexts
	 */
	public final Map<String, Context> getContexts() {
		return Collections.unmodifiableMap(contexts);
	}

	/**
	 * Get a specific context (the one associated to the environment).
	 *
	 * @param environmentName environment
	 * @return the context if it exists, {@code null} otherwise
	 */
	public final Context getContext(String environmentName) {
		return contexts.get(environmentName);
	}

	/**
	 * Remove a context (the one associated to the provided environment).
	 * No-op if context doesn't exist.
	 *
	 * @param environmentName environment
	 */
	public final void removeContext(String environmentName) {
		this.contexts.remove(environmentName);
	}

	/**
	 * Check if the agent has a context associated to the provided environment.
	 *
	 * @param environmentName environment
	 * @return <code>true</code> if agent has a context, else <code>false</code>
	 */
	public final boolean hasContext(String environmentName) {
		return contexts.containsKey(environmentName);
	}

	/**
	 * Get the activation time of the agent.
	 *
	 * @return activation time. {@code null} if not activated yet.
	 */
	public final BigDecimal getActivationTime() {
		return activationTime;
	}

	/**
	 * Adds a given role together with all its intermediary roles in the role hierarchy to the list of
	 * roles this agent can play.
	 *
	 * @param newRole role to add
	 */
	protected final void addPlayableRole(Class<? extends Role> newRole) {
		final var added = this.playableRoles.add(newRole);
		if (added) {
			for (final var intermediaryRole : newRole.getInterfaces()) {
				addPlayableRole((Class<? extends Role>) intermediaryRole);
			}
		}
	}

	@Override
	public void onEvent(EnvironmentEvent event) {
		final var actions = plan.getActionsForEvent(event.getClass());
		for (final var action : actions) {
			action.setEventToReact(event);
			action.execute();
		}
	}

	/**
	 * {@inheritDoc}
	 * Checks if the given role is one of the roles you set playable with {@link #addPlayableRole(Class)}.
	 */
	@Override
	public boolean canPlayRole(Class<? extends Role> role) {
		return playableRoles.contains(role);
	}

  // Deactivate probing functionalities

  /**
   * {@inheritDoc}
   *
   * @throws UnsupportedOperationException this operation is not allowed on {@link SimulatedAgent}
   */
  @Override
  public final void addProbe(Probe<? extends AbstractAgent> probe) {
    throw new UnsupportedOperationException("Operation 'addProbe' not permitted on SimulatedAgent");
  }

  /**
   * {@inheritDoc}
   *
   * @return An always empty array
   */
  @Override
  public final Probe<AbstractAgent>[] allProbes() {
    return new Probe[0];
  }

  /**
   * {@inheritDoc}
   *
   * @return An always empty set. Never {@code null}.
   */
  @Override
  public final Set<Probe<? extends AbstractAgent>> getProbes() {
    return Set.of();
  }
}
