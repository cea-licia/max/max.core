/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.agent;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import madkit.kernel.AgentAddress;
import madkit.kernel.Probe;
import max.core.Community;
import max.core.Context;
import max.core.EnvironmentEvent;
import max.core.IEventGenerator;
import max.core.IEventListener;
import max.core.SimulationProbe;
import max.core.action.ACTakeRole;
import max.core.role.RActor;
import max.core.role.RGroupManager;
import max.core.role.Role;

/**
 *
 * SimulatedEnvironment is a first-class abstraction in the modeling and
 * engineering of complex computational systems, such as pervasive, adaptive,
 * and multi-agent systems [Weyns]. It is usually modeled through the resource
 * abstraction, as a non-goal-driven entity producing events and/or reactively
 * waiting for requests to perform its function. SimulatedEnvironment is
 * essential in coordination since it works as a mediator for agent interaction
 * through which agents can communicate and coordinate indirectly. It is active;
 * featuring autonomous dynamics, and affecting agent coordination. It has a
 * structure; requiring a notion of locality, and allowing agents of any sort to
 * move through a topology.
 * <p>
 * To this end, SimulatedEnvironment is modeled as a MaDKit Watcher agent.
 * SimulatedEnvironment is mainly in charge of (1) allocating physical
 * addresses (e.g., coordinate address) and/or logical addresses (e.g., wallet
 * address or even alias) to agents and artifacts and, (2) mediating agent-agent
 * and agent-artifact interaction through these addresses.
 * <p>
 * In this generic implementation, there is no limitation about the number of
 * agents and/or artifacts that can be allocated to an address (e.g., in a Grid
 * environment several agents may be at the same position). Similarly, there is
 * also no limitation about the number of addresses an agent can allocate (e.g.,
 * in a Bitcoin environment an agent may have several wallet addresses). Note
 * decided totally yet, but probably artifacts will not have more than one
 * address.
 * <p>
 * SimulatedEnvironment uses a {@link Probe} to set the agents' address field
 * for this environment so that they can use the environment's methods once they
 * enter the artificial society.
 *
 * @author Önder Gürcan
 * @author Pierre Dubaillay
 * @author François Le Fevre
 * @author Aimen Djari
 * 
 * @since 1.0
 */
public class SimulatedEnvironment extends MAXAgent implements IEventGenerator<AgentAddress> {

	/**
	 * The roles allowed to be played in this environment.
	 */
	private final List<Class<? extends Role>> allowedRoles = new ArrayList<>();

	/**
	 * The list of agents in this environment.
	 */
	private final Map<AgentAddress, IEventListener> agents = new Hashtable<>();

	/**
	 * Default constructor for {@link SimulatedEnvironment}. Allows {@link RActor} role. By default,
	 * and if the experiment is started in GUI mode, the environment displays its GUI.
	 */
	public SimulatedEnvironment() {
		addAllowedRole(RActor.class);
		setVisible(true);
	}

	/**
	 * Add the given role to roles that can be taken in the environment.
	 *
	 * It doesn't technically prevent playing a not-allowed role, but the agent
	 * address associated won't be tracked by the environment. No effect if called
	 * after agent activation.
	 *
	 * @param role Allowed role
	 * @throws IllegalArgumentException <code>role</code> is {@code null}
	 */
	protected final void addAllowedRole(Class<? extends Role> role) {
		if (Objects.isNull(role))
			throw new IllegalArgumentException("role is null");
		this.allowedRoles.add(role);
	}

	@Override
	protected void activate() {
		super.activate();

		// Create a dedicated group and take role in it
		createGroup(Community.COMMUNITY, getName());
		new ACTakeRole<>(getName(), RGroupManager.class, this).execute();

		for (Class<? extends Role> agentRole : this.allowedRoles) {
			// agents have to join into the dedicated environment when they are requesting a
			// role in a <community, environment>
			addProbe(new SimulationProbe<SimulatedAgent>(getName(), agentRole) {
				@Override
				protected void adding(SimulatedAgent agent) {
					super.adding(agent);
					// Check if agent has context for environment
					if (!agent.hasContext(SimulatedEnvironment.this.getName())) {
						// if not, create a dedicated context
						agent.addContext(createContext(agent));
					}
					AgentAddress agentAddressIn = agent.getAgentAddressIn(SimulatedEnvironment.this.getName(),
							agentRole);
					SimulatedEnvironment.this.register(agentAddressIn, agent);
				}

				@Override
				protected void removing(SimulatedAgent agent) {
					AgentAddress agentAddressIn = agent.getAgentAddressIn(SimulatedEnvironment.this.getName(),
							agentRole);
					SimulatedEnvironment.this.unregister(agentAddressIn);

					// Remove agent context if this is the last role it plays in this environment
					// Agent has still its role here, so if getMyRoles returns 1, then we are
					// leaving the last role
					final var hasNoMoreRole = agent.getMyRoles(SimulatedEnvironment.this.getName()).size() <= 1;
					if (hasNoMoreRole) {
						agent.removeContext(SimulatedEnvironment.this.getName());
					}
				}
			});
		}

		getLogger().log(Level.FINER, " [ N/A ] : " + "activate() is finished!");
	}

	/**
	 * Registers an agent to roleSpecificAddress. Neither roleSpecificAddress nor agent can be
	 * null. If either is null, a {@link NullPointerException} is thrown. It should be noted that the 
	 * very same agent can register to this environment with various roles each time with a different 
	 * dedicated roleSpecificAddress.
	 * 
	 * @param roleSpecificAddress the unique role specific address for agent for this environment
	 * @param agent the agent that request registering itself to this environment
	 * 
	 * @throws IllegalArgumentException - if roleSpecificAddress or agent is null
	 */
	@Override
	public void register(AgentAddress roleSpecificAddress, IEventListener agent) {
		if (roleSpecificAddress == null) {
			throw new IllegalArgumentException("roleSpecificAddress cannot be null.");
		} 
		
		if (agent == null) {
			throw new IllegalArgumentException("agent cannot be null.");
		}
		
		Optional.of(agent).ifPresent(o -> {
			getAgents().put(roleSpecificAddress, o);
			getLogger().log(Level.FINE, roleSpecificAddress + " is registered to " + this);
		});
	}

	/**
	 * Unregisters an agent from roleSpecificAddress. roleSpecificAddress cannot be
	 * null. If it is null, a {@link NullPointerException} is thrown.
	 * 
	 * @param roleSpecificAddress the unique role specific address for agent for this environment
	 * 
	 * @throws IllegalArgumentException - if roleSpecificAddress is null
	 */
	@Override
	public void unregister(AgentAddress roleSpecificAddress) {
		if (roleSpecificAddress == null) {
			throw new IllegalArgumentException("roleSpecificAddress cannot be null.");
		}
		
		getAgents().remove(roleSpecificAddress);
		getLogger().log(Level.FINE, roleSpecificAddress + " is unregistered from " + this);
	}

	/**
	 * Notifies the corresponding message observer if it exists and registered to
	 * the list of observers.
	 * 
	 * @param roleSpecificAddress the unique role specific address for agent for this environment
	 * @param event the event that is targeting a specific role of a specific agent registered to this environment
	 * 
	 * @throws IllegalArgumentException - if roleSpecificAddress is null
	 */
	@Override
	public void notify(AgentAddress roleSpecificAddress, EnvironmentEvent event) {
		if (roleSpecificAddress == null) {
			throw new IllegalArgumentException("roleSpecificAddress cannot be null.");
		}
		
		IEventListener messageObserver = getAgents().get(roleSpecificAddress);
		if (messageObserver != null) {
			messageObserver.onEvent(event);
		} else {
			getLogger().log(Level.INFO,
					"[" + getSimulationTime().getCurrentTick() + "] : "
							+ " Cannot deliver the message! The receiver address " + roleSpecificAddress
							+ " is not found in this environment.");
		}
	}

	/**
	 * Returns the mapping {@link AgentAddress} to {@link IEventListener} stored by
	 * this environment.
	 *
	 * @return mapping. Never {@code null}.
	 */
	protected final Map<AgentAddress, IEventListener> getAgents() {
		return this.agents;
	}

	/**
	 * Tells if this environment is aware of the provided agent.
	 *
	 * @param agentAddress agent address
	 * @return <code>true</code> if the environment knows the agent
	 */
	public final boolean hasAgent(AgentAddress agentAddress) {
		return getAgents().containsKey(agentAddress);
	}

	/**
	 * This method is used for creating dedicated contexts. It should be overridden
	 * each time a new environment is defined.
	 * 
	 * @param agent the owner agent of the new context
	 * @return a new context for agent
	 */
	protected Context createContext(SimulatedAgent agent) {
		return new Context(agent, this);
	}

	/**
	 * Tells if the given role is allowed in the environment.
	 *
	 * @param role role to check
	 * @return <code>true</code> if the role is allowed
	 */
	public final boolean allowsRole(Class<? extends Role> role) {
		return this.allowedRoles.contains(role);
	}

	/**
	 * Get all allowed roles.
	 *
	 * @return a list of allowed roles. Never {@code null}
	 */
	public List<Class<? extends Role>> getAllowedRoles() {
		return allowedRoles;
	}

}