/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.agent.gui;

import javax.swing.JMenuBar;
import madkit.gui.AgentFrame;
import madkit.kernel.AbstractAgent;

/**
 * Custom default agent frame for all agents in the simulation.
 *
 * Replaces the "Madkit" menu by "MAX" for now.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public class MAXAgentFrame extends AgentFrame {

  /**
   * Creates a new {@link MAXAgentFrame}.
   *
   * @param agent the considered agent
   */
  public MAXAgentFrame(AbstractAgent agent) {
    super(agent);
  }

  @Override
  public JMenuBar createMenuBar() {
    final var menuBar = super.createMenuBar();
    menuBar.getMenu(0).setText("MAX");  // Replace Madkit by MAX
    return menuBar;
  }
}
