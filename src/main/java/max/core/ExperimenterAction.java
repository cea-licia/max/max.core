/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core;

import max.core.action.Action;
import max.core.agent.ExperimenterAgent;
import max.core.role.RExperimenter;

/**
 * This is an {@link Action} that can be scheduled and executed by an
 * ExperimenterAgent role.
 * 
 * @author Önder Gürcan
 * @since 1.0
 *
 * @param <A> Experimenter type
 */
abstract public class ExperimenterAction<A extends ExperimenterAgent> extends Action<A> {

	/**
	 * Default constructor.
	 *
	 * @param owner the action's owner.
	 */
	public ExperimenterAction(final A owner) {
		super(Community.SIMULATION_ENVIRONMENT, RExperimenter.class, owner);
	}

}
