/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core;

import madkit.kernel.Probe;
import max.core.agent.MAXAgent;
import max.core.role.Role;

/**
 * MAX version of Madkit {@link Probe} type.
 * Hides some implementation details and follow MAX conventions regarding environments and roles.
 *
 * @param <A> Agent type
 * @author Pierre Dubaillay
 */
public class SimulationProbe<A extends MAXAgent> extends Probe<A> {

    /**
     * Default constructor of {@link SimulationProbe}.
     *
     * @param environmentName Name of the environment this probe will probe
     * @param role Role class this probe will probe
     */
    public SimulationProbe(final String environmentName, final Class<? extends Role> role) {
        super(Community.COMMUNITY, environmentName, role.getName());
    }
}
