/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core;

import madkit.kernel.AgentAddress;
import max.core.agent.SimulatedAgent;
import max.core.agent.SimulatedEnvironment;

/**
 * For each simulatedEnvironment an agent enters, all the information (simulatedEnvironment
 * address, variables, etc.) related to that simulatedEnvironment should be encapsulated
 * in a Context abstraction.
 * 
 * @author Önder Gürcan
 * @author Mohamed Aimen Djari
 * @author Pierre Dubaillay
 * 
 * @since 1.0
 *
 */
public class Context {

	/** The simulatedEnvironment of this context. */
	private final SimulatedEnvironment simulatedEnvironment;

	/** The context's owner. */
	private final SimulatedAgent owner;

	/**
	 * Default constructor.
	 *
	 * @param owner context's owner
	 * @param simulatedEnvironment associated environment
	 */
	public Context(SimulatedAgent owner, SimulatedEnvironment simulatedEnvironment) {
		this.owner = owner;
		this.simulatedEnvironment = simulatedEnvironment;
	}

  /**
   * Gets the associated environment's name.
   *
   * @return name of the environment
   */
  public final String getEnvironmentName() {
		return simulatedEnvironment.getName();
	}

	/**
	 * Gets the associated environment.
	 *
	 * @return a reference to the environment
	 */
  public final SimulatedEnvironment getEnvironment() {
		return simulatedEnvironment;
	}

	/**
	 * Gets the owner's address for the given role.
	 *
	 * @param role the role
	 * @return either the {@link AgentAddress} or {@code null} if the agent is not playing the role
	 */
  public final AgentAddress getMyAddress(String role) {
		if (role == null) {
			throw new IllegalArgumentException("The 'role' parameter cannot be null");
		}
		
		AgentAddress address = owner.getAgentAddressIn(Community.COMMUNITY, simulatedEnvironment.getName(), role);
		return address;
	}

	/**
	 * Get the context's owner.
	 *
	 * @return reference to the owner.
	 */
  public final SimulatedAgent getOwner() {
		return owner;
	}

}
