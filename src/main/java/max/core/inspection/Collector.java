/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection;

import java.util.Map;

/**
 * An object designed to collect data inside the simulation.
 *
 * <p>{@link Collector} objects can be associated with several {@link Exporter}s. Each time valuable
 * data is collected, the collector should call {@link Exporter#export(Object)} on each associated
 * exporter.
 *
 * <p>It is up to the implementation to decide what is "valuable data" and when to export it.
 *
 * @param <D> structure of the produced data
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public interface Collector<D> {

  /**
   * Associate a new exporter to this collector.
   *
   * @param exporter exporter to associate
   */
  void addExporter(Exporter<D> exporter);

  /** Called once when the collector is activated. It should propagate the call to exporters. */
  void activate();

  /**
   * Called once the collector is about to be removed. It should propagate the call to exporters.
   */
  void end();

  /**
   * Gets a mapping {@link java.lang.String} to {@link java.lang.Object} describing properties of
   * the data this collector collects. Can be anything but {@code null}.
   *
   * By default, returns an empty map
   *
   * @return this collector's metadata
   */
  default Map<String, Object> getMetadata() {
    return Map.of();
  }
}
