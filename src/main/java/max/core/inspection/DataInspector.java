/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection;

import java.util.ArrayList;
import java.util.List;
import max.core.Community;
import max.core.action.ACTakeRole;
import max.core.agent.MAXAgent;
import max.core.role.RDataInspector;

/**
 * An agent designed to support simulation {@link Collector}s.
 *
 * You can add as many collectors as you want.
 *
 * {@link Collector#activate()} is called at the right time: if the
 * collector is added before the {@link DataInspector} agent activation, then it is activated at the same time the
 * agents is. Otherwise, the collector is activated immediately.
 *
 * Collector's {@link Collector#end()} method is called when the agent dies, in its {@link #end()} method.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public class DataInspector extends MAXAgent {

    /** List of collectors. */
    private final List<Collector<?>> collectors;

    /**
     * Creates a new instance of {@link DataInspector} without any collectors yet.
     */
    public DataInspector() {
        this.collectors = new ArrayList<>();
    }

    /**
     * Adds a new collector to this inspector.
     * If the inspector is already active, then the collector is immediately activated.
     *
     * @param collector collector to add
     */
    public void addCollector(Collector<?> collector) {
        collectors.add(collector);

        // If dataInspector is already activated, start immediately the collector
        if (isAlive()) {
            collector.activate();
        }
    }

    @Override
    protected void activate() {
        super.activate();

        // Take the RDataInspector role in Simulation Environment.
        new ACTakeRole<>(Community.SIMULATION_ENVIRONMENT, RDataInspector.class, this).execute();

        // activate collectors
        for (final var collector : collectors) {
            collector.activate();
        }
    }

    @Override
    protected void end() {
        for (final var collector : collectors) {
            collector.end();
        }
        super.end();
    }
}
