/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.exporter;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import max.core.inspection.Exporter;

/**
 * An exporter designed to export data in a file.
 *
 * This base manages everything related to file creation, opening and closing and also provides a utility method named
 * {@link #writeData(String)}, which writes provided data in the file.
 *
 * @param <D> type of exported data
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public abstract class FileExporter<D> implements Exporter<D> {
    /** File path where data is stored. */
    private final Path filePath;

    /** Writer object to write in the file. */
    private Writer writer;

    /** Is the writer open ? */
    private boolean writerIsOpen;

    /** Metadata to write. */
    private final Map<String, Object> metadata;

    /**
     * Given an owner and a file path, creates a new instance.
     *
     * @param filePath where data is stored
     * @param metadata some metadata to write in the file header
     */
    protected FileExporter(Path filePath, Map<String, Object> metadata) {
        this.filePath = filePath;
        this.metadata = metadata;
    }

    @Override
    public void activate() {
        if (!writerIsOpen) {
            // Open file
            try {
                Files.createDirectories(filePath.getParent());
                Files.createFile(filePath);
                writer = Files.newBufferedWriter(filePath);
                writerIsOpen = true;
            } catch (IOException e) {
                getLogger().warning(String.format(
                        "[%s] Unable to create file %s",
                        this.getClass().getName(), filePath)
                );
                return;
            }

            // Write metadata
            try {
                writeMetadata(metadata);
            } catch (final IOException e) {
                getLogger().log(Level.WARNING, "Unable to write metadata in file.", e);
            }
        }
    }

    @Override
    public void end() {
        if (writerIsOpen) {
            writerIsOpen = false;
            try {
                writer.close();
            } catch (IOException e) {
                getLogger().warning(String.format(
                        "[%s] Unable to close file %s", this.getClass().getName(), filePath));
            }
        }
    }

    /**
     * Writes the provided string-formatted data into the file.
     * If something bad happens, the exporter will no longer write data.
     *
     * @param data data to write
     */
    protected void writeData(String data) {
        if (writerIsOpen) {
            try {
                writer.write(data);
            } catch (IOException e) {
                getLogger().warning(String.format(
                        "[%s] Unable to write data in file %s",
                        this.getClass().getName(), filePath)
                );
                end();
            }
        }
    }

    /**
     * Writes the given metadata in the file.
     *
     * @param metadata metadata to write
     * @throws IOException the metadata can't be written for some reason
     */
    protected abstract void writeMetadata(Map<String, Object> metadata) throws IOException;

    /**
     * Gets the class logger.
     *
     * @return a logger, never {@code null}
     */
    protected abstract Logger getLogger();
}
