/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.exporter;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Exporter exporting data into csv files. It expects data structured as a list of things.
 *
 * <p>Metadata are exported the first time {@link #export(List)} is called. The format is the
 * following: for each entry, the file will contain a line '# entry_name: value'. Entry's value is
 * computed using the {@link Object#toString()} method.
 *
 * <p>If the metadata contains a key named as {@link #HEADER_ENTRY}, then the value is expected to
 * be the list of column's names and will be used to write them as the first line (after metadata).
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public class CSVFileExporter extends FileExporter<List<?>> {

    /** Which key is used to fill each column's name (aka the header). */
    public static final String HEADER_ENTRY = "header";

    /** Default delimiters. */
    private static final String LINE_DELIMITER = System.lineSeparator();
    private static final String COLUMN_DELIMITER = ",";

    /** Class logger. */
    private static final Logger LOGGER = Logger.getLogger(CSVFileExporter.class.getCanonicalName());

    /** Column delimiter. */
    private final String delimiter;

    /** Line delimiter. */
    private final String newLine;

    /**
     * Create a new instance of {@link CSVFileExporter}.
     *
     * @param filePath file path where data is stored
     * @param metadata metadata to write in the file header
     * @param delimiter column delimiter
     * @param newLine new line character
     */
    public CSVFileExporter(Path filePath, Map<String, Object> metadata, String delimiter, String newLine) {
        super(filePath, metadata);
        this.delimiter = delimiter;
        this.newLine = newLine;
    }

    /**
     * Create a new instance of {@link CSVFileExporter}. Newline character is set to system one.
     *
     * @param filePath file path where data is stored
     * @param metadata metadata to write in the file header
     * @param delimiter column delimiter
     * @see #CSVFileExporter(Path, Map, String, String)
     */
    public CSVFileExporter(Path filePath, Map<String, Object> metadata, String delimiter) {
        this(filePath, metadata, delimiter, LINE_DELIMITER);
    }

    /**
     * Create a new instance of {@link CSVFileExporter}. Newline character is set to system one.
     *
     * @param filePath file path where data is stored
     * @param metadata metadata to write in the file header
     * @see #CSVFileExporter(Path, Map, String, String)
     */
    public CSVFileExporter(Path filePath, Map<String, Object> metadata) {
        this(filePath, metadata, COLUMN_DELIMITER, LINE_DELIMITER);
    }

    @Override
    protected void writeMetadata(Map<String, Object> metadata) throws IOException {
        writeData(metadata.entrySet().stream()
            .filter(e -> !HEADER_ENTRY.equals(e.getKey()))
            .map(e -> "# " + e.getKey() + ": " + e.getValue())
            .collect(Collectors.joining(newLine))
            + newLine
        );
        if (metadata.containsKey(HEADER_ENTRY)) {
            writeData(String.join(delimiter, (List<String>) metadata.get(HEADER_ENTRY)) + newLine);
        }
    }

    @Override
    public void export(List<?> data) {
        writeData(data.stream()
                .map(Object::toString)
                .collect(Collectors.joining(delimiter))
                + newLine
        );
    }

    @Override
    protected Logger getLogger() {
        return LOGGER;
    }
}
