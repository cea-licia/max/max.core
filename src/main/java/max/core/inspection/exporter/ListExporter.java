/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.exporter;

import java.util.List;
import max.core.inspection.Exporter;

/**
 * A custom {@link Exporter} storing data in a {@link List}.
 *
 * <p>It is in fact an adapter around a plain old list. Each time {@link #export(List)} method is
 * called a new entry is added in the list.
 *
 * Although you can use this exporter anywhere, it has been primarily designed to be used in tests.
 *
 * @author Pierre Dubaillay
 */
public class ListExporter implements Exporter<List<?>> {

  /** Storage. */
  private final List<List<?>> storage;

  /**
   * Create a new {@link ListExporter} instance.
   *
   * @param storage storage object where data will be put.
   */
  public ListExporter(List<List<?>> storage) {
    this.storage = storage;
  }

  @Override
  public void export(List<?> data) {
    storage.add(data);
  }

  @Override
  public void activate() {
    // Empty, nothing to initialize
  }

  @Override
  public void end() {
    // Empty, nothing to clean
  }
}
