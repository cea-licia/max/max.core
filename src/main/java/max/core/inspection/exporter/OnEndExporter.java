/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.exporter;

import java.util.LinkedList;
import java.util.List;
import max.core.inspection.Exporter;

/**
 * Special exporter delaying data export when the method {@link #end()} is called.
 *
 * This exporter needs a child exporter and will manage its lifecycle.
 *
 * @param <D> Type of exported data
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public class OnEndExporter<D> implements Exporter<D> {

    /** Wrapped exporter (the one that does the work). */
    private final Exporter<D> wrapped;

    /** Structure storing data. Each item is one call to export method. */
    private final List<D> storedData;

    /**
     * Build a new instance by wrapping the provided exporter.
     *
     * @param wrapped wrapped exporter
     */
    public OnEndExporter(Exporter<D> wrapped) {
        this.wrapped = wrapped;
        this.storedData = new LinkedList<>();
    }

    @Override
    public void export(D data) {
        this.storedData.add(data);
    }

    @Override
    public void activate() {
        // Do nothing
    }

    @Override
    public void end() {
        wrapped.activate();
        storedData.forEach(wrapped::export);
        wrapped.end();
    }
}
