/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.collector;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import max.core.Community;
import max.core.action.Action;
import max.core.agent.MAXAgent;
import max.core.inspection.Collector;
import max.core.inspection.DataInspector;
import max.core.inspection.Exporter;
import max.core.role.RDataInspector;
import max.core.scheduling.ActionActivator;

/**
 * An implementation base for collectors.
 *
 * This base takes care of:
 * <ul>
 *     <li>Exporters storage</li>
 *     <li>Calling {@link Exporter#activate()} and {@link Exporter#end()} when it is required</li>
 * </ul>
 *
 * It also provides a utility method called {@link #export(Object)} which takes care of calling
 * {@link Exporter#export(Object)} on each registered exporter and a special action named {@link CollectAction} to
 * ease the creation of collecting actions.
 *
 * Finally, it provides some shorthand methods like {@link #getCurrentTime()} and {@link #schedule(ActionActivator)}.
 *
 * @param <D> Structure of the data exported to exporters
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public abstract class AbstractCollector<D> implements Collector<D> {

    /**
     * A convenient action for collecting data.
     *
     * @max.role {@link RDataInspector}
     */
    protected abstract static class CollectAction extends Action<MAXAgent> {

        /** The collector name, to use in {@link #toString()}. */
        private final String collectorName;

        /**
         * Create a new {@link CollectAction} instance.
         *
         * @param owner       Action's owner
         * @param collectorName name of the collector
         */
        protected CollectAction(MAXAgent owner, String collectorName) {
            super(Community.SIMULATION_ENVIRONMENT, RDataInspector.class, owner);
            this.collectorName = collectorName;
        }

        @Override
        public String toString() {
            return collectorName
                + "{env=" + getEnvironment()
                + ", role=" + getRole()
                + ", owner=" + super.getOwner()
                + "}";
        }
    }

    /** Associated metadata. */
    private final Map<String, Object> metadata;

    /** Collection of registered exporters. */
    private final List<Exporter<D>> exporters;

    /** Collector's owner, aka the inspector. */
    private final DataInspector owner;

    /** Is this collector activated ? */
    private boolean activated;

    /**
     * Create a new instance without any exporters. Register itself to the owner.
     *
     * @param owner inspector owning the collector
     * @param metadata Metadata associated to collected data.
     */
    protected AbstractCollector(DataInspector owner, Map<String, Object> metadata) {
        this.metadata = new HashMap<>(metadata);
        this.exporters = new ArrayList<>();
        this.owner = owner;
        owner.addCollector(this);
    }

    /**
     * Create a new instance without any exporters. Register itself to the owner.
     *
     * @param owner inspector owning the collector
     */
    protected AbstractCollector(DataInspector owner) {
        this(owner, Map.of());
    }

    @Override
    public void addExporter(Exporter<D> exporter) {
        exporters.add(exporter);
        if (activated) exporter.activate();
    }

    @Override
    public void activate() {
        activated = true;
        exporters.forEach(Exporter::activate);
    }

    @Override
    public void end() {
        for (final var exporter : exporters) {
            exporter.end();
        }
    }

    /**
     * Export the provided data. It will call {@link Exporter#export(Object)} on each registered exporter.
     *
     * @param data data to export.
     */
    protected void export(D data) {
        exporters.forEach(e -> e.export(data));
    }

    /**
     * Shorthand method to retrieve the current tick.
     *
     * @return current simulation time
     */
    protected BigDecimal getCurrentTime() {
        return owner.getSimulationTime().getCurrentTick();
    }

    /**
     * Shorthand method to schedule an activator using the inspector.
     *
     * @param activator activator to schedule
     */
    protected void schedule(ActionActivator<MAXAgent> activator) {
        owner.schedule(activator);
    }

    /**
     * Get this collector's owner.
     *
     * @return the inspector agent
     */
    protected DataInspector getOwner() {
        return owner;
    }

    @Override
    public Map<String, Object> getMetadata() {
        return metadata;
    }
}
