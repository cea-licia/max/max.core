/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.collector;

import java.math.BigDecimal;
import java.util.Map;
import max.core.inspection.DataInspector;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationScheduleFactory;

/**
 * A specialized collector that collects data each tick.
 *
 * <p>What 'collect' means is left to the developer, who must override the {@link #collectData()}
 * method.
 *
 * <p>The first collect call is scheduled immediately at activation, meaning that if the developer
 * need to initialize some parameters before collecting, s/he has to make it in the constructor or
 * before calling the {@link #activate()} method (for example: redefine activate by initializing the
 * attributes and then calling super.activate()).
 *
 * @param <D> collected data type
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public abstract class EachTickCollector<D> extends AbstractCollector<D> {

  /**
   * Create a new instance without any exporters. Register itself to the owner.
   *
   * @param owner inspector owning the collector
   * @param metadata Metadata associated to collected data.
   */
  protected EachTickCollector(DataInspector owner, Map<String, Object> metadata) {
    super(owner, metadata);
  }

  /**
   * Create a new instance without any exporters. Register itself to the owner.
   *
   * @param owner inspector owning the collector
   */
  protected EachTickCollector(DataInspector owner) {
    super(owner);
  }

  @Override
  public void activate() {
    super.activate();
    schedule(
        new ActionActivator<>(
            ActivationScheduleFactory.createRepeatingInfinitely(getCurrentTime(), BigDecimal.ONE),
            new CollectAction(getOwner(), this.getClass().getSimpleName()) {

              @Override
              public void execute() {
                collectData();
              }
            }));
  }

  /** Collects the data. Called each tick. */
  protected abstract void collectData();
}
