/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.collector;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import madkit.simulation.probe.SingleAgentProbe;
import max.core.Community;
import max.core.action.Action;
import max.core.inspection.DataInspector;
import max.core.role.RScheduler;
import max.core.scheduling.SchedulerAgent;

/**
 * Collect all actions executed each tick.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public class ActionsCollector extends EachTickCollector<List<?>> {

    /** Probe used to inspect the scheduler. */
    private final SingleAgentProbe<SchedulerAgent, List<Action<?>>> schedulerProbe;

    /**
     * Create a new instance without any exporters
     *
     * @param owner    inspector owning the collector
     */
    protected ActionsCollector(DataInspector owner) {
        super(owner, Map.of("header", Arrays.asList("tick", "action name")));
        schedulerProbe = new SingleAgentProbe<>(
                Community.COMMUNITY,
                Community.SIMULATION_ENVIRONMENT,
                RScheduler.class.getName(),
                "lastExecutedActions"
        );
    }

    @Override
    public void activate() {
        getOwner().addProbe(schedulerProbe);
        super.activate();
    }

  @Override
  protected void collectData() {
    final var actions = schedulerProbe.getPropertyValue();
    actions.forEach(a -> export(List.of(getCurrentTime(), a)));
  }
}
