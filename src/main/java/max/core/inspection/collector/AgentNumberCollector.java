/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection.collector;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import max.core.SimulationProbe;
import max.core.agent.MAXAgent;
import max.core.inspection.DataInspector;
import max.core.role.RActor;

/**
 * Collector collecting the number of user agents at each tick.
 *
 * Output data is [tick, # of agents].
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public class AgentNumberCollector extends EachTickCollector<List<?>> {

    /** Probe used to query the number of agents. */
    private final SimulationProbe<MAXAgent> probe;

    /**
     * Create a new instance of {@link AgentNumberCollector} probing the given environment.
     *
     * @param owner inspector owning the collector
     * @param env environment to inspect
     * @param metadata some metadata
     */
    public AgentNumberCollector(DataInspector owner, String env, Map<String, Object> metadata) {
        super(owner, metadata);
        probe = new SimulationProbe<>( env, RActor.class);
    }

    /**
     * Like {@link #AgentNumberCollector(DataInspector, String, Map)} but with default metadata.
     *
     * @param owner inspector owning the collector
     * @param env environment to inspect
     */
    public AgentNumberCollector(DataInspector owner, String env) {
        this(owner, env, Map.of(
                "chart-type", "LINE_CHART",
                "header", Arrays.asList("Tick", "# of agents")
        ));
    }

    @Override
    public void activate() {
        getOwner().addProbe(probe);
        super.activate();
    }

    @Override
    protected void collectData() {
        export(List.of(getCurrentTime(), probe.size()));
    }
}
