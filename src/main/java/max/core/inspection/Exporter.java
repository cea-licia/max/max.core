/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.inspection;

/**
 * Interface for all kind of exporters.
 *
 * An {@link Exporter} is an object capable of exporting some data. It can be in a database, in a file or even in a
 * graphical interface.
 *
 * Exporters have (at least) three methods:
 * <ul>
 *     <li>{@link #activate()}: called one time and before all other two methods. You can perform some setup operations
 *     here</li>
 *     <li>{@link #export(Object)}: called each time some data must be exported</li>
 *     <li>{@link #end()}: last method called, perform your cleanup operations here</li>
 * </ul>
 *
 * @param <D> Structure of the exportable data
 * @author Pierre Dubaillay
 */
public interface Exporter<D> {

    /**
     * Exports the given data (whatever export means in te context of your own exporter).
     *
     * @param data data to export
     */
    void export(D data);

    /**
     * Activates for the first time this exporter.
     */
    void activate();

    /**
     * Called when the exporter is no longer needed.
     */
    void end();
}
