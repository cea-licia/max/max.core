/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core;

/**
 * Observer pattern for events.
 *
 * <p>Any type implementing this interface becomes an event generator. Listeners can also
 * (de)register themselves.
 *
 * @author Önder Gürcan
 * @author François Le Fevre
 * @since 1.0
 * @param <A> address type of listeners
 * @see IEventListener
 */
public interface IEventGenerator<A> {

	/**
	 * Registers a given observer with its address.
	 *
	 * @param address observer's address
	 * @param obj the observer itself
	 */
	void register(A address, IEventListener obj);
	
	/**
	 * Unregisters an observer using its address.
	 * @param address the observer's address
	 */
	void unregister(A address);
	
	/**
	 * Notifies the observers of message delivery.
	 *
	 * @param address address of the observer
	 * @param event the emitted event
	 */
	void notify(A address, EnvironmentEvent event);
	
}

