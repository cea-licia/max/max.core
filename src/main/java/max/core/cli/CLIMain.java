/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import java.util.logging.Level;

/**
 * The main class when a model is launched from the command line interface.
 *
 * <p>The CLI has two commands: the run command, which permits running an experiment and the plot
 * command, which can plot data if requirements are fulfilled.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public class CLIMain {

  // STATICS

  /** A set of options available to all commands. Only contains the 'help' option for now. */
  private static class MainCommand {
    @Parameter(
        names = {"-h", "--help"},
        help = true,
        description = "Display program usage.")
    private boolean helpFlag;
  }

  /**
   * Entry point of the CLI interface.
   *
   * @param args arguments given in the terminal
   */
  public static void main(String[] args) {
    final var mainCommand = new MainCommand();
    final var runCommand = new RunCommand();
    final var plotCommand = new PlotCommand();

    final var jc =
        JCommander.newBuilder()
            .addObject(mainCommand)
            .addCommand(runCommand)
            .addCommand(plotCommand)
            .programName("<your-model>.jar")
            .build();

    try {
      jc.parse(args);
    } catch (final Exception e) { // Print usage if we can't parse, rather than a stacktrace
      jc.usage();
      return;
    }

    // If help flag is set, then display usage and return
    if (mainCommand.helpFlag) {
      jc.usage();
      return;
    }

    // Get the issued command and run it
    final var command = jc.getParsedCommand();
    if ("run".equals(command)) {
      executeCommand(runCommand);
    } else if ("plot".equals(command)) {
      executeCommand(plotCommand);
    } else {
      jc.usage();
    }
  }

  /**
   * Executes the given command.
   *
   * <p>If the command throws a {@link CommandException}, then the command logger is used to log the
   * error (with FINE level) and the program exits with a non-zero status code.
   *
   * @param command Command to execute
   */
  private static void executeCommand(AbstractCommand command) {
    try {
      command.execute();
    } catch (CommandException e) {
      command.getLogger().log(Level.FINE, "An error occurred.", e);
      System.exit(-1);
    }
  }
}
