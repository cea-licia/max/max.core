/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.logging.Level;
import java.util.logging.Logger;
import max.core.MAXParameters;
import max.core.agent.ExperimenterAgent;
import max.core.cli.configuration.ConfigurationLoader;
import max.core.cli.configuration.ConfigurationLoadingException;
import max.core.cli.configuration.JSONConfigurationLoader;
import max.core.cli.configuration.XMLConfigurationLoader;

/**
 * The run command.
 *
 * <p>Defines one required parameter name CONFIG_FILE which must point to the experimenter's
 * configuration file.
 *
 * <p>Also defines an option called 'clear', which removes the previous results if any.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
@Parameters(commandDescription = "Run experiments.", commandNames = "run")
public class RunCommand extends AbstractCommand {

  /** Class logger. */
  private static final Logger LOGGER = Logger.getLogger(RunCommand.class.getName());

  /** Key to find the experimenter class name in the configuration file. */
  private static final String EXPERIMENTER_CLASS_KEY = "MAX_CORE_EXPERIMENTER_NAME";

  @Parameter(description = "CONFIG_FILE", required = true)
  private String configFile;

  @Parameter(
      names = {"-c", "--clear"},
      description = "Clear previous results")
  private boolean clearFlag;

  @Override
  public void execute() throws CommandException {
    // Create configuration loader according to file extension.
    ConfigurationLoader loader = new XMLConfigurationLoader(configFile);
    final var split = configFile.split("\\.");

    if ("json".equals(split[split.length - 1])) {
      loader = new JSONConfigurationLoader(Path.of(configFile));
      getLogger().info("Using the JSON logger");
    } else {
      getLogger().info("Using the XML loader.");
    }

    // Try to load the configuration
    getLogger().info("Loading configuration");
    try {
      loader.load();
    } catch (ConfigurationLoadingException e) {
      throw new CommandException(e);
    }
    getLogger().info("Configuration loaded.");

    final var experimenter =
        ExperimenterAgent.fromClassName(MAXParameters.getParameter(EXPERIMENTER_CLASS_KEY));
    getLogger().fine("Experimenter created.");

    if (clearFlag) {
      try {
        clean(experimenter.getOutputFolder().getParent());
      } catch (IOException e) {
        getLogger().severe("Unable to clean previous results.");
        throw new CommandException(e);
      }
    }

    getLogger().info("Running experiment.");
    runExperiment(experimenter);
    getLogger().info("Done.");
  }

  @Override
  protected Logger getLogger() {
    return LOGGER;
  }

  /**
   * Creates a madkit kernel, launch the provided experimenter and then wait until the simulation
   * ends.
   *
   * @param experimenterAgent the experimenter to launch
   */
  private void runExperiment(ExperimenterAgent experimenterAgent) {

    ExperimenterAgent.launchExperimenter(experimenterAgent);

    final var barrier = experimenterAgent.isSimulationEnded();
    synchronized (barrier) {
      while (!barrier.get()) {
        try {
          barrier.wait();
        } catch (InterruptedException e) {
          Thread.currentThread().interrupt();
          getLogger().log(Level.SEVERE, "Wait interrupted.", e);
        }
      }
    }
  }

  /**
   * Clean the given folder (recursive delete).
   *
   * @param folder folder to clean
   * @throws IOException unable to delete some file or folder
   */
  private void clean(Path folder) throws IOException {
    Files.walkFileTree(
        folder,
        new SimpleFileVisitor<>() {
          @Override
          public FileVisitResult visitFile(Path file, BasicFileAttributes attrs)
              throws IOException {
            Files.delete(file);
            return FileVisitResult.CONTINUE;
          }

          @Override
          public FileVisitResult postVisitDirectory(Path dir, IOException e) throws IOException {
            if (e == null) {
              Files.delete(dir);
              return FileVisitResult.CONTINUE;
            } else {
              throw e;
            }
          }
        });
  }
}
