/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;
import com.beust.jcommander.converters.PathConverter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Plot command.
 *
 * <p>Given a file or a folder, this command reads the file(s) and creates a diagram. Requires R to
 * be installed.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
@Parameters(
    commandDescription = "Plot data contained in a file or in a directory.",
    commandNames = "plot")
public class PlotCommand extends AbstractCommand {

  /** Command's logger. */
  private static final Logger LOGGER = Logger.getLogger(PlotCommand.class.getName());

  /** Defaults. */
  private static final String DEFAULT_OUT_FOLDER = "out";
  private static final String DEFAULT_LOG_FOLDER = "logs";
  private static final Path DEFAULT_R_PATH = Path.of("rscript");

  /** Where the plot script is located in the jar. */
  public static final String PLOT_SCRIPT_LOCATION = "/script/PlotSimpleChart.R";

  /** Input path provided by the CLI user. Can be either a file or a folder. */
  @Parameter(description = "INPUT_FILE_OR_FOLDER", converter = PathConverter.class, required = true)
  private Path inputPath;

  /** Optional output folder. Default is $inputPath/out. */
  @Parameter(
      names = {"-o", "--output"},
      description = "Output folder.",
      converter = PathConverter.class)
  private Path outputPath;

  /** Optional path to the R executable. Default is 'rscript'. */
  @Parameter(
      names = {"--rpath"},
      description = "Path to the R executable",
      converter = PathConverter.class)
  private Path rPath;

  /** Where R logs are written. */
  private Path logsFolderPath;

  @Override
  protected Logger getLogger() {
    return LOGGER;
  }

  @Override
  public void execute() throws CommandException {
    // Setup environment: set default values if not provided and create folders.
    setupDefaults();
    createOutputFolder();
    createLogsFolder();
    getLogger().info("Folders created.");

    // Plot in parallel
    final var plottingScript = extractInternalPlotRScript();
    final List<Path> dataFiles;

    getLogger().info(() -> "Plot script extracted to " + plottingScript);
    if (Files.isDirectory(inputPath)) {
      try (final var stream = Files.list(inputPath)) {
        dataFiles = stream.collect(Collectors.toList());
      } catch (IOException e) {
        getLogger().severe("Unable to list data files in " + inputPath);
        throw new CommandException(e);
      }
    } else {
      dataFiles = List.of(inputPath);
    }

    getLogger().info("Plotting ...");
    for (final var f : dataFiles) {
      final var outputFile = buildOutputFromInput(f);
      final var logFile = buildLogFromInput(f);
      plot(f, outputFile, plottingScript, logFile);
      getLogger().fine(() -> "Plotted " + f + " in " + outputFile);
    }
    getLogger().info("Done.");
  }

  /**
   * Given an input csv file, plots the data in png format.
   *
   * @param input path of the input file
   * @param output path to the output file
   * @param plotScript path to the R script used to plot data
   * @param logFile path to the log file
   * @throws CommandException The plotting failed
   */
  private void plot(Path input, Path output, Path plotScript, Path logFile)
      throws CommandException {
    // Configure process
    final var process =
        new ProcessBuilder(
            rPath.toString(), plotScript.toString(), input.toString(), output.toString());
    process.redirectOutput(logFile.toFile());
    process.redirectError(logFile.toFile());

    // Execute
    try {
      final var plottingProgram = process.start();
      final var exitCode = plottingProgram.waitFor();
      if (exitCode != 0) {
        throw new CommandException();
      }
    } catch (IOException e) {
      getLogger().severe("Unable to start the plotting program.");
      throw new CommandException(e);
    } catch (InterruptedException e) {
      getLogger().severe("MAX main thread interrupted while waiting plotting termination.");
      Thread.currentThread().interrupt();
      throw new CommandException(e);
    }
  }

  /**
   * Given an input file path, builds a path to the output png file.
   *
   * @param input path to the csv file
   * @return a path to the png one
   */
  private Path buildOutputFromInput(Path input) {
    return outputPath.resolve(input.getFileName().toString().split("\\.")[0] + ".png");
  }

  /**
   * Given an input file path, builds a path to the log file.
   *
   * @param input path to the csv file
   * @return a path to the log one
   */
  private Path buildLogFromInput(Path input) {
    return logsFolderPath.resolve(input.getFileName().toString().split("\\.")[0] + ".log");
  }

  /**
   * Extracts the R script located in the final jar and returns the path to it.
   *
   * @return the path to the extracted script
   * @throws CommandException the script can't be extracted
   */
  private Path extractInternalPlotRScript() throws CommandException {
    final var inputStream = PlotCommand.class.getResourceAsStream(PLOT_SCRIPT_LOCATION);
    if (inputStream != null) {
      try {
        final var extracted = Files.createTempFile(null, null);
        Files.copy(inputStream, extracted, StandardCopyOption.REPLACE_EXISTING);
        return extracted;
      } catch (IOException e) {
        getLogger().severe("Unable to extract the plotting script.");
        throw new CommandException(e);
      } finally {
        try {
          inputStream.close();
        } catch (IOException e) {
          // Ignore error
        }
      }
    }
    getLogger().severe("Unable to find the packaged plotting script.");
    throw new CommandException();
  }

  /**
   * Sets up default values if the CLI user didn't use them.
   */
  private void setupDefaults() {
    if (outputPath == null) {
      if (Files.isRegularFile(inputPath)) {
        outputPath = inputPath.getParent().resolve(DEFAULT_OUT_FOLDER);
      } else {
        outputPath = inputPath.resolve(DEFAULT_OUT_FOLDER);
      }
    }
    if (rPath == null) {
      rPath = DEFAULT_R_PATH;
    }
    logsFolderPath = outputPath.resolve(DEFAULT_LOG_FOLDER);
  }

  /**
   * Creates the output folder.
   *
   * @throws CommandException folder creation failed
   */
  private void createOutputFolder() throws CommandException {
    createFolder(outputPath);
  }

  /**
   * Creates the logs' folder.
   *
   * @throws CommandException folder creation failed
   */
  private void createLogsFolder() throws CommandException {
    createFolder(logsFolderPath);
  }

  /**
   * Given a folder's path, creates all non-existing folders in the hierarchy.
   *
   * @param folder folder's path
   * @throws CommandException the folder (or one of its parents) can't be created.
   */
  private void createFolder(Path folder) throws CommandException {
    if (!Files.exists(folder)) {
      try {
        Files.createDirectories(folder);
      } catch (IOException e) {
        getLogger().severe("Unable to create folder '" + folder + "'.");
        throw new CommandException(e);
      }
    }
  }
}
