/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli;

import java.util.logging.Logger;

/**
 * Base command.
 *
 * <p>Provides useful methods for all concrete commands.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
public abstract class AbstractCommand {

  /**
   * Gets the class logger.
   *
   * @return the logger, never {@code null}.
   */
  protected abstract Logger getLogger();

  /**
   * Executes what this command is supposed to do.
   *
   * @throws CommandException something went wrong and the program should exit with a non-zero
   *     status code
   */
  public abstract void execute() throws CommandException;
}
