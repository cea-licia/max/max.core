/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli;

/**
 * An exception dedicated to CLI commands. Indicates that something went wrong and that the program
 * should exit with a non-zero status.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
class CommandException extends Exception {

  /**
   * Creates a new {@link CommandException} instance.
   *
   * @param cause the exception that caused the failure
   */
  public CommandException(Throwable cause) {
    super(cause);
  }

  /**
   * Creates a new {@link CommandException} instance.
   */
  public CommandException() {
    super();
  }
}
