/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli.configuration;

/**
 * Common interface for all configuration loaders.
 *
 * <p>It declares a unique method call {@link #load()}, which must populate {@link
 * max.core.MAXParameters} if the configuration can be loaded. Otherwise, a {@link
 * ConfigurationLoadingException} should be thrown.
 *
 * @author Pierre Dubaillay
 */
@FunctionalInterface
public interface ConfigurationLoader {

  /**
   * Loads the configuration and populates {@link max.core.MAXParameters}.
   *
   * @throws ConfigurationLoadingException the configuration can't be loaded.
   */
  void load() throws ConfigurationLoadingException;
}
