/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli.configuration;

/**
 * Exception indicating the failure during the load of a configuration.
 *
 * @author Pierre Dubaillay
 */
public class ConfigurationLoadingException extends Exception {

  /**
   * Creates a new {@link ConfigurationLoadingException} instance, without any message or inner
   * exception.
   */
  public ConfigurationLoadingException() {
    super();
  }

  /**
   * Creates a new {@link ConfigurationLoadingException} instance with the provided cause message.
   *
   * @param message an explanation about what caused the exception.
   */
  public ConfigurationLoadingException(String message) {
    super(message);
  }

  /**
   * Creates a new {@link ConfigurationLoadingException} instance with both a cause message and an
   * inner exception.
   *
   * @param message an explanation about what caused the exception
   * @param inner an inner exception
   */
  public ConfigurationLoadingException(String message, Throwable inner) {
    super(message, inner);
  }
}
