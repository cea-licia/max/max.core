/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli.configuration;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.networknt.schema.JsonSchema;
import com.networknt.schema.JsonSchemaFactory;
import com.networknt.schema.SpecVersion.VersionFlag;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import max.core.MAXParameters;

/**
 * JSON format configuration loader.
 *
 * <p>Given a file path, the loader will try to load the configuration. Notice that the user
 * configuration file is validated against the parameters' schema.
 *
 * @author Pierre Bubaillay
 */
public class JSONConfigurationLoader implements ConfigurationLoader {

  /** JSON validation factory, configured to recognize V2019-09 specification. */
  private static final JsonSchemaFactory JSON_FACTORY =
      JsonSchemaFactory.getInstance(VersionFlag.V201909);

  /** Path to the configuration file. */
  private final Path configFile;

  /** The schema the configuration will be validated against. */
  private final JsonSchema schema;

  /**
   * Creates a new {@link JSONConfigurationLoader} instance.
   *
   * @param configFile path to the file containing the configuration
   */
  public JSONConfigurationLoader(Path configFile) {
    this.configFile = configFile;

    InputStream is =
        Thread.currentThread()
            .getContextClassLoader()
            .getResourceAsStream("max/core/schema/parameters-v1.json");
    this.schema = JSON_FACTORY.getSchema(is);
  }

  @Override
  public void load() throws ConfigurationLoadingException {
    final var mapper = new ObjectMapper();
    try {
      final var node = mapper.readTree(configFile.toFile());
      validate(node);
      mapProperties(node);
    } catch (IOException e) {
      throw new ConfigurationLoadingException(
          "Unable to parse JSON configuration file at " + configFile, e);
    }
  }

  /**
   * Validates the loaded JSON against the JSON Schema.
   *
   * <p>If the json file is not valid, then an exception is thrown. Only the first issue is
   * considered.
   *
   * @param node root node of the JSON file
   * @throws ConfigurationLoadingException the provided JSON is not valid
   */
  private void validate(JsonNode node) throws ConfigurationLoadingException {
    final var messages = schema.validate(node);
    final var error = messages.stream().findFirst();

    if (error.isPresent()) {
      throw new ConfigurationLoadingException(error.get().getMessage());
    }
  }

  /**
   * Given a validated JSON configuration, maps each entry into a valid property and stores it in
   * {@link MAXParameters}.
   *
   * @param node JSON configuration
   */
  private void mapProperties(JsonNode node) {
    // Simulator section
    final var simulator = node.get("simulator");

    final var step = simulator.get("step");
    MAXParameters.setParameter("MAX_CORE_SIMULATION_STEP", step.asText());

    final var execution = simulator.get("execution");
    if (execution != null) {
      MAXParameters.setParameter("MAX_CORE_ACTION_EXECUTION_STRATEGY", execution.asText());
    }

    final var ui = simulator.get("ui");
    if (ui != null) {
      MAXParameters.setParameter("MAX_CORE_UI_MODE", ui.asBoolean() ? "GUI" : "SILENT");
    }

    final var desktop = simulator.get("desktop");
    if (desktop != null) {
      MAXParameters.setParameter("MAX_CORE_DESKTOP_FLAG", desktop.asText());
    }

    // experiment section
    final var experiment = node.get("experiment");

    final var experimenterClass = experiment.get("experimenter-class");
    MAXParameters.setParameter("MAX_CORE_EXPERIMENTER_NAME", experimenterClass.asText());

    final var timeout = experiment.get("timeout");
    MAXParameters.setParameter("MAX_CORE_TIMEOUT_TICK", timeout.asText());

    final var resultsFolder = experiment.get("results-folder");
    if (resultsFolder != null) {
      MAXParameters.setParameter("MAX_CORE_RESULTS_FOLDER_NAME", resultsFolder.asText());
    }

    final var properties = experiment.get("properties");
    if (properties != null) {
      for (final var propsIterator = properties.fields(); propsIterator.hasNext(); ) {
        final var property = propsIterator.next();
        MAXParameters.setParameter(property.getKey(), property.getValue().asText());
      }
    }
  }
}
