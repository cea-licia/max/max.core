/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.cli.configuration;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.InvalidPropertiesFormatException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import max.core.MAXParameters;
import max.core.cli.CLIMain;

/**
 * XML format configuration loader.
 *
 * <p>Given a file path, the loader will first try to load the file as an external one. If the file
 * is not found, then the loader will try to find the file inside the jar.
 *
 * @author Pierre Dubaillay
 */
public class XMLConfigurationLoader implements ConfigurationLoader {

  /** Class's logger. */
  private static final Logger LOGGER = Logger.getLogger(XMLConfigurationLoader.class.getName());

  /** Configuration file's path. */
  private final String configFile;

  /**
   * Creates a new {@link XMLConfigurationLoader} instance.
   *
   * @param configFile file's path to load
   */
  public XMLConfigurationLoader(String configFile) {
    this.configFile = configFile;
  }

  /**
   * Try to load the configuration file in {@link MAXParameters}.
   *
   * <p>This method will first try to load the file as an external one. If the file doesn't exist,
   * then it tries to consider it as an in-jar file. If the latter fails too, no configuration is
   * loaded and the method throws an exception.
   *
   * @throws ConfigurationLoadingException the provided file is malformed or the internal
   *     configuration file can't be loaded for some reason.
   */
  @Override
  public void load() throws ConfigurationLoadingException {
    final var properties = new Properties();
    var loaded = false;

    // Start by considering the provided file as an external one.
    try {
      final var asPath = Path.of(configFile);
      properties.loadFromXML(Files.newInputStream(asPath));
      loaded = true;
    } catch (InvalidPropertiesFormatException e) {
      throw new ConfigurationLoadingException(
          "Unable to load external configuration file " + configFile, e);
    } catch (IOException e) {
      LOGGER.log(
          Level.INFO,
          () ->
              "Unable to load external configuration file "
                  + configFile
                  + ", maybe it is internal ?");
    }

    // If we couldn't read the file, try to consider it as an internal (jar) file
    if (!loaded) {
      final var loader = CLIMain.class.getClassLoader();
      final var stream = loader.getResourceAsStream(configFile);
      if (stream != null) {
        try {
          properties.loadFromXML(stream);
        } catch (IOException e) {
          throw new ConfigurationLoadingException(
              "Unable to load internal configuration file " + configFile, e);
        }
      } else {
        throw new ConfigurationLoadingException(
            "Internal configuration file doesn't exists " + configFile);
      }
    }

    // If some configurations file has been loaded, then override MAXParameters
    MAXParameters.loadParameters(properties);
  }
}
