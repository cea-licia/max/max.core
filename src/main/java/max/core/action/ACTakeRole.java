/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.action;

import madkit.kernel.AbstractAgent;
import max.core.Community;
import max.core.agent.MAXAgent;
import max.core.role.Role;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Special action to take a role in an environment.
 * All intermediary roles are taken too.
 *
 * @param <A> Agent's type
 * @max.role {@link Role}
 * @author Pierre Dubaillay
 */
public class ACTakeRole<A extends MAXAgent> extends Action<A> {

    /** Roles to take. */
    private final Collection<Class<? extends Role>> rolesToTake;

    /** Environment to play inside. */
    private final String environmentToPlayInside;

    /**
     * Default constructor.
     *
     * @param environment the environment to join
     * @param role        the role to take
     * @param owner       the owner of this action
     */
    public ACTakeRole(String environment, Class<? extends Role> role, final A owner) {
        this(environment, getIntermediaryRoles(role), owner);
    }

    /**
     * Copy constructor, avoiding to recalculate intermediary roles.
     *
     * @param env Environment to join
     * @param roles All roles to take
     * @param owner Action's owner
     */
    private ACTakeRole(final String env, final Collection<Class<? extends Role>> roles, final A owner) {
        super(Community.SIMULATION_ENVIRONMENT, Role.class, owner);
        rolesToTake = new ArrayList<>(roles);
        environmentToPlayInside = env;
    }

    @Override
    public void execute() {
        for (final var role : rolesToTake) {
            takeRole(role);
        }
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new ACTakeRole(environmentToPlayInside, rolesToTake, getOwner());
    }

    /**
     * Given a role, make the action's owner take the role inside the action's environmentToPlayInside environment.
     *
     * @param role role to take
     * @throws IllegalArgumentException <code>role</code> is {@code null}
     */
    private void takeRole(Class<? extends Role> role) {
        if (Objects.isNull(role)) throw new IllegalArgumentException("role is null");

        final var owner = getOwner();

        // Check if agent can play the role
        if (!owner.canPlayRole(role)) {
            owner.getLogger().severe("Role " + role.getName() + " can't be played by agent.");
            return;
        }

        // Avoid taking a role we already have
        if (owner.getMyRoles(environmentToPlayInside).contains(role)) {
            return;
        }

        // Call kernel to take the role
        final AbstractAgent.ReturnCode requestRole = owner.requestRole(
                Community.COMMUNITY, environmentToPlayInside, role.getName());
        if (AbstractAgent.ReturnCode.SUCCESS.equals(requestRole)) {
            owner.getLogger().info(() ->
                    "Requested role " + role.getName() + " in " + environmentToPlayInside);
        } else {
            owner.getLogger().severe(
                    "Unable to take role " + role.getName() + " in " + environmentToPlayInside);
            owner.getLogger().fine(requestRole::toString);
        }
    }

    /**
     * Lookup all super roles of the provided role. Only retains classes and interfaces implementing/extending
     * {@link Role}.
     *
     * @param role the root role
     * @return intermediary roles, plus the provided one
     */
    private static Set<Class<? extends Role>> getIntermediaryRoles(final Class<? extends Role> role) {
        assert role != null && role.isInterface();

        return walkClassHierarchy(role)
                .filter(Role.class::isAssignableFrom)
                .filter(r -> !Role.class.equals(r)) // Exclude root Role
                .collect(Collectors.toSet());
    }

    /**
     * Find all implemented interfaces of the provided type.
     * Notice that the stream can contain duplicates.
     *
     * @param root type you want to explore the hierarchy
     * @return a stream containing all super classes and implemented interfaces, plus the root type.
     */
    private static Stream<Class<? extends Role>> walkClassHierarchy(final Class<? extends Role> root) {
        assert root != null && root.isInterface();

        return Stream.concat(
                Stream.of(root),
                Arrays.stream(root.getInterfaces())
                        .flatMap(i -> ACTakeRole.walkClassHierarchy((Class<? extends Role>) i))
        );
    }
}