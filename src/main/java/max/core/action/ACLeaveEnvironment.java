/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.action;

import max.core.Community;
import max.core.agent.MAXAgent;
import max.core.role.Role;

/**
 * Leave an environment.
 * This will abandon all roles taken in the environment and remove associated context as side effect.
 *
 * @param <A> Agent's type
 * @max.role {@link Role}
 * @author Pierre Dubaillay
 */
public class ACLeaveEnvironment<A extends MAXAgent> extends Action<A> {

    /** The environment to leave. */
    private final String envToLeave;

    /**
     * Default constructor.
     *
     * @param environment environment to leave
     * @param owner action's owner
     */
    public ACLeaveEnvironment(final String environment, final A owner) {
        super(Community.SIMULATION_ENVIRONMENT, Role.class, owner);
        envToLeave = environment;
    }

    @Override
    public void execute() {
        getOwner().leaveGroup(Community.COMMUNITY, envToLeave);
        getOwner().getLogger().info( "Leaving environment " + envToLeave);
    }

    @Override
    public <T extends Action<A>> T copy() {
        return (T) new ACLeaveEnvironment(envToLeave, getOwner());
    }
}