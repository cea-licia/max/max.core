/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.action;

import max.core.EnvironmentEvent;
import max.core.agent.SimulatedAgent;
import max.core.role.Role;

/**
 * A refinement of {@link Action}.
 * This particular action is designed to be scheduled when an {@link EnvironmentEvent} occurs.
 *
 * @see Plan#getEventBoundActions()
 * @param <A> Owner's type
 * @author Pierre Dubaillay
 */
public abstract class ReactiveAction<A extends SimulatedAgent> extends Action<A> {

    /** The event that triggered action execution. */
    private EnvironmentEvent eventToReact;

    /**
     * Default constructor.
     *
     * @param environment the environment that this action will be executed in
     * @param role        the role that this action belongs to
     * @param owner       the owner of this action
     * @param inputs      inputs
     */
    protected ReactiveAction(String environment, Class<? extends Role> role, A owner, Object... inputs) {
        super(environment, role, owner, inputs);
    }

    /**
     * Set the event that triggers the action.
     *
     * @param eventToReact the event
     */
    public void setEventToReact(EnvironmentEvent eventToReact) {
        this.eventToReact = eventToReact;
    }

    /**
     * Get the event that triggered the action.
     *
     * @param <T> Real type of the event
     * @return the event
     */
    protected <T extends EnvironmentEvent> T getEventToReact() {
        return (T) eventToReact;
    }
}
