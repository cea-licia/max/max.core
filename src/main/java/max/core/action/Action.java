/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.action;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import max.core.Community;
import max.core.agent.MAXAgent;
import max.core.role.Role;
import max.core.scheduling.ActionActivator;
import max.core.scheduling.ActivationSchedule;
import max.core.scheduling.ActivationScheduleFactory;

/**
 * An action is an element that is the fundamental unit of executable
 * functionality. The execution of an action represents some transformation or
 * processing in the modeled system. An action execution represents the run-time
 * behavior of executing an action within a specific role execution. An action
 * is considered to take zero time and cannot be interrupted.
 * 
 * @author Önder Gürcan
 * @author Pierre Dubaillay
 * @param <A> Owner type
 */
public abstract class Action<A extends MAXAgent> {

	/** Community where the action runs. For internal use only. */
	private final String community;

	/** In which environment the action runs. */
	private final String environment;

	/** The required role at execution type the owner must have. */
	private final Class<? extends Role> role;

	/** The action's owner. */
	private A owner;

	/** A list of additional inputs. */
	private final List<Object> inputs;

	/**
	 * Create a new action running in the given environment, requiring the given role and having the provided owner.
	 *
	 * @param environment Where the action will run
	 * @param role What role is required
	 * @param owner Action's owner
	 * @param inputs Additional inputs
	 */
	protected Action(String environment, Class<? extends Role> role, A owner, Object... inputs) {
		this.community = Community.COMMUNITY;
		this.environment = environment;
		this.role = role;
		this.owner = owner;
		this.inputs = Arrays.asList(inputs);
	}

	/**
	 * Make a copy of the current object.
	 * It is up to concrete implementations to decide how to implement it. Default implementation is
	 * to throw an {@link UnsupportedOperationException}
	 *
	 * @param <T> The action's type
	 * @return A copy of the current instance.
	 */
	public <T extends Action<A>> T copy() {
		throw new UnsupportedOperationException();
	}

	/**
	 * Get the Madkit community associated to the action.
	 * @return Madkit community
	 */
	public final String getCommunity() {
		return community;
	}

	/**
	 * Get the environment in which the action will be executed.
	 * @return environment's name
	 */
	public final String getEnvironment() {
		return environment;
	}

	/**
	 * Get the role required to play this action.
	 * @return the role
	 */
	public final Class<? extends Role> getRole() {
		return role;
	}

	/**
	 * Get the current action's owner.
	 * @return action's owner
	 */
	public final A getOwner() {
		return owner;
	}

	/**
	 * Get additional parameter passed to the constructor.
	 * @param index index in the parameters array
	 * @return the parameter's value
	 */
	public final Object getInput(int index) {
		return this.inputs.get(index);
	}

	/**
	 * Set the action's owner after action's creation. Replace the old one.
	 * @param newOwner the new action's owner
	 */
	public final void setOwner(final A newOwner) {
		this.owner = newOwner;
	}

	/**
	 * Execute owner agent action(s) if owner is still in environment and
	 * playing role.
	 */
	public abstract void execute();
	
	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action only
	 * one time. The action will start at the specified time.
	 *
	 * @param start the start time
	 * @return an {@link ActionActivator} appropriate for scheduling a non-repeating
	 *         action
	 */
	public ActionActivator<A> oneTime(final BigDecimal start) {
		return new ActionActivator<A>(ActivationScheduleFactory.createOneTime(start), this);
	}
	
	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action only
	 * one time. The action will start at the specified time.
	 *
	 * @param start the start time
	 * @return an {@link ActionActivator} appropriate for executing this action only
	 *         one time
	 */
	public ActionActivator<A> oneTime(final long start) {
		return oneTime(BigDecimal.valueOf(start));
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action in a
	 * finitely-repeating fachion. The action will start at the specified time.
	 *
	 * @param start    the start time
	 * @param stop     the stop time
	 * @param interval how much time between each activation
	 * @return an {{@link ActionActivator} appropriate for executing this action in
	 *         a finitely-repeating fachion
	 */
	public ActionActivator<A> repeatFinitely(final BigDecimal start, final BigDecimal stop, final BigDecimal interval) {
		return new ActionActivator<A>(ActivationScheduleFactory.createRepeatingFinitely(start, stop, interval), this);
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action in a
	 * finitely-repeating fachion. The action will start at the specified time.
	 *
	 * @param start    the start time
	 * @param stop     the stop time
	 * @param interval how much time between each activation
	 * @return an {{@link ActionActivator} appropriate for executing this action in
	 *         a finitely-repeating fachion
	 */
	public ActionActivator<A> repeatFinitely(final long start, final long stop, final long interval) {
		return repeatFinitely(BigDecimal.valueOf(start), BigDecimal.valueOf(stop), BigDecimal.valueOf(interval));
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action in
	 * an infinitely-repeating fashion. The behavior will start at the specified
	 * time.
	 *
	 * @param start    the start time
	 * @param interval how much time between each activation
	 * @return an {@link ActivationSchedule} appropriate for executing this action
	 *         in an infinitely-repeating fashion
	 */
	public ActionActivator<A> repeatInfinitely(final BigDecimal start, final BigDecimal interval) {
		return new ActionActivator<A>(ActivationScheduleFactory.createRepeatingInfinitely(start, interval), this);
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action in
	 * an infinitely-repeating fashion. The behavior will start at the specified
	 * time.
	 *
	 * @param start    the start time
	 * @param interval how much time between each activation
	 * @return an {@link ActivationSchedule} appropriate for executing this action
	 *         in an infinitely-repeating fashion
	 */
	public ActionActivator<A> repeatInfinitely(final long start, final long interval) {
		return repeatInfinitely(BigDecimal.valueOf(start), BigDecimal.valueOf(interval));
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action in
	 * an infinitely-repeating fashion following a normal distribution. Two normals
	 * are used : one to get the start time and another one to get delta times
	 * between activations.
	 *
	 * @param meanStartT mean value for the normal used to get the start time
	 * @param sdStartT   standard deviation for the normal used to get the start
	 *                   time
	 * @param meanDeltaT mean value for the normal used to get time amount between
	 *                   activations
	 * @param sdDeltaT   standard deviation for the normal used to get time amount
	 *                   between activations
	 * @return an {@link ActionActivator} appropriate for executing this action in
	 *         an infinitely-repeating fashion
	 */
	public ActionActivator<A> repeateInfinitelyNormalDistribution(final BigDecimal meanStartT,
			final BigDecimal sdStartT, final BigDecimal meanDeltaT, final BigDecimal sdDeltaT) {
		return new ActionActivator<A>(ActivationScheduleFactory.createRepeatingInfinitelyNormalDistribution(meanStartT,
				sdStartT, meanDeltaT, sdDeltaT), this);
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action in
	 * an infinitely-repeating fashion following a normal distribution. Two normals
	 * are used : one to get the start time and another one to get delta times
	 * between activations.
	 *
	 * @param meanStartT mean value for the normal used to get the start time
	 * @param sdStartT   standard deviation for the normal used to get the start
	 *                   time
	 * @param meanDeltaT mean value for the normal used to get time amount between
	 *                   activations
	 * @param sdDeltaT   standard deviation for the normal used to get time amount
	 *                   between activations
	 * @return an {@link ActionActivator} appropriate for executing this action in
	 *         an infinitely-repeating fashion
	 */
	public ActionActivator<A> repeatInfinitelyNormalDistribution(final long meanStartT, final long sdStartT,
			final long meanDeltaT, final long sdDeltaT) {
		return repeateInfinitelyNormalDistribution(BigDecimal.valueOf(meanStartT), BigDecimal.valueOf(sdStartT),
				BigDecimal.valueOf(meanDeltaT), BigDecimal.valueOf(sdDeltaT));
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action an
	 * infinitely-repeating fashion following a uniform distribution. Two uniforms
	 * are used : one to get the start time and another one to get delta times
	 * between activations.
	 *
	 * @param minStartT minimum value of the uniform distribution used for roll the
	 *                  start time value
	 * @param maxStartT maximum value of the uniform distribution used for roll the
	 *                  start time value
	 * @param minDeltaT minimum value of the uniform distribution used for roll the
	 *                  time amount between two activations
	 * @param maxDeltaT minimum value of the uniform distribution used for roll the
	 *                  time amount between two activations
	 * @return an {@link ActionActivator} appropriate for executing this action an
	 *         infinitely-repeating fashion following a uniform distribution
	 */
	public ActionActivator<A> repeateInfinitelyUniformDistribution(final BigDecimal minStartT,
			final BigDecimal maxStartT, final BigDecimal minDeltaT, final BigDecimal maxDeltaT) {
		return new ActionActivator<A>(ActivationScheduleFactory.createRepeatingInfinitelyUniformDistribution(minStartT,
				maxStartT, minDeltaT, maxDeltaT), this);
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action an
	 * infinitely-repeating fashion following a uniform distribution. Two uniforms
	 * are used : one to get the start time and another one to get delta times
	 * between activations.
	 *
	 * @param minStartT minimum value of the uniform distribution used for roll the
	 *                  start time value
	 * @param maxStartT maximum value of the uniform distribution used for roll the
	 *                  start time value
	 * @param minDeltaT minimum value of the uniform distribution used for roll the
	 *                  time amount between two activations
	 * @param maxDeltaT minimum value of the uniform distribution used for roll the
	 *                  time amount between two activations
	 * @return an {@link ActionActivator} appropriate for executing this action an
	 *         infinitely-repeating fashion following a uniform distribution
	 */
	public ActionActivator<A> repeateInfinitelyUniformDistribution(final long minStartT, final long maxStartT,
			final long minDeltaT, final long maxDeltaT) {
		return repeateInfinitelyUniformDistribution(BigDecimal.valueOf(minStartT), BigDecimal.valueOf(maxStartT),
				BigDecimal.valueOf(minDeltaT), BigDecimal.valueOf(maxDeltaT));
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action in
	 * an infinitely-repeating fashion following a poisson distribution. Two Poisson
	 * distributions are used : one to get the start time and another one to get
	 * delta times between activations.
	 *
	 * @param meanStartT mean value of the Poisson distribution used for roll the
	 *                   start time value
	 * @param meanDeltaT mean value of the Poisson distribution used for roll the
	 *                   time amount between two activations
	 * @return an {@link ActionActivator} appropriate for executing this action in
	 *         an infinitely-repeating fashion following a poisson distribution
	 */
	public ActionActivator<A> repeateInfinitelyPoissonDistribution(final BigDecimal meanStartT,
			final BigDecimal meanDeltaT) {
		return new ActionActivator<A>(
				ActivationScheduleFactory.createRepeatingInfinitelyPoissonDistribution(meanStartT, meanDeltaT), this);
	}

	/**
	 * Creates an {@link ActionActivator} appropriate for executing this action in
	 * an infinitely-repeating fashion following a poisson distribution. Two Poisson
	 * distributions are used : one to get the start time and another one to get
	 * delta times between activations.
	 *
	 * @param meanStartT mean value of the Poisson distribution used for roll the
	 *                   start time value
	 * @param meanDeltaT mean value of the Poisson distribution used for roll the
	 *                   time amount between two activations
	 * @return an {@link ActionActivator} appropriate for executing this action in
	 *         an infinitely-repeating fashion following a poisson distribution
	 */
	public ActionActivator<A> repeateInfinitelyPoissonDistribution(final long meanStartT, final long meanDeltaT) {
		return repeateInfinitelyPoissonDistribution(BigDecimal.valueOf(meanStartT), BigDecimal.valueOf(meanDeltaT));
	}

}
