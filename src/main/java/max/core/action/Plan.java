/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.action;

import max.core.EnvironmentEvent;
import max.core.agent.SimulatedAgent;
import max.core.scheduling.ActionActivator;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Representation of an Agent's plan.
 * For now, a plan has two parts:
 * <ul>
 *     <li>a static one, implemented in {@link #getInitialPlan()}</li>
 *     <li>a dynamic part for event-bound actions, implemented in {@link #getEventBoundActions()} (optional)</li>
 * </ul>.
 *
 * The purpose of a plan is to describe what action will run, when and where (static). The plan also defines
 * what action to execute when a particular event occurs (event-bound)
 *
 * Plans will be reworked in the future.
 *
 * @author Pierre Dubaillay
 */
public interface Plan<A extends SimulatedAgent> {

    /**
     * Get the initial plan, ie a list of {@link ActionActivator} that will be scheduled at agent activation.
     *
     * @return the {@link ActionActivator} list.
     */
    List<ActionActivator<A>> getInitialPlan();

    /**
     * A mapping Event type to List of actions.
     * It will be used to run an action when an event is received. Selected action depends on event type.
     *
     * @return mapping event type to list of actions
     */
    default Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<A>>> getEventBoundActions() {
        return Collections.emptyMap();
    }

    /**
     * Convenient method to get the action associated to the provided event type.
     *
     * @param eventType event type
     * @return a list (potentially empty) of actions associated to this event type.
     */
    default List<ReactiveAction<A>> getActionsForEvent(final Class<? extends EnvironmentEvent> eventType) {
        return getEventBoundActions().getOrDefault(eventType, Collections.emptyList());
    }

    /**
     * Copy this plan.
     * 
     * All encapsulated action will have its owner set to the provided one.
     * @param owner new actions owner
     * @return the copy
     */
    default Plan<A> copyFor(final SimulatedAgent owner)  {
        final Plan<A> original = this;
        return new Plan<A>() {
            private final List<ActionActivator<A>> initialPLan = buildInitialPlan();
            private final Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<A>>> eventBased = buildEventBased();

            private List<ActionActivator<A>> buildInitialPlan() {
                return  original.getInitialPlan().stream()
                            .map(aa -> {
                                final var cpy = aa.copy();
                                cpy.getAction().setOwner((A) owner);
                                return cpy;
                            })
                            .collect(Collectors.toList());
            }

            private Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<A>>> buildEventBased() {
                final Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<A>>> result = new HashMap<>();
                original.getEventBoundActions().forEach((type, list) -> {
                    list.forEach(action -> {
                        final ReactiveAction<A> cpy = action.copy();
                        cpy.setOwner((A) owner);
                        final var copyList = result.computeIfAbsent(type, k -> new ArrayList<>());
                        copyList.add(cpy);
                    });
                });
                return result;
            }

            @Override
            public List<ActionActivator<A>> getInitialPlan() {
                return initialPLan;
            }

            @Override
            public Map<Class<? extends EnvironmentEvent>, List<ReactiveAction<A>>> getEventBoundActions() {
                return eventBased;
            }
        };
    }
}
