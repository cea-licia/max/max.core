/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.action;

import max.core.Community;
import max.core.agent.SimulatedAgent;
import max.core.role.Role;

import java.util.logging.Level;

/**
 * Leave a role in a particular environment.
 *
 * @param <A> Agent's type
 * @max.role {@link Role}
 * @author Pierre Dubaillay
 */
public class ACLeaveRole<A extends SimulatedAgent> extends Action<A> {
    /** Environment where the role is being played. */
    private final String env;

    /** Role to leave. */
    private final Class<? extends Role> role;

    /**
     * Default constructor.
     *
     * @param environment environment where the role is being played
     * @param role the role to leave
     * @param owner the action's owner
     */
    public ACLeaveRole(final String environment, final Class<? extends Role> role, final A owner) {
        super(Community.SIMULATION_ENVIRONMENT, Role.class, owner);
        this.env = environment;
        this.role = role;
    }

    @Override
    public void execute() {
        getOwner().leaveRole(Community.COMMUNITY, env, role.getName());
        getOwner().getLogger().log(Level.INFO, "Leaving role " + role.getName() + " in " + env);
    }

    @Override
    public  <T extends Action<A>> T copy() {
        return (T) new ACLeaveRole<>(env, role, getOwner());
    }
}