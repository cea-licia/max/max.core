/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import madkit.kernel.Activator;

/**
 * All kinds of execution strategies the scheduler can have.
 *
 * <p>For now there are three different strategies:
 *
 * <ul>
 *   <li>{@link #MONO_THREAD}: one thread executing sequentially each activator.
 *   <li>{@link #ONE_THREAD_PER_AGENT}: one thread per agent, each activator within the same agent
 *       executing sequentially.
 *   <li>{@link #ONE_THREAD_PER_ACTION}: one thread per action/activator, maximum concurrency.
 * </ul>
 *
 * Notice that only {@link #MONO_THREAD} strategy is officially supported, the two others are
 * experimental.
 *
 * @since 1.2.0
 * @author Pierre Dubaillay
 */
enum ExecutionStrategy {

  /** Default execution strategy. Each activator is run sequentially on the scheduler's thread. */
  MONO_THREAD {
    @Override
    Map<ActionActivator<?>, Throwable> execute(
        SchedulerAgent scheduler, Collection<ActionActivator<?>> activators) {
      final Map<ActionActivator<?>, Throwable> errors = new HashMap<>();
      for (final var activator : activators) {
        try {
          scheduler.executeActivator(activator);
        } catch (final Throwable error) {
          errors.put(activator, error);
        }
      }
      return errors;
    }
  },

  /**
   * Activators are grouped by owner, meaning that in the best case, {@code n} activators are
   * running concurrently, with {@code n} being the number of agents in the simulation.
   *
   * <p>Notice that two activators owned by the same agent will never run concurrently with this
   * strategy.
   *
   * <p>Experimental feature, use at your own risk.
   */
  ONE_THREAD_PER_AGENT {
    @Override
    Map<ActionActivator<?>, Throwable> execute(
        SchedulerAgent scheduler, Collection<ActionActivator<?>> activators) {
      final Map<ActionActivator<?>, Throwable> errors = new HashMap<>();
      activators.stream()
          .collect(Collectors.groupingBy(ActionActivator::getOwner))
          .values()
          .forEach(
              (actionActivators ->
                  EXECUTOR.submit(() -> MONO_THREAD.execute(scheduler, actionActivators))));

      // Take futures by completion order improving execution time
      for (var i = 0; i < activators.size(); ++i) {
        try {
          errors.putAll(EXECUTOR.take().get());
        } catch (ExecutionException e) {
          scheduler.getLogger().severeLog("Execution batch failed.", e);
        } catch (InterruptedException e) {
          scheduler.getLogger().severeLog("Scheduler thread interrupted.", e);
          Thread.currentThread().interrupt();
        }
      }
      return errors;
    }
  },

  /**
   * Each activator execution is submitted to the executor independently, meaning that in the best
   * case, all activators are running concurrently.
   *
   * <p>Notice that two activators owned by the same agent can run concurrently with this strategy.
   *
   * <p>Experimental feature, use at your own risk.
   */
  ONE_THREAD_PER_ACTION {
    @Override
    Map<ActionActivator<?>, Throwable> execute(
        SchedulerAgent scheduler, Collection<ActionActivator<?>> activators) {
      final Map<ActionActivator<?>, Throwable> errors = new HashMap<>();
      final Map<Future<?>, ActionActivator<?>> futureToActivator =
          activators.stream()
              .map(
                  activator -> // tuple (future, activator)
                      List.of(
                          EXECUTOR.submit(
                              () -> {
                                scheduler.executeActivator(activator);
                                return null;
                              }),
                          activator))
              .collect(  // map future to activator
                  Collectors.toMap(
                      tuple -> (Future<?>) tuple.get(0),
                      tuple -> (ActionActivator<?>) tuple.get(1)));

      // Take futures by completion order improving execution time
      for (var i = 0; i < activators.size(); ++i) {
        Future<?> task = null;
        try {
          task = EXECUTOR.take();
          task.get();
        } catch (final InterruptedException e) {
          scheduler.getLogger().severeLog("Scheduler thread interrupted.", e);
          Thread.currentThread().interrupt();
        } catch (final ExecutionException e) {
          errors.put(futureToActivator.get(task), e.getCause());
        }
      }
      return errors;
    }
  };

  /** Completion service to execute activators concurrently. */
  private static final CompletionService<Map<ActionActivator<?>, Throwable>> EXECUTOR =
      new ExecutorCompletionService<>(Activator.getMadkitServiceExecutor());

  /**
   * Executes the provided bunch of activators and returns a mapping activator to error for each
   * activator that failed.
   *
   * <p>How activators are executed (sequentially, with partial concurrency ...) is left to
   * implementation.
   *
   * @param scheduler {@link SchedulerAgent} which want to execute the activators
   * @param activators a collection of activators to execute.
   * @return a mapping failed activator to error
   */
  abstract Map<ActionActivator<?>, Throwable> execute(
      SchedulerAgent scheduler, Collection<ActionActivator<?>> activators);
}
