/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling.impl;

import cern.jet.random.Uniform;
import cern.jet.random.engine.MersenneTwister;
import cern.jet.random.engine.RandomEngine;
import max.core.scheduling.ActivationSchedule;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * An {@link ActivationSchedule} using two Uniform distributions to roll start and
 * delta values.
 *
 * @author Pierre Dubaillay
 */
public class UniformActivationSchedule implements ActivationSchedule {

	/** Random numbers generator. */
	private static final RandomEngine GENERATOR = new MersenneTwister((int) System.currentTimeMillis());

	/** How many repetitions. */
	private final long maxRepetitionCount;

	/** The Uniform distribution giving the start time. */
	private final Uniform startTimeU;

	/** The Uniform distribution giving the delta times. */
	private final Uniform deltaTimeU;

	/**
	 * Create a new {@link UniformActivationSchedule}.
	 *
	 * @param maxRepCount how many times the schedule should be repeated
	 * @param minStart minimum value used for the first distribution rolling start values
	 * @param maxStart maximum value used for the first distribution rolling start values
	 * @param minDelta minimum value used for the second distribution rolling delta values
	 * @param maxDelta maximum value used for the second distribution rolling delta values
	 */
	public UniformActivationSchedule(final long maxRepCount, final BigDecimal minStart, final BigDecimal maxStart,
			final BigDecimal minDelta, final BigDecimal maxDelta) {
		maxRepetitionCount = maxRepCount;
		startTimeU = new Uniform(minStart.doubleValue(), maxStart.doubleValue(), GENERATOR);
		deltaTimeU = new Uniform(minDelta.doubleValue(), maxDelta.doubleValue(), GENERATOR);
	}

	@Override
	public BigDecimal getStartTime() {
		return BigDecimal.valueOf(Math.round(startTimeU.nextDouble()));
	}

	@Override
	public BigDecimal getDeltaTime() {
		return BigDecimal.valueOf(Math.round(deltaTimeU.nextDouble()));
	}

	@Override
	public long getMaximumRepetitionCount() {
		return maxRepetitionCount;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final UniformActivationSchedule that = (UniformActivationSchedule) o;
		return maxRepetitionCount == that.maxRepetitionCount && startTimeU.equals(that.startTimeU) &&
		       deltaTimeU.equals(that.deltaTimeU);
	}

	@Override
	public int hashCode() {
		return Objects.hash(maxRepetitionCount, startTimeU, deltaTimeU);
	}
}
