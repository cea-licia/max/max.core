/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling.impl;

import cern.jet.random.Poisson;
import cern.jet.random.engine.MersenneTwister;
import cern.jet.random.engine.RandomEngine;
import max.core.scheduling.ActivationSchedule;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * An {@link ActivationSchedule} using two Poisson distributions to roll start and
 * delta values.
 *
 * @author Pierre Dubaillay
 */
public class PoissonActivationSchedule implements ActivationSchedule {

	/** Random numbers generator. */
	private static final RandomEngine GENERATOR = new MersenneTwister((int) System.currentTimeMillis());

	/** How many repetitions. */
	private final long maxRepetitionCount;

	/** The Poisson distribution used to get the start time. */
	private final Poisson startTimeP;

	/** The Poisson distribution used to get delta times. */
	private final Poisson deltaTimeP;

	/**
	 * Create a new {@link PoissonActivationSchedule} object.
	 *
	 * @param maxRepCount how many times the schedule should be repeated
	 * @param startMean mean value for the first Poisson distribution (used for start values)
	 * @param deltaMean mean value for the second Poisson distribution (used for delta values)
	 */
	public PoissonActivationSchedule(final long maxRepCount, final BigDecimal startMean, final BigDecimal deltaMean) {
		maxRepetitionCount = maxRepCount;
		startTimeP = new Poisson(startMean.doubleValue(), GENERATOR);
		deltaTimeP = new Poisson(deltaMean.doubleValue(), GENERATOR);
	}

	@Override
	public BigDecimal getStartTime() {
		return BigDecimal.valueOf(Math.round(startTimeP.nextDouble()));
	}

	@Override
	public BigDecimal getDeltaTime() {
		return BigDecimal.valueOf(Math.round(deltaTimeP.nextDouble()));
	}

	@Override
	public long getMaximumRepetitionCount() {
		return maxRepetitionCount;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final PoissonActivationSchedule that = (PoissonActivationSchedule) o;
		return maxRepetitionCount == that.maxRepetitionCount && startTimeP.equals(that.startTimeP) &&
		       deltaTimeP.equals(that.deltaTimeP);
	}

	@Override
	public int hashCode() {
		return Objects.hash(maxRepetitionCount, startTimeP, deltaTimeP);
	}
}
