/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling.impl;

import max.core.scheduling.ActivationSchedule;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * A simple {@link ActivationSchedule} that uses fixed values for start and delta
 * times.
 *
 * @author Pierre Dubaillay
 */
public class SimpleActivationSchedule implements ActivationSchedule {

	/** The start time value. */
	private final BigDecimal startTime;

	/** The fixed delta time between two activations. */
	private final BigDecimal deltaTime;

	/** How many repetitions. */
	private final long maxRepetitionCount;

	/**
	 * Create a new {@link SimpleActivationSchedule} object.
	 *
	 * @param maxRepetitionCount how many times the schedule should be repeated
	 * @param startTime start time value
	 * @param deltaTime delta time between two successive activations
	 */
	public SimpleActivationSchedule(final long maxRepetitionCount, final BigDecimal startTime,
                final BigDecimal deltaTime) {
		this.startTime = startTime;
		this.deltaTime = deltaTime;
		this.maxRepetitionCount = maxRepetitionCount;
	}

	@Override
	public BigDecimal getStartTime() {
		return startTime;
	}

	@Override
	public BigDecimal getDeltaTime() {
		return deltaTime;
	}

	@Override
	public long getMaximumRepetitionCount() {
		return maxRepetitionCount;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final SimpleActivationSchedule that = (SimpleActivationSchedule) o;
		return maxRepetitionCount == that.maxRepetitionCount && startTime.equals(that.startTime) &&
		       deltaTime.equals(that.deltaTime);
	}

	@Override
	public int hashCode() {
		return Objects.hash(startTime, deltaTime, maxRepetitionCount);
	}
}
