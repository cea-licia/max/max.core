/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling.impl;

import cern.jet.random.Normal;
import cern.jet.random.engine.MersenneTwister;
import cern.jet.random.engine.RandomEngine;
import max.core.scheduling.ActivationSchedule;

import java.math.BigDecimal;
import java.util.Objects;

/**
 * An {@link ActivationSchedule} using two Normal distributions to roll start and
 * delta values.
 *
 * @author Pierre Dubaillay
 */
public class NormalActivationSchedule  implements ActivationSchedule {

	/** Random numbers generator. */
	private static final RandomEngine GENERATOR = new MersenneTwister((int) System.currentTimeMillis());

	/** The normal distribution giving the start time. */
	private final Normal startTimeN;

	/** The normal distribution giving the delta time. */
	private final Normal deltaTimeN;

	/** How many repetitions. */
	private final long maxRepetitionCount;

	/**
	 * Creates a new {@link NormalActivationSchedule} object.
	 *
	 * @param maxRepCount how many times the schedule should be repeated
	 * @param meanStartT mean value used for the first distribution rolling start values
	 * @param sdStartT standard deviation value used for the first distribution rolling start values
	 * @param meanDeltaT mean value used for the second distribution rolling delta values
	 * @param sdDeltaT standard deviation value used for the second distribution rolling delta values
	 */
	public NormalActivationSchedule(final long maxRepCount, final BigDecimal meanStartT,
            final BigDecimal sdStartT, final BigDecimal meanDeltaT, final BigDecimal sdDeltaT) {

		startTimeN = new Normal(meanStartT.doubleValue(), sdStartT.doubleValue(), GENERATOR);
		deltaTimeN = new Normal(meanDeltaT.doubleValue(), sdDeltaT.doubleValue(), GENERATOR);

		maxRepetitionCount = maxRepCount;
	}

	@Override
	public BigDecimal getStartTime() {
		return BigDecimal.valueOf(Math.round(startTimeN.nextDouble()));
	}

	@Override
	public BigDecimal getDeltaTime() {
		return BigDecimal.valueOf(Math.round(deltaTimeN.nextDouble()));
	}

	@Override
	public long getMaximumRepetitionCount() {
		return maxRepetitionCount;
	}

	@Override
	public boolean equals(final Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		final NormalActivationSchedule that = (NormalActivationSchedule) o;
		return startTimeN.equals(that.startTimeN) && deltaTimeN.equals(that.deltaTimeN) &&
		       (maxRepetitionCount == that.maxRepetitionCount);
	}

	@Override
	public int hashCode() {
		return Objects.hash(startTimeN, deltaTimeN, maxRepetitionCount);
	}
}
