/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.stream.Collectors;

import madkit.kernel.AbstractAgent;
import madkit.kernel.Activator;
import max.core.Community;
import max.core.MAXParameters;
import max.core.action.Action;
import max.core.agent.MAXAgent;
import max.core.role.RScheduler;
import max.core.role.Role;

/**
 * Scheduler is used for scheduling actions of agents.
 *
 * @author Önder Gürcan
 * @author Pierre Dubaillay
 * @author François Le Fevre
 * @author Aimen Djari
 * @since 1.0
 */
public class SchedulerAgent extends madkit.kernel.Scheduler implements RScheduler {

    /**
     * Key to access the ALWAYS_VERIFY_ROLE_PLAYING flag value.
     */
    private static final String ALWAYS_VERIFY_ROLE_PLAYING_KEY = "MAX_CORE_ALWAYS_VERIFY_ROLE_PLAYING";
    /**
     * Key to retrieve the action execution policy.
     */
    private static final String ACTION_EXECUTION_STRATEGY_KEY = "MAX_CORE_ACTION_EXECUTION_STRATEGY";

    /**
     * The queue of activators sorted according to their next activation times in an
     * ascending manner.
     */
    private final PriorityQueue<ActionActivator<? extends MAXAgent>> activatorQueue;

    /**
     * List containing actions executed last tick.
     * Queried using reflexion.
     */
    private final List<Action<? extends MAXAgent>> lastExecutedActions;

    /**
     * The simulation step of this scheduler.
     */
    private BigDecimal simulationStep;

    /**
     * How to manage errors in activators.
     */
    private ErrorHandler errorHandler;

    /**
     * Flag that tells if the scheduler is about to die. If true, no more operations are running.
     */
    private final AtomicBoolean schedulerIsDead;

    /**
     * Flag that (de)activates the role checking feature.
     */
    private final Boolean checkRoles;

    /**
     * Which execution this scheduler uses. Default is MONO-THREAD. Value set in activate method.
     */
    private ExecutionStrategy executionStrategy;

    /**
     * Creates a new {@link SchedulerAgent} instance.
     * <p>
     * It also associates this instance to a default {@link ErrorHandler} that you can change later if
     * you want. This default handler will log ({@link Level#WARNING}) exceptions and finish {@link ActionActivator}s.
     */
    public SchedulerAgent() {
        // initialize the activator queue
        this.activatorQueue = new PriorityQueue<>(100, (activator1, activator2) -> {
            BigDecimal nextExecutionTime1 = activator1.getNextExecutionTime();
            BigDecimal nextExecutionTime2 = activator2.getNextExecutionTime();
            int result = nextExecutionTime1.compareTo(nextExecutionTime2);

            if (result == 0) {
                if (activator1.getId() > activator2.getId()) {
                    result = 1;
                } else {
                    result = -1;
                }
            }
            return result;
        });

        // Default error handler is to log the exception and finish the activator
        errorHandler =
                (scheduler, exceptions) ->
                        exceptions.forEach(
                                (activator, exception) -> {
                                    scheduler.getLogger().log(Level.WARNING, "Activator=" + activator, exception);
                                    activator.finish();
                                });

        // initialize last executed actions list
        lastExecutedActions = new LinkedList<>();
        schedulerIsDead = new AtomicBoolean();
        checkRoles = MAXParameters.getBooleanParameter(ALWAYS_VERIFY_ROLE_PLAYING_KEY, Boolean.TRUE);
    }

    @Override
    public final void activate() {
        // Compute and set execution strategy
        setExecutionStrategy();

        // request the Scheduler role
        requestRole(Community.SIMULATION_ENVIRONMENT, RScheduler.class);

        // set the simulation state as PAUSED
        setSimulationState(SimulationState.PAUSED);
    }

    /**
     * Request a role in the given environment.
     *
     * @param environmentName environment
     * @param role            role to take
     */
    public final void requestRole(String environmentName, Class<? extends Role> role) {
        getLogger().log(Level.FINER, " [ N/A ] : " + "requestRole() is called!");

        ReturnCode requestRole = requestRole(Community.COMMUNITY, environmentName, role.getName());
        getLogger().log(Level.FINE, "role binding [" + environmentName + ", " + role.getName() + "] : " + requestRole);
        if (requestRole.equals(ReturnCode.SUCCESS)) {
            // add a dummy activator to be able to get simulation time (because of MaDKit).
            getLogger().log(Level.FINER, "Dummy activator is added for community = " + Community.COMMUNITY
                    + ", environment = " + environmentName + ", role = " + role);
        }

        getLogger().log(Level.FINER, " [ N/A ] : " + "requestRole() is finished!");
    }

    /**
     * Activates the activators for the current simulation time and advances the
     * simulation time.
     */
    @Override
    public final void doSimulationStep() {
        BigDecimal currentTime = getSimulationTime().getCurrentTick();

        // Clear last executed actions
        lastExecutedActions.clear();

        if (!this.activatorQueue.isEmpty()) {

            // Retrieve all activators to run
            final var activators = new LinkedList<ActionActivator<?>>();
            ActionActivator<?> activator = activatorQueue.peek();
            while (activator != null && activator.getNextExecutionTime().compareTo(currentTime) == 0) {
                activators.add(activatorQueue.poll());
                activator = activatorQueue.peek();
            }

            // Fill lastExecutedActions
            lastExecutedActions.addAll(activators.stream()
                    .map(ActionActivator::getAction)
                    .collect(Collectors.toList()));

            // Execute activators and collect errors
            final Map<ActionActivator<?>, Throwable> errors = executionStrategy.execute(this, activators);

            // If errors occurred, execute error handler
            if (!errors.isEmpty()) {
                errorHandler.handleExceptions(this, errors);
            }

            // Reschedule activators if necessary
            activators.forEach(act -> {
                if (!act.isFinished()) {
                    schedule(act);
                }
            });

            // advance the simulation time
            getSimulationTime().incrementCurrentTick(simulationStep);
        } else if (currentTime.compareTo(BigDecimal.ZERO) > 0) {
            this.killAgent(this);
        }
        // set the speed of the simulation in milliseconds
        pause(getDelay());
    }

    @Override
    public synchronized void addActivator(Activator<? extends AbstractAgent> activator) {
        super.addActivator(activator);
    }

    /**
     * Schedules a given activator for its execution, unless it is finished or its
     * next time is in the past.
     *
     * @param activator activator to schedule
     */
    public final void schedule(ActionActivator<? extends MAXAgent> activator) {
        getLogger().log(Level.FINE, "[" + getSimulationTime().getCurrentTick() + "] : schedule() is called.");

        BigDecimal actualTime = getSimulationTime().getCurrentTick();
        BigDecimal divide = activator.getNextExecutionTime().divide(simulationStep);
        BigDecimal roundedNextExecutionTime = divide.setScale(0, RoundingMode.DOWN);
        int compareToZero = divide.subtract(roundedNextExecutionTime).compareTo(BigDecimal.ZERO);
        // Execute if next execution time is a divider of simulation step
        if (compareToZero == 0) {
            int compareTo = activator.getNextExecutionTime().compareTo(actualTime);
            if (compareTo == 0) { // if the action is to be activated now

                try {
                    executeActivator(activator);
                } catch (final Throwable e) {
                    errorHandler.handleExceptions(this, Map.of(activator, e));
                }

                // Reschedule activator if necessary
                if (!activator.isFinished()) {
                    schedule(activator);
                }

            } else if (compareTo > 0) { // if the action is to be activated later
                // add activator to the activator queue
                this.addToTheActivatorQueue(activator);

                // add activator to the kernel engine. This has to be done to make an activator
                // work properly
                this.addActivator(activator);

                getLogger().log(Level.FINE, "[" + getSimulationTime().getCurrentTick() + "] : " + activator
                        + " scheduled for being activated at t = " + activator.getNextExecutionTime());
            }
        } else {
            throw new IllegalArgumentException("Invalid interval for the action " + activator.getAction()
                    + ". The action is not schedulable because nextExecutionTime (" + activator.getNextExecutionTime()
                    + ") is not a divider of simulationStep (" + simulationStep + ").");
        }

        getLogger().log(Level.FINE, "[" + getSimulationTime().getCurrentTick() + "] : schedule() is finished.");
    }

    /**
     * Add the given activator to the internal activator queue.
     *
     * @param activator activator to add
     */
    private void addToTheActivatorQueue(ActionActivator<? extends MAXAgent> activator) {
        this.activatorQueue.add(activator);
    }

    /**
     * Executes the given activator.
     *
     * <p>Depending on MAX_CORE_ALWAYS_VERIFY_ROLE_PLAYING value, this method checks or not if the
     * agent is playing the role associated to the action.
     *
     * <p>If the check is enabled and the agent is not playing the requested role, then it emits a
     * warning message and terminates the activator.
     *
     * @param activator Activator to execute
     * @throws RuntimeException any runtime exception thrown by the activator
     */
    void executeActivator(ActionActivator<? extends MAXAgent> activator) {
        if (Boolean.FALSE.equals(checkRoles)
                || activator.isAgentPlayingRole(
                getAgentsWithRole(
                        activator.getCommunity(), activator.getGroup(), activator.getRole()))) {
            executeAndLog(activator);
            getLogger()
                    .log(
                            Level.FINE,
                            "["
                                    + getSimulationTime().getCurrentTick()
                                    + "] : "
                                    + activator
                                    + " has been activated");
        } else {
            getLogger()
                    .log(
                            Level.WARNING,
                            "["
                                    + getSimulationTime().getCurrentTick()
                                    + "] : "
                                    + activator.getOwner().getName()
                                    + " is not playing the "
                                    + activator.getRole()
                                    + " role and thus cannot activate the action "
                                    + activator.getAction().getClass().getName());
            activator.finish();
        }
        if (activator.isFinished()) {
            this.removeActivator(activator);
        }
    }

    @Override
    public synchronized void removeActivator(Activator<? extends AbstractAgent> activator) {
        super.removeActivator(activator);
    }

    /**
     * Pause the scheduler and thus the simulation.
     */
    public final void pauseSimulation() {
        setSimulationState(SimulationState.PAUSED);
    }

    /**
     * Resume the scheduler and thus the simulation.
     */
    public final void resumeSimulation() {
        setSimulationState(SimulationState.RUNNING);
    }

    /**
     * Stop the scheduler and thus the simulation.
     */
    public final void stopSimulation() {
        setSimulationState(SimulationState.SHUTDOWN);
    }

    /**
     * Change the simulation step.
     *
     * @param simulationStep the new simulation step
     */
    public final void setSimulationStep(BigDecimal simulationStep) {
        this.simulationStep = simulationStep;
    }

    /**
     * Last behavior called by Madkit. It pauses the scheduler and then notify all threads waiting on
     * this agent's end.
     */
    @Override
    protected void end() {
        super.end();

        // Notify all threads waiting on this agent's end
        synchronized (schedulerIsDead) {
            schedulerIsDead.set(true);
            schedulerIsDead.notifyAll();
        }
    }

    /**
     * Sets the error handler used by the scheduler to handle errors in activators.
     *
     * @param handler the new handler, which can't be {@code null}
     * @throws IllegalArgumentException {@literal handler} is {@code null}
     */
    public void setErrorHandler(ErrorHandler handler) {
        if (handler == null) throw new IllegalArgumentException("Handler can't be null");
        errorHandler = handler;
    }

    /**
     * Synchronization object used to wait scheduler termination. Other threads should wait on it and
     * check if value (<code>true</code> if scheduler is dead).
     *
     * @return the synchronization object
     */
    public AtomicBoolean isSchedulerDead() {
        return schedulerIsDead;
    }

    /**
     * Using MAX_CORE_ACTION_EXECUTION_STRATEGY parameter, computes the execution strategy and set it
     * for later use.
     */
    private void setExecutionStrategy() {
        var strategy = ExecutionStrategy.MONO_THREAD;
        try {
            strategy =
                    ExecutionStrategy.valueOf(MAXParameters.getParameter(ACTION_EXECUTION_STRATEGY_KEY));
        } catch (final IllegalArgumentException | NullPointerException ignored) {
            // Fallback to default value
        }
        this.executionStrategy = strategy;
    }
}