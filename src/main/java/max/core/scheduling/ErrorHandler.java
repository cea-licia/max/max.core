/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling;

import java.util.Map;

/**
 * Small interface describing objects designed to handle action execution failures.
 *
 * For each tick, each time a batch of actions is executed, the {@link SchedulerAgent} collect exceptions. Those
 * exceptions are mapped with their {@link ActionActivator}.
 *
 * It is up to this interface realizations to handle the exceptions. One can decide to log them, kill the
 * simulation or even reschedule.
 *
 * @author Pierre Dubaillay
 */
@FunctionalInterface
public interface ErrorHandler {

  /**
   * Handles exceptions thrown by actions.
   *
   * @param scheduler reference to the {@link SchedulerAgent}
   * @param exceptions a mapping activator to exception. Only contains activators that thrown an exception.
   */
  void handleExceptions(SchedulerAgent scheduler, Map<ActionActivator<?>, Throwable> exceptions);
}