/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling;

import max.core.scheduling.impl.NormalActivationSchedule;
import max.core.scheduling.impl.PoissonActivationSchedule;
import max.core.scheduling.impl.SimpleActivationSchedule;
import max.core.scheduling.impl.UniformActivationSchedule;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * A factory to create {@link ActivationSchedule} objects.
 *
 * <p>{@link ActivationSchedule} objects created using this factory class are cached for better
 * performances. It means that if you try to create twice the same {@link ActivationSchedule}, the
 * second time you will get a reference to the first created instance.
 *
 * @author Pierre Dubaillay
 * @author Önder Gürcan
 */
public final class ActivationScheduleFactory {

	/** Pool of already-created {@link ActivationSchedule} objects. */
	private static final Map<ActivationSchedule, ActivationSchedule> pool = new HashMap<>();

	/** One shot at simulation start. */
	public static final ActivationSchedule ONE_SHOT_AT_STARTUP = createOneTime(BigDecimal.ZERO);
	
	/** One shot at simulation start. */
	public static final ActivationSchedule ONE_SHOT_AT_ONE = createOneTime(BigDecimal.ONE);

	/** One-One: starts at tick 1 and repeat indefinitely each tick. */
	public static final ActivationSchedule ONE_ONE_INDEF = createRepeatingInfinitely(BigDecimal.ONE, BigDecimal.ONE);

	/**
	 * Creates an {@link ActivationSchedule} appropriate for scheduling a non-repeating action. The
	 * action will start at the specified time.
	 *
	 * @param start the start time
	 * @return an {@link ActivationSchedule} appropriate for scheduling a non-repeating action
	 */
	public static ActivationSchedule createOneTime(final BigDecimal start) {
		return addOrGetFromPool(new SimpleActivationSchedule(1, start, BigDecimal.ZERO));
	}

	/**
	 * Creates a Schedule appropriate for scheduling an infinitely-repeating
	 * action. The behavior will start at the specified time.
	 *
	 * @param start the start time
	 * @param interval how much time between each activation
	 * @return an {@link ActivationSchedule} appropriate for scheduling an infinitely-repeating action
	 */
	public static ActivationSchedule createRepeatingInfinitely(final BigDecimal start, final BigDecimal interval) {
		return addOrGetFromPool(new SimpleActivationSchedule(Long.MAX_VALUE, start, interval));
	}

	/**
	 * Creates a Schedule appropriate for scheduling a finitely-repeating action.
	 * The action will start at the specified time.
	 *
	 * @param start the start time
	 * @param stop the stop time
	 * @param interval how much time between each activation
	 * @return an {@link ActivationSchedule} appropriate for scheduling a finitely-repeating action
	 */
	public static ActivationSchedule createRepeatingFinitely(final BigDecimal start, final BigDecimal stop,
			final BigDecimal interval) {
		long repetition = stop.subtract(start).divide(interval, RoundingMode.DOWN).longValue() + 1;
		return addOrGetFromPool(new SimpleActivationSchedule(repetition, start, interval));
	}

	/**
	 * Creates a Schedule appropriate for scheduling an infinitely-repeating
	 * action following a normal distribution.
	 * Two normals are used : one to get the start time and another one to get delta times between activations.
	 *
	 * @param meanStartT mean value for the normal used to get the start time
	 * @param sdStartT standard deviation for the normal used to get the start time
	 * @param meanDeltaT mean value for the normal used to get time amount between activations
	 * @param sdDeltaT standard deviation for the normal used to get time amount between activations
	 * @return an {@link ActivationSchedule} appropriate for scheduling an infinitely-repeating action
	 */
	public static ActivationSchedule createRepeatingInfinitelyNormalDistribution(final BigDecimal meanStartT,
            final BigDecimal sdStartT, final BigDecimal meanDeltaT, final BigDecimal sdDeltaT) {
		return addOrGetFromPool(new NormalActivationSchedule(
				Long.MAX_VALUE, meanStartT, sdStartT, meanDeltaT,sdDeltaT));
	}

	/**
	 * Creates a Schedule appropriate for scheduling an infinitely-repeating
	 * action following a uniform distribution.
	 * Two uniforms are used : one to get the start time and another one to get delta times between activations.
	 *
	 * @param minStartT minimum value of the uniform distribution used for roll the start time value
	 * @param maxStartT maximum value of the uniform distribution used for roll the start time value
	 * @param minDeltaT minimum value of the uniform distribution used for roll the time amount between two activations
	 * @param maxDeltaT minimum value of the uniform distribution used for roll the time amount between two activations
	 * @return an {@link ActivationSchedule} appropriate for scheduling an infinitely-repeating action
	 */
	public static ActivationSchedule createRepeatingInfinitelyUniformDistribution(final BigDecimal minStartT,
			final BigDecimal maxStartT, final BigDecimal minDeltaT, final BigDecimal maxDeltaT) {
		return addOrGetFromPool(new UniformActivationSchedule(
				Long.MAX_VALUE, minStartT, maxStartT, minDeltaT, maxDeltaT));
	}

	/**
	 * Creates a Schedule appropriate for scheduling an infinitely-repeating
	 * action following a poisson distribution.
	 * Two Poisson distributions are used : one to get the start time and another one to get delta times between
	 * activations.
	 *
	 * @param meanStartT mean value of the Poisson distribution used for roll the start time value
	 * @param meanDeltaT mean value of the Poisson distribution used for roll the time amount between two activations
	 * @return an {@link ActivationSchedule} appropriate for scheduling an infinitely-repeating action
	 */
	public static ActivationSchedule createRepeatingInfinitelyPoissonDistribution(final BigDecimal meanStartT,
			final BigDecimal meanDeltaT) {
		return addOrGetFromPool(new PoissonActivationSchedule(Long.MAX_VALUE, meanStartT, meanDeltaT));
	}

	/**
	 * Add the schedule if not present and retrieve from pool.
	 * Input and output are the same if the input object was not ine the pool, otherwise output object will be the
	 * one already stored.
	 *
	 * @param schedule {@link ActivationSchedule} object to store
	 * @return object to use
	 */
	private static ActivationSchedule addOrGetFromPool(final ActivationSchedule schedule) {
		return pool.computeIfAbsent(schedule, k -> schedule);
	}

	/**
	 * Hide default constructor by a private empty one.
	 */
	private ActivationScheduleFactory() {
		// Empty constructor
	}

}
