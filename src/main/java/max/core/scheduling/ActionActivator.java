/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling;

import madkit.kernel.AbstractAgent;
import madkit.kernel.Activator;
import madkit.kernel.AgentAddress;
import madkit.kernel.Scheduler;
import max.core.action.Action;
import max.core.agent.MAXAgent;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * An activator that invokes a single method with no parameters of specific
 * agent, i.e. owner. An ActionActivator is ideally scheduled by an agent
 * (owner) itself.
 * 
 * @author Önder Gürcan
 * @author Pierre Dubaillay
 * @author François Le Fevre
 * @since 1.0
 * @version 1.0
 * 
 */
public class ActionActivator<A extends MAXAgent> extends Activator<A> {

	// STATICS
	/**
	 * ID of the latest ActionActivator.
	 */
	private static final AtomicInteger lastID = new AtomicInteger(0);

	// ATTRIBUTES
	/**
	 * The variable that specifies the next time this Activator to be executed.
	 */
	private BigDecimal nextExecutionTime;

	/**
	 * The variable that describes when an action should be scheduled.
	 */
	private final ActivationSchedule schedule;

	/**
	 * The variable that controls the iterations of this activator.
	 */
	private long executionCount;

	/**
	 * Indicates whether this activator will no more be executed or not.
	 */
	private boolean isFinished;

	/**
	 * Action to be activated.
	 */
	private final Action<A> action;

	/**
	 * ID of this ActionActivator.
	 */
	private final int id;

	/**
	 * Builds a new ActionActivator on the given CGR location of the artificial
	 * society. Once created, it has to be added by a {@link Scheduler} agent using
	 * the {@link Scheduler#addActivator(Activator)} method. Once added, it could be
	 * used to trigger the behavior on all the agents which are at this CGR
	 * location, regardless of their class type as long as they extend
	 * {@link AbstractAgent}
	 *
	 * @param schedule the schedule description
	 * @param action the action to execute
	 *
	 */
	public ActionActivator(final ActivationSchedule schedule, final Action<A> action) {
		super(action.getCommunity(), action.getEnvironment(), action.getRole().getName());

		// initialize parameters
		this.action = action;
		this.schedule = schedule;
		this.executionCount = 0;
		this.isFinished = false;
		this.id = lastID.getAndIncrement();

		// calculate the starting time
		this.nextExecutionTime = schedule.getStartTime();
	}

	/**
	 * Copy the current object.
	 * Notice that:
	 * <ul>
	 *     <li>{@link ActivationSchedule} will point to the same object as they
	 *     are immutable</li>
	 *     <li>{@link Action} will be a new copy</li>
	 *     <li>IDs are different</li>
	 *     <li>execution count is reset to 0</li>
	 *     <li>next execution time is computed again and most likely different</li>
	 * </ul>
	 *
	 * @return a copy of the current activator
	 */
	public ActionActivator<A> copy() {
		return new ActionActivator<>(schedule, action.copy());
	}

	/**
	 * Execute the wrapped action and reschedule it if necessary.
	 *
	 * @param agents ignored
	 * @param args ignored
	 */
	@Override
	public final void execute(List<A> agents, Object... args) {
		action.execute();

		// increase execution count
		this.executionCount++;

		// if the action is activated as many times as scheduled
		if (this.executionCount == this.schedule.getMaximumRepetitionCount()) {
			// finish the execution if this activator.
			this.isFinished = true;
		} else {
			// else calculate the next execution time
			addIntervalToNextExecutionTime(schedule.getDeltaTime());
		}
	}

	@Override
	public final List<A> getCurrentAgentsList() {
		return Collections.singletonList(getOwner());
	}

	/**
	 * Returns the next time this Activator to be executed.
	 * 
	 * @return the next time this Activator to be executed.
	 */
	public final BigDecimal getNextExecutionTime() {
		return nextExecutionTime;
	}

	/**
	 * Adds interval to the next execution time.
	 * 
	 * @param interval how much time to add
	 */
	private void addIntervalToNextExecutionTime(BigDecimal interval) {
		this.nextExecutionTime = this.nextExecutionTime.add(interval);
	}

	/**
	 * Returns true if this activator will not executed anymore, false otherwise.
	 * 
	 * @return true if this activator will not executed anymore, false otherwise.
	 */
	public final boolean isFinished() {
		return isFinished;
	}

	/**
	 * Terminate this Activator as soon as possible.
	 */
	public final void finish() {
		isFinished = true;
	}

	/**
	 * Get the encapsulated action's owner.
	 * @return the action's owner.
	 */
	public final A getOwner() {
		return this.action.getOwner();
	}

	/**
	 * Check if the action is played by the correct agent and role. If
	 * agent's address is one of the provided, then this method will return true.
	 * @param agentsWithRole list of valid agent addresses (one per role)
	 * @return true if the agent is playing one of the provided roles.
	 */
	public final boolean isAgentPlayingRole(List<AgentAddress> agentsWithRole) {
		boolean result = false;
		if (getOwner().isAlive()) {
			AgentAddress agentAddress = getOwner().getAgentAddressIn(getCommunity(), getGroup(), getRole());
			if (agentAddress != null) {
				result = agentsWithRole.contains(agentAddress);
			}
		} else {
			finish();
		}
		return result;
	}

	@Override
	public String toString() {
		return "<" +
				this.getCommunity() +
				"." +
				this.getGroup() +
				"." +
				this.getRole() +
				">." +
				" scheduled " +
				this.schedule;
	}

	/**
	 * Get the unique ID of this activator instance.
	 * @return unique ID
	 */
	public final int getId() {
		return id;
	}

	/**
	 * Get the wrapped action.
	 * @return the action
	 */
	public final Action<A> getAction() {
		return this.action;
	}

}
