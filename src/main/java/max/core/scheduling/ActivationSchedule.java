/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.scheduling;

import max.core.action.Action;

import java.math.BigDecimal;

/**
 * Common interface for all activation schedule.
 *
 * {@link ActivationSchedule} objects are immutable, meaning they can be reused or even shared multiple times.
 * They describe a scheduling plan for "something". Plan includes:
 * <ul>
 *     <li>first activation</li>
 *     <li>how many times it should be activated (repetitions)</li>
 *     <li>how much time between two successive executions</li>
 * </ul>
 *
 * @author Pierre Dubaillay
 */
public interface ActivationSchedule {
    /**
     * Get when the associated object (most-likely an {@link Action}) should be scheduled for the first time
     * @return first time
     */
    BigDecimal getStartTime();

    /**
     * Get the delta time between two successive executions.
     * @return delta time
     */
    BigDecimal getDeltaTime();

    /**
     * How many times the associated object should be scheduled.
     * It is up to the scheduling system to keep track of how many times the object has already been scheduled.
     * @return maximum repetition count
     */
    long getMaximumRepetitionCount();
}