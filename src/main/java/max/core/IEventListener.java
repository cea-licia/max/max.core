/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core;

/**
 * Observer pattern for events.
 *
 * Any type implementing this interface can be notified by events generators once registered.
 *
 * @author Önder Gürcan
 * @author François Le Fevre
 * @since 1.0
 */
public interface IEventListener {

	/**
	 * Executed when the listener is notified by an event generator..
	 * 
	 * @param event the emitted event
	 */
	void onEvent(EnvironmentEvent event);

}
