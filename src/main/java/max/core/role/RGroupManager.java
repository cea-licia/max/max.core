/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.role;

/**
 * Role played by any {@link max.core.agent.SimulatedEnvironment} in their own group (what we call environment in MAX).
 *
 * This role tells the agent is acting as a group manager, i.e. it manages incoming and outgoing agents for its own group.
 *
 * @since 1.2
 * @author Pierre Dubaillay
 */
public interface RGroupManager extends Role {

}
