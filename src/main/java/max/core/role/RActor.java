/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.role;

/**
 * Base role for all user roles.
 *
 * This role is taken by {@link max.core.agent.SimulatedAgent} in the core environment. It represents agents
 * capable of executing complex behaviors, i.e. acting in the simulation.
 *
 * @since 1.2
 * @author Pierre Dubaillay
 */
public interface RActor extends Role {
}
