/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core.role;

/**
 * Role taken by the {@link max.core.scheduling.SchedulerAgent} in the MAX core environment.
 *
 * @since 1.2
 * @author Pierre Dubaillay
 */
public interface RScheduler extends Role {
}
