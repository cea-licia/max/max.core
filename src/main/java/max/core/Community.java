/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core;

/**
 * A community is a body of agents of common interests scattered through a
 * larger society. A community contains a set of environments but an environment may only be
 * part of a single community.
 * 
 * @author Önder Gürcan
 * @since 1.0
 *
 */
public class Community {
	
	public static final String COMMUNITY = "MAX";
	
	public static final String SIMULATION_ENVIRONMENT = "SimulationEnvironment";
	
}
