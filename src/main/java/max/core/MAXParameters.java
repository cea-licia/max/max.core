/*********************************************************************
 * Copyright (c) 2022 CEA (Commissariat à l'énergie atomique et aux énergies alternatives)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 **********************************************************************/
package max.core;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.Properties;

/**
 * Global parameters for MAX.
 *
 * <p>This class stores globally all parameters defined for the current simulator instance. It also
 * provides methods to read and write those stored values.
 *
 * @author Önder Gürcan
 * @author Pierre Dubaillay
 * @since 1.0
 */
public class MAXParameters {

	/** Parameters are stored as standard Java properties. */
	private static final Properties properties = new Properties();

	/**
	 * Sets the value of the given parameter.
	 *
	 * If the parameter doesn't exist, it is created.
	 *
	 * @param key parameter's name
	 * @param value the new value
	 */
	public static void setParameter(String key, String value) {
		properties.setProperty(key, value);
		System.out.println(key + " has been set to " + value);
	}

	/**
	 * Retrieves the stored value of the given parameter as a {@link String} instance.
	 *
	 * @param key parameter's name
	 * @return either the value or {@code null} if the parameter doesn't exist
	 */
	public static String getParameter(String key) {
		return properties.getProperty(key);
	}


  /**
   * Given a key, retrieves the associated value if exists. If not, the provided default value is
   * returned instead.
   *
   * @param key The key
   * @param defaultValue default value returned if the key doesn't exist.
   * @return Either the associated value, or the default one.
   */
  public static String getParameter(String key, String defaultValue) {
    return Optional.ofNullable(getParameter(key)).orElse(defaultValue);
  }

	/**
	 * Returns the stored value for key {@code key} as a {@link Boolean} instance. Default is
	 * {@link Boolean#FALSE}.
	 *
	 * @param key The key
	 * @return Either {@link Boolean#TRUE} or {@link Boolean#FALSE}
	 * @see #getBooleanParameter(String, Boolean)
	 */
	public static Boolean getBooleanParameter(String key) {
		return getBooleanParameter(key, Boolean.FALSE);
	}

	/**
	 * Returns the stored value for key {@code key} as a {@link Boolean} instance. If the provided key
	 * doesn't exist (ie no value associated to it), then the provided default {@code default} value
	 * is returned.
	 *
	 * @param key The key
	 * @param defaultValue Default value returned when the key has no value associated. If
	 * 		{@code null}, considered to be {@link Boolean#FALSE}.
	 * @return Either {@link Boolean#TRUE} or {@link Boolean#FALSE}
	 */
	public static Boolean getBooleanParameter(String key, Boolean defaultValue) {
		return Boolean.parseBoolean(Optional.ofNullable(getParameter(key)).orElse(defaultValue.toString()));
	}

  /**
   * Gets the {@link Integer} value associated to the provided key.
	 *
	 * @param key the key
	 * @return Either the associated value or {@code 0} if the parameter doesn't exist.
   */
  public static Integer getIntegerParameter(String key) {
		return Integer.parseInt(Optional.ofNullable(getParameter(key)).orElse("0"));
	}

  /**
   * Gets the {@link Integer} value associated to the provided key. If the key doesn't exist, then
   * the default value is returned instead.
   *
   * @param key the key
   * @param defaultValue default value returned when the key doesn't exist
   * @return Either the associated value or the default one
   */
  public static Integer getIntegerParameter(String key, Integer defaultValue) {
    final var val = getParameter(key);
    if (val == null) return defaultValue;
    return getIntegerParameter(key);
  }

	/**
	 * Gets the {@link Double} value associated to the provided key.
	 *
	 * @param key the parameter's name
	 * @return Either the value, or {@code 0} if the parameter doesn't exist.
	 */
	public static Double getDoubleParameter(String key) {
		return Double.parseDouble(Optional.ofNullable(getParameter(key)).orElse("0.0"));
	}

	/**
	 * Gets the {@link BigDecimal} value associated to the provided key.
	 *
	 * @param key the parameter's name
	 * @return Either the value, or {@code 0} if the parameter doesn't exist.
	 */
	public static BigDecimal getBigDecimalParameter(String key) {
		return new BigDecimal(Optional.ofNullable(getParameter(key)).orElse("0.0"));
	}
	
	/**
	 * Loads the default parameters first and then the parameters from the given
	 * experimenterConfigFile. If experimenterConfigFile is null, only the default
	 * properties will be loaded.
	 * 
	 * @param parametersToLoad properties to load
	 */
	public static void loadParameters(Properties parametersToLoad) {
		properties.putAll(parametersToLoad);
		properties.list(System.out);
	}

	/**
	 * Returns a string representation of the internal {@link Properties} object.
	 *
	 * @return a string representation
	 */
	public static String getListedParameters() {
		return properties.toString();
	}

	/**
	 * Clear all parameters.
	 */
	public static void clearParameters() {
		properties.clear();
	}

}
