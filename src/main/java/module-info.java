module max.core {
  // Exported packages
  exports max.core;
  exports max.core.action;
  exports max.core.agent;
  exports max.core.inspection;
  exports max.core.inspection.exporter;
  exports max.core.inspection.collector;
  exports max.core.role;
  exports max.core.scheduling;
  exports max.core.test;
  exports max.core.cli;
  exports max.core.cli.configuration;

  // Dependencies
  requires java.logging;
  requires colt; // automatic module
  requires jcommander; // automatic module
  requires com.fasterxml.jackson.databind;
  requires json.schema.validator;

  // Even if types from these modules are exposed, customer modules will
  // have to explicitly require them.
  // This is done because max.test is not strictly required as it only contains testing utils
  requires transitive org.junit.jupiter.api;
  requires org.junit.jupiter.engine;
  requires org.junit.platform.launcher;
  requires org.junit.jupiter.params;

  // Transitive dependencies (At least a type is in a method signature)
  requires transitive madkit;
  requires java.desktop; // automatic module

  // Open max.core to madkit so madkit can use reflexive tools
  opens max.core to
      madkit;
  opens max.core.agent to
      madkit;
  opens max.core.agent.gui to
      madkit;
  opens max.core.scheduling to
      madkit;
  opens max.core.inspection to
      madkit;
}
