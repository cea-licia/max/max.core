# max.core

This repository contains the base layer of the entire MAX framework and defines key entities
including agents, contexts, environments, actions, plans and more. It enables raw agent-based
simulations with global supervision tools like a scheduler, probes and data collectors and basic
communications support.

If you are looking for more complex networking features, take a look at our [P2P model](https://cea-licia.gitlab.io/max/max.gitlab.io/library/network/peer-to-peer/). We also
provide an abstract layer for blockchain applications, which is
called [the generic blockchain model](https://cea-licia.gitlab.io/max/max.gitlab.io/library/blockchain/generic/).

## About MAX

__MAX__ (for 'Multi-Agent eXperimenter') is a framework for agent-based simulation and full
agent-based applications building. The objective of __MAX__  is to make rapid prototyping of
industrial cases and to carry out their feasibility analysis in a realistic manner.

__MAX__ is particularly well suited for modeling open, distributed and intelligent systems
developing over time. It embeds the Agent/Environment/Role (AER) organizational model, includes
tools and components useful for developing and testing simulation models and is written in Java 11
to run on any device.

You want to know if MAX is suited for your project ? Check our [website](https://cea-licia.gitlab.io/max/max.gitlab.io) to find it out!

## Getting Started

To include this model in your application, here is what you need to add in your pom file:

```xml
<!-- Add the MAX group package repository. -->
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.com/api/v4/groups/16222017/-/packages/maven</url>
  </repository>
</repositories>

...

<dependencies>
    <dependency>
      <groupId>max</groupId>
      <artifactId>core</artifactId>
      <version>${max.core.version}</version>
    </dependency>
</dependencies>
```

## Entrypoints

The model includes two entrypoints:

- a Command Line Interface (package `cli`, main class is `CLIMain`)
- a JUnit one, to launch simulations in a test (package `test`, main class is `TestMain`)

The CLI can be used to launch any model (including yours). It uses a parameters file to load the
experiment and can even plot data. The testing feature allows you to launch simulations in JUnit
tests to check how your model behaves. Some tests of the current module are written using it.

Check the [documentation](https://cea-licia.gitlab.io/max/max.gitlab.io/docs/using/running-experiments/) to learn how to run your simulations.

## Parametrization

The __max.core__ model uses some parameters to configure itself:

* Required parameters
    * `MAX_CORE_EXPERIMENTER_NAME` (String): full name of the experimenter's class (for
      example: `"max.core.ExperimenterAgent"`).
    * `MAX_CORE_SIMULATION_STEP` (Float): defines the step size for the simulation experiment. It is
      important to note that if an agent tries to schedule a behavior for a time which is not a
      multiple of this parameter, this behavior will not be scheduled.
    * `MAX_CORE_TIMEOUT_TICK` (Float): defines when the simulation will end, in number of ticks.
* Optional parameters:
    * `MAX_CORE_RESULTS_FOLDER_NAME` (String): Defines the folder's path where to put the collected
      data (if any) during the simulation. Default is a folder named `results` in the working
      directory.
    * `MAX_CORE_UI_MODE` (Enum): Defines if the simulator can draw GUIs. The two possible values
      are `SILENT` (default) and `GUI`, the latter allowing GUIs drawing.
  * `MAX_CORE_DESKTOP_FLAG` (Boolean): Decides if it should use the unified desktop window or not.
    If set to `true`, each agent's GUI will be displayed in a master window. Default is `false` and
    the parameter is relevant only if `MAX_CORE_UI` is set to `GUI`.

# Licence

__max.core__ is released under
the [Eclipse Public Licence 2.0 (EPL 2.0)](https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html)